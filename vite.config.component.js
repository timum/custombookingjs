/* 
  Excludes react so that consumers which are react apps themselves are able to use the TimumBooking component exported through index.jsx
  If not done, react is bundled together with bookingjs meaning consumers have 2 reacts which leads to copious amounts of errors on the consumers side. 

  If you've got a nicer solution not reliant on 2 separate conf files, feel free to delete me. 
*/
import { defineConfig } from 'vite';
import { resolve } from 'path';
import react from '@vitejs/plugin-react';
import svgrPlugin from 'vite-plugin-svgr';
import sourcemaps from 'rollup-plugin-sourcemaps';

export default defineConfig({
  define: { 'process.env.NODE_ENV': '"production"' },
  plugins: [react(), svgrPlugin()],
  build: {
    outDir: `build/component`,
    lib: {
      entry: resolve(__dirname, './src/index.jsx'),
      name: 'timum',
      fileName: `booking`,
    },
    sourcemap: true,
    rollupOptions: {
      external: ['react', 'react-dom'],
      output: {
        globals: {
          react: 'React',
          'react-dom': 'ReactDOM',
        },
      },
      plugins: [sourcemaps()],
    },
  },
});
