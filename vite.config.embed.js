import { defineConfig } from 'vite';
import { resolve } from 'path';
import react from '@vitejs/plugin-react';
import svgrPlugin from 'vite-plugin-svgr';
import sourcemaps from 'rollup-plugin-sourcemaps';

export default defineConfig({
  define: { 'process.env.NODE_ENV': '"production"' },
  plugins: [react(), svgrPlugin()],
  build: {
    outDir: 'build',
    lib: {
      entry: resolve(__dirname, './src/index.jsx'),
      name: 'timum',
      fileName: 'booking',
    },
    server: {
      open: true,
    },
    sourcemap: true,
    rollupOptions: {
      plugins: [sourcemaps()],
    },
    // rollupOptions: {
    //   external: ['react', 'react-dom'],
    //   output: {
    //     globals: {
    //       react: 'React',
    //       'react-dom': 'ReactDOM',
    //     },
    //   },
    // },
  },
});
