import { mergeConfig } from 'vite';

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],

  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
  ],

  staticDirs: ['../public'],

  framework: {
    name: '@storybook/react-vite',
  },
  async viteFinal(config) {
    const lol =  mergeConfig(config, {
      // Add storybook-specific dependencies to pre-optimization
      optimizeDeps: {
        include: ['@mui/material/Tooltip'],
      }
    });
    return lol;
  },

  typescript: {
    reactDocgen: 'false', // or false if you don't need docgen at all
  },

  docs: {
    autodocs: true
  }
};
