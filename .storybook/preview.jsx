import {
  createTheme,
  CssBaseline,
  responsiveFontSizes,
  ThemeProvider,
} from '@mui/material';
import { SnackbarProvider } from 'notistack';

import { MemoryRouter } from 'react-router';

import { Provider } from 'react-redux';
import store from '../src/store/store';
import { timumApiSlice } from '@timum/timum_pdk';
import { reset as resetAppState } from '../src/slices/appState';

import { initialize as initializeMSW, mswDecorator } from 'msw-storybook-addon';

import atcbCss from '../src/styles/atcb-css';
import { Global } from '@emotion/react';
import { StrictMode } from 'react';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

let theme = createTheme({
  palette: {
    primary: {
      light: '#5bc0de',
      main: '#337ab7',
    },
    error: {
      main: '#d9534f',
      contrastText: '#fff',
    },
    success: {
      main: '#5cb85c',
      contrastText: '#fff',
    },
    warning: {
      main: '#f0ad4e',
    },
    text: {
      primray: '#333',
      secondary: '#858585',
      disabled: 'rgb(51, 51, 51, 0.38)',
    },
  },
  typography: {
    fontFamily: 'Ek Mukta, Helvetica, Arial, sans-serif',
  },
  // components: {
  //   MuiOutlinedInput: {
  //     styleOverrides: {
  //       input: {
  //         paddingLeft: '12px',
  //         paddingTop: '6px',
  //         paddingRight: '12px',
  //         paddingBottom: '6px',
  //         lineHeight: 1.428571429,
  //         fontSize: 14,
  //       },
  //     },
  //   },
  //   MuiFormHelperText: {
  //     styleOverrides: {
  //       root: {
  //         lineHeight: 0.66,
  //       },
  //     },
  //   },
  //   MuiInputLabel: {
  //     styleOverrides: {
  //       root: {
  //         fontSize: 14,
  //         verticalAlign: 'middle',
  //         lineHeight: 'normal',
  //       },
  //     },
  //   },
  //   MuiButtonBase: {
  //     styleOverrides: {
  //       root: {
  //         textTransform: 'inherit',
  //       },
  //     },
  //   },
  //   MuiButton: {
  //     styleOverrides: {
  //       root: {
  //         textTransform: 'inherit',
  //       },
  //     },
  //   },
  //   MuiCssBaseline: {
  //     styleOverrides: {
  //       body: {
  //         scrollbarColor: '#bfbfbf #ffffff',
  //         '&::-webkit-scrollbar, & *::-webkit-scrollbar': {
  //           backgroundColor: '#ffffff',
  //         },
  //         '&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb': {
  //           borderRadius: 8,
  //           backgroundColor: '#bfbfbf',
  //           minHeight: 16,
  //           width: 8,
  //           border: '3px solid #ffffff',
  //         },
  //         '&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus':
  //           {
  //             backgroundColor: '#8f8f8f',
  //           },
  //         '&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active':
  //           {
  //             backgroundColor: '#8f8f8f',
  //           },
  //         '&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover':
  //           {
  //             backgroundColor: '#8f8f8f',
  //           },
  //         '&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner': {
  //           backgroundColor: '#ffffff',
  //         },
  //       },
  //     },
  //   },
  // },
});
theme = responsiveFontSizes(theme, { factor: 2.5 });

initializeMSW();

const stdLayout = (Story) => {
  store.dispatch(timumApiSlice.util.resetApiState());
  store.dispatch(resetAppState());

  return (
    <StrictMode>
      <Global styles={atcbCss} />

      <ThemeProvider theme={theme}>
        <SnackbarProvider
          maxSnack={3}
          autoHideDuration={3000}
          anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        >
            <Provider store={store}>
              <>
                <CssBaseline />
                <Story />
              </>
            </Provider>
        </SnackbarProvider>
      </ThemeProvider>
    </StrictMode>
  );
};

export const decorators = [mswDecorator, stdLayout];
