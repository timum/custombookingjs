import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import sourcemaps from 'rollup-plugin-sourcemaps';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  rollupOptions: {
    plugins: [sourcemaps()],
  },
});
