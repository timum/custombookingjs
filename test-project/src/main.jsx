// import Dialog from '@mui/material/Dialog';
// import DialogTitle from '@mui/material/DialogTitle';
// import DialogContent from '@mui/material/DialogContent';
// import DialogContentText from '@mui/material/DialogContentText';
// import DialogActions from '@mui/material/DialogActions';
// import Button from '@mui/material/Button';

// import React from 'react';
// import { createRoot } from 'react-dom/client';
import { init } from '../../build/booking.js';
// import { TimumBooking } from '../../build/component/booking.js';
// import UseAsModule from './UseAsModule.jsx';
import { default as config } from './config.js';

// const rootEl = document.getElementById('bookingjs');
// const root = createRoot(rootEl);
// root.render(
//   <React.StrictMode>
//     <TimumBooking appConfig={config.appConfig} muiTheme={config.muiTheme} />
//   </React.StrictMode>,
// );

init(config.appConfig, config.muiTheme);

// init({
//   ref: 'booking-widget-demo-resource@timum',
//   host: 'http://localhost:9999',
//   // With the interfaces prop customization gets crazy.
//   // Here you can customize the various dialogs to your liking.
//   // The methods are capitalized bc you are effectively defining react components.

//   // All of them have a 'children' prop. It contains the standard dialog content as written by timum.
//   // If you want to just change the containers (removing fullscreen, limit size, re-enable backdrop ...)
//   // but not the general content just change the examples below to your liking.

//   // If you need even more customisation, you can forego the usage of the provided 'children' prop completly
//   // and write your own interface.
//   // The methods get passed in all the information we ourselves used for each particular component.
//   // This does include the necessary queries so you don't nee to import our sdk on your own
//   // (nor need to setup a redux store).
//   interfaces: {
//     // used when no valid(!) pdData is given.
//     //( meaning if one is given but timum cannot figure out who this is, regardless, this is shown. )
//     UnkownBookingPage: ({ open, onClose, children }) => {
//       return (
//         <Dialog open={open} onClose={onClose}>
//           <DialogContent>{children}</DialogContent>
//         </Dialog>
//       );
//     },

//     // used when valid(!) pdData is given.
//     //( meaning if one is given but timum CAN figure out who this is, this is shown. )
//     IdentifiedBookingPage: ({ open, onClose, children }) => {
//       return (
//         <Dialog open={open} onClose={onClose}>
//           <DialogContent>{children}</DialogContent>
//         </Dialog>
//       );
//     },

//     // is used by unknownBookingPage and identifiedBookingPage in reaction to a successfull booking
//     // the existing container (of UnkownBookingPage / IdentifiedBookingPage) is used and it's contents
//     // replaced. In the standard case this is a <Dialog> as seen above.
//     ConfirmationPage: ({
//       // start,
//       // end,
//       // productName,
//       // productDescription,
//       // resourceName,
//       // customerMail,
//       // contactChannel,
//       // bookingProcess,
//       children,
//     }) => {
//       return children;
//     },

//     // is used by unknownBookingPage when typed in email belongs to an active timum user.
//     // (Security feature, preventing unknwons to fill our users calenedars with arbitrary appointmens.)
//     LoginPage: ({ open, onClose, children }) => {
//       return (
//         <Dialog open={open} onClose={onClose}>
//           <DialogContent>{children}</DialogContent>
//         </Dialog>
//       );
//     },
//     CancelPage: ({
//       // timeslot,
//       // productName,
//       // resourceName,
//       // onCancelSuccess,
//       open,
//       onClose,
//       children,
//     }) => {
//       return (
//         <Dialog open={open} onClose={onClose}>
//           <DialogContent>{children}</DialogContent>
//         </Dialog>
//       );
//     },
//     ProductPage: ({ open, onClose, children }) => {
//       return (
//         <Dialog open={open} onClose={onClose}>
//           <DialogContent>{children}</DialogContent>
//         </Dialog>
//       );
//     },
//     ErrorDialog: ({ open, onClose }) => {
//       // const { t } = useTranslation();

//       return (
//         <Dialog
//           open={open}
//           onClose={onClose}
//           aria-labelledby="alert-dialog-title"
//           aria-describedby="alert-dialog-description"
//         >
//           <DialogTitle id="alert-dialog-title">
//             {/* {t('reservation_failed.title')} */}
//           </DialogTitle>
//           <DialogContent>
//             <DialogContentText id="alert-dialog-description">
//               {/* {t('reservation_failed.message')} */}
//             </DialogContentText>
//           </DialogContent>
//           <DialogActions>
//             <Button onClick={onClose} autoFocus>
//               Ok
//             </Button>
//           </DialogActions>
//         </Dialog>
//       );
//     },
//   },
// });
