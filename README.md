# timum booking

## Introduction

timum BookingJs is a fully customizable appointment booking frontend app, intended to be integrated into every web page or web app or mobile app. It is ready to use out-of-the-box. And it provides the ability to customize it.
This documentation guides you through all the customization capabilities.

## Content

- [Features](#features)
- [How to Integrate](#how-to-integrate)
  - [In a Script Tag](#in-a-script-tag)
  - [As ESM import](#as-esm-import)
  - [Entity Referencing](#entity-referencing)
    - [Channel Reference](#channel-reference)
    - [Resource Reference](#resource-reference)
    - [Resource Reference with channelKey](#resource-reference-with-channelKey)
      - [Example](#example)
    - [Customer pre-identification](#customer-pre-identification)
      - [Identification via pDataId](#identification-via-pdataid)
      - [Identification via logged-in User](#identification-via-logged-in-user)
- [Advanced Customisation](#advanced-usage)
  - [muiTheme](#muiTheme)
  - [fcConfig](#fcConfig)
  - [appConfig](#appConfig)
  - [How to use Callbacks](#how-to-use-callbacks)
    - [Booking Related](#booking-related)
    - [Cancelation Related](#cancelation-related)
    - [Data Fetching Related](#data-fetching-related)
  - [Localisation](#localisation)
  - [Booking Form Fields](#booking-form-fields)
    - [Custom Fields](#custom-fields)
  - [Custom Field Example](#custom-field-example)
    - [The Scenario](#the-scenario)
    - [The Implementation](#the-implementation)

<a name=”features”></a>

## Features

- Real time availability and booking
- Booking requests for public appointments - approval required
- Multiple participant appointments
- Visitor ligitimation - only invited visitors see the booking widget on public pages, listings, exposés
- Visitor identification - via customer ID from CRM
- Show/hide fully booked appointments option
- Prebooking period option - prevents booking if appointment is too soon from now
- Double booking prevention
- Cancelling appointment by customer with proper authentication
- Fully customizable look and feel with mui theming (available with professional plans)
- Customizable wording across languages (availabe with professional plans)
- Dynamic definition of input fields with localized labels and validation (available with professional plans)
  - Therefore, ability to request additional information from customers during booking process
- Support for multiple languages (English, Italian, Spanish, French and German included) - add your own language
- Callbacks for custom code execution and event response (e.g. booking created, appointments loaded, booking canceled)

<a name=”how-to-integrate”></a>

## How to Integrate

<a name=”in-a-script-tag”></a>

These are some minimal examples. BookingJs is highly customizable.
An overview over all configuration options can be found in [Advanced Customisation](#advanced-usage)

### In a Script Tag

Add the following code to your webpage:

```html
<div id="bookingjs" style="margin: 32px"></div>
<script type="module">
  import * as timum from 'https://cdn.timum.de/bookingjs/1/booking.js';

  timum.init({ ref: 'booking-widget-demo-resource@timum' });
</script>
```

A working fiddle can be found [here](https://jsfiddle.net/timum/3swq1tdy/).

Alternatively, you can add `ref` to your url as a parameter. This allows you to omit `ref` in the javascript portion. Like this:

`https://your.website.nic?ref=<enter resource reference here>`

```html
<div id="bookingjs" style="margin: 32px"></div>
<script type="module">
  import * as timum from 'https://cdn.timum.de/bookingjs/1/booking.js';

  timum.init(); //<- no need for the reference here
</script>
```

<a name=”as-esm-import”></a>

### As ESM import

1. Add timum booking to your project with
   `yarn add @timum/booking`
   or use npm with
   `npm install @timum/booking`
   <br>
2. Add `<div id="bookingjs" style="margin: 32px"></div>` where you want timum to be displayed (the margin is just a sggestion, not mandatory).
   <br>
3. Add the following code to one of your .js files:

```javascript
import { init } from '@timum/booking';

init({ ref: 'booking-widget-demo-resource@timum' });
```

A example project can be found [here](https://stackblitz.com/edit/react-8q6r8b?file=src/index.js)

### As React component import

1. Add timum booking to your project with
   `yarn add @timum/booking`
   or use npm with
   `npm install @timum/booking`
   <br>
2. Add the following code anywhere in your jsx:

```javascript
import { TimumBooking } from '@timum/booking';

// other code

<TimumBooking
  appConfig={{
    ref: 'booking-widget-demo-resource@timum'
    // other options
  }}
/>;

// other code
```

<a name="entity-referencing"></a>

### Entity Referencing

As shown in [How to Integrate](#how-to-integrate), there are two ways in which to provide bookingjs with the necessary references to retrieve public appointments from the backend.

Additionally, there are three different kinds of references you can provide, each having their own implications concerning the delivered appointments or whether the booking frontend is shown at all.

<a name="channel-reference"></a>

#### Channel Reference

Resource channels are entities in Timum, each storing a configuration that changes the booking experience for customers. Each individual resource has four channels that users can distribute to their customers through a link.
To use these channels in an embed scenario, provide a channel reference in the ref property (either in the URL or as part of the config). In this case, nothing else is required.

<a name="resource-reference"></a>

#### Resource Reference

A reference that refers to a plain resource, where the channel is unknown. The default channel (and its corresponding configuration) is used instead.

<a name="#resource-reference-with-channelKey"></a>

#### Resource Reference with ChannelKey

If you don't have a channel reference but still want to use a different channel than the default one, you can provide a combination of resource reference and `channelKey` to uniquely identify the channel (and resource) to be used.

The following table gives an overview of the different permutations and how bookingjs behaves when encountering each of them:

| url RESref OR CHref | timum.init RESref OR CHref | timum.init chKey |          show OR hide          |
| :-----------------: | :------------------------: | :--------------: | :----------------------------: |
|          -          |             -              |     ignored      |            **HIDE**            |
|  ResourceReference  |          ignored           |       YES        | **show widget for channelKey** |
|  ResourceReference  |          ignored           |        -         |    **show default Channel**    |
|          -          |     ResourceReference      |        -         |    **show default Channel**    |
|          -          |     ResourceReference      |       YES        | **show widget for channelKey** |
|  ChannelReference   |          ignored           |     ignored      |        **show Channel**        |
|          -          |      ChannelReference      |     ignored      |        **show Channel**        |

- RESref: short for resource reference.
- CHref: short for channel reference.

<a name="example"></a>

##### Example

- First row: If neither a resource reference nor a channel reference is provided, not in the URL nor in the config object, any given channel key is ignored, and bookingjs is hidden.

- Third row: If a resource reference is given in the URL and in the config object, and no channelKey is provided, the ref in the URL takes priority. Since it is a resource reference and no channel key is provided, the default channel is used.

- Fifth row: If a resource reference and a `channelKey` are provided in the config object, and there is nothing in the URL, the config object is used. BookingJs gets displayed using the channel uniquely identified by the resource reference and the `channelKey`.

<a name="customer-pre-identification"></a>

#### Customer pre-identification

BookingJs can identify customers automatically if configured correctly.
This means they don't need to input their personal data prior to booking.

<a name="identification-via-pdataid"></a>

##### Identification via pDataId

Detection method: Is there a valid pDataId paired with a supported pDataPlatform (platform) in parent url matching an entity (contact, customer) in given CRM?
<em>
General behaviour: If there is, then visitor is pre identified. If not: provide standard booking dialog.
</em>
See [appConfig](#appconfig) for more information about pData.

<a name="identification-via-logged-in-user"></a>

##### Identification via Logged-in User

Even customers can register at timum and become registered and logged-in users. If the booking system identifies a logged-in user, the booking dialog does not provide personal data input fields but informs the visitor that they are identified.

> Note that if the customer is logged in to timum and there is a pData config/url param as well then the auth cookie takes priority.

---

<a name=”advanced-usage”></a>

## Advanced usage

timum booking has a lot of customisation options.

The init accepts the following arguements in the order listed here.
The following table gives a rough overview over each.

| Arguments | Description                                                                                        |
| --------- | -------------------------------------------------------------------------------------------------- |
| appConfig | object, containing various options like callbacks, localiation, input fields etc.                  |
| muiTheme  | object, mui theme allowing you to change the look and feel of timum. Requires a professional plan. |
| fcConfig  | object, only used if you use full calendar as a booking frontend (settable in appConfig).          |

<a name=”muiTheme”></a>

### muiTheme

timum uses mui components for all frontends. The muiTheme object allows global design changes.
See [here](https://mui.com/) for a general overview. <br> See [here](https://mui.com/material-ui/customization/default-theme/) for theme related documentation.

When you open the file `config.js` in [this](https://stackblitz.com/edit/react-8q6r8b?file=src/index.js) example, you can find an elaborate custom configuration which includes a redesign of the standard timum theme.

Needs professional plan.

<a name=”fcConfig”></a>

### fcConfig

FullCalendar can be set as one of the frontends in appConfig.
See [here](https://fullcalendar.io/docs#toc) for a full list of configuration options.

> Note that since fullCalendar isn't mui based, it does not consider the muiTheme object.

There are also some options specific to bookingjs. In addition to the configuration options of fullcalendar the following options are also available (all of them are optional):

<table>
   <tr>
      <th>Property</th>
      <th>Description</th>
   </tr>
   <tr>
      <td>largeView</td>
      <td>string, Determines which <code>view</code> is displayed for screen widths <i>greater</i> 601px. <br>Please refer to the fullcalendar docs linked above for an overview of applicable values. </td>
   </tr>
   <tr>
      <td>smallView</td>
      <td>string, Determines which <code>view</code> is displayed for screen widths <i>smaller</i> 601px. <br> Please refer to the fullcalendar docs linked above for an overview of applicable values. 
   </td>
   </tr>
   <tr>
      <td>useCustomTimumCss</td>
      <td>boolean, to increase responsiveness, timum overrides some fullcalendar css classes. Set this to <code>false</code> to gain total control over fullcalendar's css.
   </td>
   </tr>
</table>

<a name=”appConfig”></a>

### appConfig

This object's options directly affect timum's behaviour or allow you to react to it.

<table>
   <tr>
      <th>Property</th>
      <th>Description</th>
   </tr>
   <tr>
      <td>ref</td>
      <td>string/array, Reference of the resource to show the appointments of. <br><em> Either this or tslRefs (see below) must be defined.</em><br> Everything else is optional. <br>Can also be a url parameter.
      <br>You can also provide a list of string references. Booking.js will then allow your customer to choose for which resource appointments should be displayed.</td>
   </tr>
   <tr>
      <td>tslRefs</td>
        <td>string/array, Reference(s) of specific appointments. For when you only want to share a selection of a resource's appointments.<br><em>Either this or refs (see above) must be defined.</em><br> Everything else is optional. <br>Can also be a url parameter.
        <br>You can also provide a list of string references. Booking.js will then load all appointments and corresponding resource information. If they belong to different resources, then bookingjs allows your customer to choose for which resource appointments should be displayed.
        <blockquote>
          Note: If this and ref are defined in conjunction: 
          The following behaviour is not yet set in stone and may<b> change in the future. </b><br>
          First, bookingjs will load all appointments of the given resources. It then loads all the appointments defined in tslRefs. It then tries to match both results in the following way:<br><br>
          If a ref has none of it's appointments referenced in tslRef: <br>
          <ul>
            <li>allow the corresponding resource to be selected by the customer.</li> 
            <li>show all free and bookable appointments of that resource.</li>
          </ul> 
          <br>
          If a tslRef belongs to a resource also defined in ref:
          <ul>
            <li>allow the corresponding resource to be selected by the customer.</li> 
            <li>show only the appointments defined in tslRefs. Discard all other appointments which would have otherwise been bookable.</li>
          </ul> 
          <br>
          If a tslRef belongs to no resource defined in ref:
          <ul>
            <li>allow the corresponding resource to be selected by the customer.</li> 
            <li>show (and load) only the corresponding appointment.</li>
          </ul>
        </blockquote>   
      </td>
    </tr>
    <tr>
      <td>prdRefs</td>
      <td>string/array, Reference(s) of specific products. For when you only want your customer to choose from a subset of your otherwise fully available list of active products.
      <blockquote>
        Note: If this and <code>tslRefs</code> are defined in conjunction: 
        To facilitate a seamless booking process, all products linked to appointments or availabilities are made selectable for customers. This ensures that customers can always choose a product that grants access to the corresponding bookable timeslots referenced in <code>tslRefs</code>.
        Without this step, customers might be restricted by <code>prdRefs</code> from selecting certain products, rendering related appointments or availabilities unselectable. In this way, <code>tslRefs</code> has priority over this field.
      </blockquote>  
    </td>
   </tr>
       <tr>
      <td>allResourcesOption</td>
      <td>boolean, This option determines whether the resource selection screen presents an "All" option for customers. When this flag is true, customers can choose to see and book from all available appointments across different resources in a single interface. <br><br>
      If the customer selects the "All" option, the system displays all bookable appointments for each resource that match their selected time and product.<br><br>
      To ensure good performance, the system will filter out duplicate appointments. For instance, if there are multiple appointments that start at the same time and offer the same service, only one of these will be shown, until booked at which point the next appointment in line will be loaded and displayed.
      <br><br>
      This feature is particularly useful when it's more important for customers to secure an appointment rather than choosing a specific resource. It allows for greater flexibility and ease in booking, especially during peak times or when specific resources may be limited.
    </td>
   </tr>
   <tr>
      <td>platform</td>
      <td>string, A fully qualified reference (<code>id@someUuid@platform</code>) can be unwieldy especially if used as a url param. This prop allows you to hardcode the <code>platform</code> part.
      <br><br>Note that this prop is ignored if <code>ref</code> is both:
      <ul>
        <li>used as a url param </li>
        <li>contains an @.</li>
      </ul>  
   </td>
   </tr>
   <tr>
      <td>prvUuid</td>
      <td>string, A fully qualified reference (<code>id@someUuid@platform</code>) can be unwieldy especially if used as a url param. This prop allows you to hardcode the <code>someUuid</code> part. This part is only sometimes necessary to uniquely identify a resource in timum. <br><br>Note that this prop is ignored if <code>ref</code> is both:
      <ul>
        <li>used as a url param </li>
        <li>contains an @.</li>
      </ul>  
   </td>
   </tr>
   <tr>
      <td>height</td>
      <td>  string, Height of timum on your page. `500px` is standard.</td>
   </tr>
    <tr>
      <td>allowCloseOnBooking</td>
      <td>boolean, whether the confirmation view is closeable once the booking was successfull.</td>
   </tr>
    <tr>
      <td>hideTimumFooter</td>
      <td>boolean, whether 'powered by timum' can be hidden or not. Needs professional plan.</td>
   </tr>
    <tr>
      <td>sendCustomValuesInMessage</td>
      <td>boolean, whether values of custom fields are concatenated and comma-separated into a single string which is then sent to the backend as <code>message</code>. If field <code>message</code> is defined its input is concatenated to the generated string as well.</td>
   </tr>
   <tr>
      <td>channelKey</td>
      <td>string, timum has, as of now, 4 different channels through which you can share your resource's appointments. <br> You can find them in the timum frontend under &lt;resource name&gt; -&gt; Calendar Sharing (Terminbuchung freigeben). <br> Every channel has its own settings allowing you to control whom of your customers can see certain appointments, whether they book directly or create a request first and other settings. <br> Valid values for this attribute are: <br> 
         - RESOURCE_PUBLIC <br> referring to "Public Booking Link" (Öffentlicher Buchungs-Link) <br> 
         - RESOURCE_EXCLUSIVE <br> referring to "Exclusive Booking Access" (Exklusiver Buchungs-Zugang) <br> 
         - resource-reference <br> referring to "Embedded booking calendars" (Eingebettete Buchungskalender) <br>
         - CALENDAR_PUBLIC <br> referring to "In all Website Plugin Views and your General Calendar" (In Website-Plugin Ansichten sowie Ihrem Gesamtkalender) <br>RESOURCE_PUBLIC is the default, used if you specify nothing else. <br><br>Can also be a url parameter. 
      </td>
   </tr>
   <tr>
      <td>calendarFrontend</td>
      <td>string, one of <code>fullCalendar</code>, <code>detailsFullCalendar</code> (these are what `init`'s 3rd parameter is for), <code>pureListView</code>, <code>detailsListView</code>, <code>condensedView</code> or <code>detailsCondensedView</code>
      <br>
      <br>
       <blockquote>
      Note: the <code>details</code> variants show details about the resource, like the description and the external url, as well as public information about the assigned consultant, if the channel allows that. 
       </blockquote>   
      </td>
   </tr>
   <tr>
      <td>culture</td>
      <td>string, The localisation to use. If not specified the browser language is used. Can also be a url parameter.</td>
   </tr>
   <tr>
      <td>pData</td>
      <td>object, <br> Data indentifying the customer, so that they don't have to input their data again. <br> Works in conjunction with timum and select, supported plaforms like OnOffice, Immosolve etc. <br> Properties: <br> 
         <code>{ platform: string, personId: string }</code> 
         <br> Can also be a url parameter. -&gt; the params are named differently though: pDataPlatform, pDataId
      <blockquote>
      Note: pData is ignored in favor of a timum auth cookie.
      Use incognito mode, another browser or sign out to circumvent this.
       </blockquote>   
      </td>
   </tr>
   <tr>
      <td>callbacks</td>
      <td>object, may contain various functions which allow to to run custom code in reaction to certain events. See below for a guide on how to do this. </td>
   </tr>
 <tr>
   <td>postMessageTarget</td>
   <td>
       string, URL of the parent site that receives callback events via the postMessage API. This property enables all callback events to be sent using the postMessage() method, which is particularly useful when using bookingjs within an <code>iframe</code>, as it restricts access to the parent site's JavaScript code.
       <br>
       The <code>event.data</code> object includes all parameters typically passed to callback functions, along with an <code>origin</code> field (always "bookingjs") and a <code>type</code> field that indicates the event's name. This <code>type</code> matches the name of the callback function you would use in a non-iframe scenario. Refer to the guide on callback functions for specifics on available event names.<br>
      <blockquote>
         <strong>Note:</strong> Setting this property overrides the <code>callback</code> property; all standard callbacks will instead trigger postMessage() events.
      </blockquote>
   </td>
</tr>
   
   <tr>
      <td>fields</td>
      <td>object, You can customise what information is demanded of your customers prior to booking. timum requires certain fields to work and has some optional fields it can parse. Fields other than those supported by timum can be evaluated in a callback (see the callback guide below for further info). All fields (yours too!) support localization and input validation. 
      <br>
      Needs professional plan. 
      </td>
   </tr>
   <tr>
      <td>localization</td>
      <td>object. Contains all localization variables and their standard texts. timum nativly supports English, German, French, Spanish and Italian. Use this to override the standard text and/or add translations for e.g your custom field labels and input validations (see the localisation guide for more info.)
      <br>
      Needs professional plan. 
      </td>
   </tr>
</table>

<a name=”how-to-use-callbacks”></a>

#### How to use callbacks

The callbacks object may contain any of the following functions.

<a name=”booking-related”></a>

##### Booking related

- `openedBookingPage`
- `closedBookingPage`,
- `createBookingStarted`
- `createBookingSuccessfull`
- `createBookingFailed`

All Booking related callbacks receive a <em>single obejct</em> as argument containing the following properties:

- a `timeslot` looking like this:
  ```
     {
        start: luxon Datetime object. Internal state. "2023-01-27T09:05:00.000Z",
        end: luxon Datetime object. Internal state. "2023-01-27T09:35:00.000Z",
        timeslot_uuid: uuid of the booked timeslot. e.g. "82ec5220-9d55-11ed-8617-e4a7a0ca32e8",
        product_uuid: uuid of the booked product e.g. "92867f70-4836-11e5-bc04-021a52c25043",
        product_name: string. e.g. "Meeting",
        resource_name: string. e.g. "Booking Widget DEMO",
        capacity: number e.g. 1,
        capacity_left: number e.g. 1,
        kind: either "models.Bookable" or "models.LotAppointment",
        untouchedStart: ISO String e.g: "2023-01-27T09:05:00+01:00" as the server sent it,
        untouchedEnd: ISO String e.g: "2023-01-27T09:35:00+01:00" as the server sent it
        }
  ```
- a `data` object looking like this:

```javascript
 {
   firstName: 'string',
   lastName: 'string',
   email: 'string',
   agbs: 'bool'

   // plus whatever optional and/or custom fields you yourself have defined.
   // -> so the makeup of this object changes depending on your settings.
 }

```

Additionally, `createBookingFailed` and `createBookingSuccessfull` also have a RTKQ `response` object in their respective argument. See [here](https://redux-toolkit.js.org/rtk-query/usage-with-typescript#type-safe-error-handling) for a reference.

<a name=”cancelation-related”></a>

##### Cancelation Related

- `openedCancelPage`
- `closedCancelPage`
- `cancelationStarted`
- `cancelationSuccessful`
- `cancelationFailed`

All cancelation related callbacks receive a single obejct as argument containing the same properties as described in [Booking related](#bookingRelated)

Additionally, `cancelationSuccessful` and `cancelationFailed` also have a RTKQ response object in their respecive argument. See here for a reference.

<a name=”dialog_related”></a>

##### Dialog related

In addition to the dialog related callbacks already mentioned in [Cancelation Related](#cancelation-related) and [Booking related](#booking-related), there are callbacks for all other dialogs as well.

- `openedProductSelection`
- `openedResourceSelection`
- `openedConfirmationPage`
- `closedProductSelection`
- `closedResourceSelection`
- `closedConfirmationPage`

These do not receive any parameters.

<a name=”data-fetching-related”></a>

##### Data Fetching Related

In order to display public appointments, resource and calendar information timum sends several requests.

- `fetchingPublicDataSucceeded`
  Gets passed in a single object looking like this:

  ```javascript
    {
      contact: {
        name: 'string',
        email: 'string',
        mobile: 'string',
        phone: 'string',
      },
      resource: {
        name: 'string', //<- 80 chars, max length of names in timum
        description: 'string',
        msgHelpText: 'string',
      },
      provider: {
        name: 'string',
        description: 'string',
        isThemingAllowed: 'boolean',
        isLocalisationAllowed: 'boolean',
        areCustomFieldsAllowed: 'boolean'
      },
      channel: {
        bookingProcess: 'string' // One of 'IMMEDIATE' or 'REQUESTED',
      },
    }
  ```

- `fetchingPublicDataFailed`
  receives a single RTKQ error object. See [here](https://redux-toolkit.js.org/rtk-query/usage-with-typescript#type-safe-error-handling) for a reference.
  <br>
- `fetchingProductsSucceeded`
  Gets passed in a single object looking like this:
  ```javascript
  {
    products: [
      {
        description: string,
        minDuration: number,
        name: string,
        uuid
      }
      //...
    ];
  }
  ```
- `fetchingProductsFailed`
  receives a single RTKQ error object. See [here](https://redux-toolkit.js.org/rtk-query/usage-with-typescript#type-safe-error-handling) for a reference.
  <br>
- `fetchingBookablesSucceeded`
  Gets passed in a single object looking like this:
  ```javascript
    {
      2023-02-06: [ //<- this obviously changes depending on when appointments are available.
        0: {
            timeslot_uuid: uuid of the booked timeslot,
            product_uuid: uuid of the booked product,
            product_name: string,
            resource_name: string,
            capacity: number,
            capacity_left: number,
            kind: one of "models.Bookable" or "models.LotAppointment",
            start: ISO String e.g: "2023-02-06T09:05:00+01:00",
            end: ISO String e.g: "2023-02-06T09:35:00+01:00"
        },
        //... more appointments ...
      ],
      //... more dates ...
    }
  ```
  - `fetchingBookablesFailed`
    receives a single RTKQ error object. See [here](https://redux-toolkit.js.org/rtk-query/usage-with-typescript#type-safe-error-handling) for a reference.
    <br>

<a name=”localisation”></a>

#### Localisation

timum booking has the ability to change all of its text. It comes with English, Italian, Spanish, French and German pre-installed, but you can add additional translations as well. You can also change the text for these languages to your preference. You don't have to redefine the entire localization object; you can just add or change the specific texts you want. Any missing texts will be taken from the default.
The code snippet provided shows the complete localization object and the standard text for both English and German. For each language, it includes text for different parts of the booking process such as product selection, appointment booking, appointment request, appointment cancellation, form fields, and more.

```javascript
localization: {
  de: {
    resource_selection_headline: 'Resource wählen',
    product_selection_headline: 'Terminart wählen',
    booked_successfully_header: 'Termin gebucht',
    booked_successfully_message:
      'Sie erhalten eine E-Mail mit den Termindetails an {{mail}}',
    requested_successfully_header: 'Termin angefragt',
    requested_successfully_message:
      'Sie erhalten eine E-Mail mit den Termindetails an {{mail}}. Sie werden unter der gleichen Adresse benachrichtigt, sobald Ihre Anfrage bearbeitet wurde.',
    submit_button_book: 'Buchen',
    submit_button_request: 'Verbindlich Anfragen',

    noEventsMessage:
      'Zur Zeit sind leider keine buchbaren Termine verfügbar.',
    appoinment_at_capacity: 'belegt',
    add_to_calendar_btn: 'Zu Kalender hinzufügen',
    until_reservation_expiration:
      '{{expiration}} bis zum Ablauf der Reservierung',
    reservation_expired: 'Reservierung abgelaufen.',
    identified_customer_hint:
      'Sie wurden mit persönlichem Link eingeladen und können direkt Ihren Termin buchen.',
    reservation_failed: {
      title: 'Hohe Nachfrage',
      mesage:
        'Dieser Termin wurde gerade durch jemanden reserviert. Bitte wählen' +
        'Sie einen anderen Termin. Oder schauen Sie später noch einmal, ob' +
        'ein Termin frei wird.',
    },
    cancellation: {
      cancelation_successfull_message: 'Termin erfolgreich abgesagt',
      cancellable_appointment_highlight: 'Mein Termin',
      submit_button_cancel: 'Absagen',
      cancel_appointment_header: 'Ihr Termin',
      message_label: 'Nachricht zur Terminabsage',
    },
    validation: {
      field_required: 'Notwendig',
      privacy_field_required:
        'Sie müssen die Datenschutzbestimmungen akzeptieren bevor Sie buchen können.',
      email_field_must_be_valid: 'Geben Sie eine valide E-Mail Adresse ein',
    },
    fields: { // field labels are markdown capable. That's why you can use basic html as well.
      firstName: 'Vorname',
      lastName: 'Nachname',
      name: 'Name',
      email: 'E-Mail',
      mobile: 'Mobil',
      message: 'Ihre Nachricht',
      accept_timum_privacy:
            '<a href="https://info.timum.de/datenschutz" target="_blank">Datenschutzbestimmungen</a> gelesen und akzeptiert',

    },
  },
  // the same for english.
  en: {
    resource_selection_headline: 'Choose Resource',
    product_selection_headline: 'Choose Product',
    booked_successfully_header: 'Appoinment Booked',
    booked_successfully_message:
      'You will receive an email with appointment details to {{mail}}',
    requested_successfully_header: 'Appointment Requested',
    requested_successfully_message:
      'You will receive an email with appointment details to {{mail}}. You will be notified at the same address once your request has been processed.',
    submit_button_book: 'Book',
    submit_button_request: 'Request',

    noEventsMessage:
      'Unfortunately, there are no bookable dates available at the moment.',
    appoinment_at_capacity: 'fully booked',
    add_to_calendar_btn: 'Add to Calendar',
    until_reservation_expiration:
      '{{expiration}} until reservation expiration.',
    reservation_expired: 'Reservation expired.',
    identified_customer_hint:
      'You have been invited with a personal link and can book your appointment directly.',
    reservation_failed: {
      title: 'High Demand',
      mesage:
        'This appointment has just been reserved by someone. Please choose another appointment. Or check back later to see if an appointment becomes available.',
    },
    cancellation: {
      cancelation_successfull_message: 'Appointment canceled sucessfully.',
      cancellable_appointment_highlight: 'My Appointment',
      submit_button_cancel: 'Cancel',
      cancel_appointment_header: 'Your Appointment',
      message_label: 'You may enter a reason here.',
    },
    validation: {
      field_required: 'Required',
      privacy_field_required:
        'You must accept the privacy policy prior to booking.',
      email_field_must_be_valid: 'Enter a valid email address',
    },
    fields: { // field labels are markdown capable. That's why you can use basic html as well.
      firstName: 'First name',
      lastName: 'Last name',
      name: 'name',
      email: 'E-mail',
      mobile: 'Mobile',
      message: 'Your Message',
      accept_timum_privacy:
            '<a href="https://info.timum.de/datenschutz" target="_blank">Privacy policy</a> read and accepted.',

    },
  },
},
```

<a name=”booking-form-fields”></a>

#### Booking Form Fields

You can customize the information you request from customers before booking by defining fields. The minimum required fields for timum are <code>firstName</code>, <code>lastName</code>, <code>email</code>, and <code>agbs</code>. By default, timum also requests <code>mobile</code> and <code>message</code> fields, but you can add your own fields as well.

Fields, including custom ones, support validation and localization.
Validation is realized using the yup library, which is included with timum. You can import it using the following code:

```javascript
import { yup } from '@timum/booking';
```

Localization is achieved by using the title property in the field definition. You can specify a translation key and add the corresponding translation to the localization object. The same process can be applied to custom field validations.

The standard configuration can be overridden except for the aforementioned, required fields. The following is the standard configuration:

```javascript
fields: {
  firstName: {
    index: 0,
    title: 'fields.firstName',
    validation: yup.string().required('validation.field_required'), // <- compare with key in localisation
  },
  lastName: {
    index: 1,
    title: 'fields.lastName',
    validation: yup.string().required('validation.field_required'),
  },
  email: {
    index: 2,
    title: 'fields.email',
    format: 'email',
    type: 'text',
    validation: yup
      .string()
      .email('validation.email_field_must_be_valid')
      .required('validation.field_required'),
  },
  mobile: {
    index: 3,
    title: 'fields.mobile',
    type: 'phoneNumber',
    isRequired: false,
    defaultCountry: 'DE',
    preferredCountries: ['DE', 'CH', 'AT'],
    // validation: is in built and ignored
  },
  message: {
    index: 7,
    title: 'fields.message',
    type: 'textarea',
    validation: yup
      .string()
      .max(1024)
      .test(
        'no-emojis',
        'validation.no_emojis',
        (value) => !RE_EMOJI.test(value), // import with 'import { RE_EMOJI } from @timum/booking'
      ),
    limit: 1024,
    onChange: (value) => value.replace(RE_EMOJI, ''),
  },
  agbs: {
    index: 8,
    title: 'fields.accept_timum_privacy',
    type: 'checkbox',
    validation: yup
      .boolean()
      .required('validation.field_required')
      .test(
        'privacyAccepted',
        'validation.privacy_field_required',
        (value) => value === true,
      ),
  },
  locale: {
    index: 9,
    preventRendering: true,
  },
},
```

<a name=”custom-fields”></a>

##### Custom Fields

If the values retrieved via custom fields need to be transferred to timum backend as additional booking information, set the parameter `sendCustomValuesInMessage`. With this, the values of custom fields are attached to the customer message. They will be visible to the customer as well in booking confirmation mailings.

`sendCustomValuesInMessage: true, // necessary to transmit custom field values as part of the customer message`

Anatomy of a custom booking form field:

`<fieldName> : {`

  <blockquote>
      <code>index</code>: number, index defines the order in which the fields get displayed.
  (If you want to reorder the default fields or
  want to inject a custom field between them,
  you must redefine them as custom fields and manually change their indexes.)
  </blockquote>
  <br>
    <blockquote>
    <code>title</code>: string or JSXElement;
  validation: yup based validation. See <a href="https://github.com/jquense/yup">yup docu</a>. Re-exported and accessible via timum.yup

  </blockquote>
  <br>
    <blockquote>
    <code>type</code>: string; either 'text' (default), 'phoneNumber', 'textarea', 'checkbox' or 'select'. Depending on the chosen type additional properties are available. See below. 
  </blockquote>
  <br>
    <blockquote>
    <code>onChange</code>: function, allows to execute code whenever the user changes the fields value. Gets 'value' as function parameter.
  </blockquote>
  <br>
    <blockquote>
    <code>prefilled</code>: string; value a field is initiated with.
                            Note that for fields of type 'select' you must use the 'key' of one of it's options.
  </blockquote>
  <br>
  <blockquote>
    <code>preventRendering</code>: boolean, default false;
    if true the field is not visible to the user.
    Useful in conjunction with sendCustomValuesInMessage allowing you to
    enrich booked appointments with internal data
    without the customer becoming aware of your internal processes.
  </blockquote>
  <br>
  <blockquote>
    <code>preventRenderingFor</code>: array, prevents rendering of the field on the set booking screens. Valid values are
    'identifiedCustomers' or 'unknownCustomers', which prevents rendering on the booking
    screen shown to identified/unidentified customers, respectively. If both
    'identifiedCustomers' and 'unknownCustomers' are set the behaviour is equal to
    preventRendering.
    Note that fields hidden in this manner get their validation disabled. This
    ensures that the booking doesn't fail with a validation error the end user is unable to fix.
  </blockquote>
`}`

fieldName

- define a field name

Depending on the type there are additional properties you can/must specify:

- Type `text`:

- `format`: string; the native input field's 'type' (e.g. 'email', 'number', etc.).

- Type `phoneNumber`:

- does NOT support `validation`. Phone number validation is complex, so timum handles it for you.
  This does mean that field validation localisation is currently not supported for fields of this type.
  This will be fixed in a future update.
- isRequired: boolean; if true, this field must be filled with a valid number
- defaultCountry: string, denotes the country code which is preselected when first rendering the component. Default is 'DE',
- preferredCountries: list of strings; denotes which countries are displayed first in the country drop down. Defaults are ['DE', 'CH', 'AT'],

- Type `textArea`:

- limit: number; sets the maximum number of characters customers can enter.

- Type `checkbox`:
- no special properties.

Type `select`:

- options: array of objects with structure { title: string, key: string }. The title is displayed and the key is passed in the data object to callbacks.

<a name=”custom-field-example”></a>

### Custom Field Example

Here is an example tying all of the aforementioned concepts together.
And [here](https://jsfiddle.net/timum/wov6pLk7/) is a fiddle containing the very example detailed in this chapter.

<a name=”the-scenario”></a>

#### The Scenario

It is necessary to determine the gender of customers for a specific purpose. The `mobile` and `message` fields are not necessary and will be removed. The new `gender` field must be filled, therefore a validation check is required to ensure it is completed. Both the name of the `gender` field and the validation messages must be translated into different languages.

<a name=”the-implementation”></a>

#### The Implementation

This is the base configuration. As it uses the standard field configuration shown in [Booking form fields](#bookingFormFields) :

```html
<div id="bookingjs" style="margin: 32px"></div>
<script type="module">
  import * as timum from 'https://cdn.timum.de/bookingjs/1/booking.js';

  timum.init({ ref: 'booking-widget-demo-resource@timum' });
</script>
```

Let's add the necessary changes:

```html
<div id="bookingjs" style="margin: 32px"></div>
<script type="module">
  import * as timum from 'https://cdn.timum.de/bookingjs/1/booking.js';

  timum.init({
    ref: 'booking-widget-demo-resource@timum',

    fields: {
      // set these fields to undefined explicitly in order to remove them
      message: undefined,
      mobile: undefined,
      firstName: {
        // index defines the order in which the fields get displayed
        index: 0,
        title: 'fields.firstName',
        validation: yup.string().required('validation.field_required') // <- compare with key in localisation
      },
      lastName: {
        index: 1,
        title: 'fields.lastName',
        validation: yup.string().required('validation.field_required')
      },
      email: {
        index: 2,
        title: 'fields.email',
        format: 'email',
        type: 'text',
        validation: yup
          .string()
          .email('validation.email_field_must_be_valid')
          .required('validation.field_required')
      },
      gender: {
        index: 3,
        // new language key
        title: 'fields.gender.title',
        // best for a limited number of predefined choices
        type: 'select',
        // could use 'validation.field_required' but
        // for this example let's create a new  key
        validation: yup.string().required('validation.gender_field_required'),
        options: [
          { key: 'm', title: 'fields.gender.male' },
          { key: 'w', title: 'fields.gender.female' },
          { key: 'd', title: 'fields.gender.other' }
        ]
      },
      agbs: {
        index: 4,
        title: 'Datenschutzbestimmungen gelesen und akzeptiert.',
        type: 'checkbox',
        validation: yup
          .boolean()
          .required('validation.field_required')
          .test(
            'privacyAccepted',
            'validation.privacy_field_required',
            (value) => value === true
          )
      }
    },
    sendCustomValuesInMessage: true, // necessary to transmit custom field values as part of the customer message

    // now we need to add the new translation keys and their translations.

    localization: {
      de: {
        validation: {
          gender_field_required: 'Bitte wählen.'
        },
        fields: {
          gender: {
            title: 'Geschlecht',
            male: 'Männlich',
            female: 'Weiblich',
            other: 'Divers'
          }
        }
      },
      en: {
        validation: {
          gender_field_required: 'Please select an option.'
        },
        fields: {
          gender: {
            title: 'Gender',
            male: 'Male',
            female: 'Female',
            other: 'Others'
          }
        }
      }
    },

    // the following callback consumes the customers input, allowing you to act on
    //the entered gender value.
    callback: {
      createBookingStarted: ({ data }) => {
        console.log(data.gender);
      }
    }
  });
</script>
```
