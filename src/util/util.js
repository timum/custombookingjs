export function pxToRem(pixelValue) {
  const currentFontSizeInPixels = parseFloat(
    getComputedStyle(document.documentElement).fontSize,
  );
  const remValue = pixelValue / currentFontSizeInPixels;
  return remValue;
}

export function determineType(value) {
  if (typeof value === 'number') {
    if (Number.isInteger(value)) {
      return 'integer';
    } else {
      return 'double'; // All non-integer numbers are considered double; js only knows double
    }
  }
  return typeof value; // For non-number types
}
