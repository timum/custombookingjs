import { css } from '@emotion/react';

export default css`
  :root {
    --fc-border-color: #efefef !important;
    --fc-button-text-color: #2c2f34 !important;
    --fc-button-bg-color: transparent !important;
    --fc-button-border-color: #d0d0d0 !important;
    --fc-button-hover-bg-color: #e2f0fc !important;
    --fc-button-active-bg-color: #94adc3 !important;
    --fc-event-bg-color: #fff !important;
    --fc-event-text-color: #555 !important;
    --fc-event-border-color: #fff !important;
    --fc-today-bg-color: transparent !important;
    --fc-event-shadow-color: #d0d0d0 !important;
  }

  .fc {
    font-size: 1rem !important;
  }
  .fc .fc-today-button {
    border: 0px !important;
  }
  .fc .fc-prev-button {
    border: 0px !important;
  }
  .fc .fc-next-button {
    border: 0px !important;
  }
  .fc .fc-button-primary:focus {
    box-shadow: 0 0 0 0rem #4c5b6a80 !important;
  }
  .fc .fc-toolbar.fc-header-toolbar {
    flex-direction: row !important; /* for alle view port sizes */
    margin-bottom: 0.3em !important;
  }
  .fc .fc-toolbar-title {
    font-size: 1.2em !important;
  }
  @media (max-width: 767px) {
    .fc-toolbar.fc-header-toolbar {
      font-size: 80% !important;
    }
  }
  @media (max-width: 767px) {
    .fc-footer-toolbar.fc-toolbar {
      font-size: 80% !important;
      margin-top: 0.5em !important;
    }
  }

  .fc .fc-col-header-cell-cushion {
    /*column label*/
    display: inline-block !important;
    padding: 2px 4px !important;
    font-size: 0.8em !important;
    color: #888 !important;
    font-weight: normal !important;
  }
  .fc .fc-timegrid-slot-label-cushion {
    /*row label*/
    font-size: 0.8em !important;
    color: #888 !important;
  }
  .fc .fc-timegrid-slot-minor {
    border-top-width: 0px !important;
  }
  .fc .fc-timegrid-slot-lane {
    border-top-width: 1px !important;
    border-top-color: #f1f1f1 !important;
    border-top-style: dotted !important;
  }
  .fc .fc-v-event {
    cursor: pointer !important;
    box-shadow: 0px 0px 3px 1px var(--fc-event-shadow-color) !important;
  }
  .fc .fc-v-event:hover {
    cursor: pointer !important;
    background-color: var(--fc-button-hover-bg-color) !important;
    border: 1px solid var(--fc-button-hover-bg-color) !important;
    box-shadow: 0px 0px 3px 1px (--fc-event-shadow-color) !important;
  }

  .fc .fc-v-event .fc-event-time {
    width: 100% !important;
    padding-left: 4px !important;
  }

  .fc .fc-v-event .fc-event-main {
    container-type: size !important;
    container-name: custom-event-container !important;
  }

  .fc .fc-event-title {
    text-align: center !important;
    width: 100% !important;
  }

  @container custom-event-container (height < 22px) {
    .fc .fc-v-event .fc-event-time {
      line-height: 1em !important;
      font-size: 0.8em !important;
    }
  }
  @container custom-event-container (width < 15px) {
    .fc .fc-v-event .fc-event-time {
      line-height: 1em !important;
      font-size: 0.7em !important;
    }
  }

  .fc .fc-v-event .fc-event-time:after {
    display: none !important;
  }

  .fc .fc-v-event .fc-event-title {
    display: none !important;
  }
`;
// @media screen and (max-width: 601px) {
//   .fc-toolbar.fc-header-toolbar {
//     font-size: 0.8em;
//     flex-direction: column;
//   }
//   .fc-toolbar-chunk {
//     display: table-row;
//     text-align: center;
//     padding: 5px 0;
//   }
// } ;
