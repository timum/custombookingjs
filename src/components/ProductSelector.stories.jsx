import ProductSelector from './ProductSelector';
import { rest } from 'msw';
import TimumApiMockResponses from '../TimumApiMockResponses';
import { faker } from '@faker-js/faker';

export default {
  component: ProductSelector,
  title: 'ProductSelector',
};

export const Default = {
  args: {},

  parameters: {
    msw: {
      handlers: {
        getActiveProducts: rest.get(
          '*/rest/1/resources/:uuid/active_products*',
          (req, res, ctx) => {
            const json = {
              products: [
                {
                  uuid: faker.datatype.uuid(),
                  name: 'Besichtigung',
                  description:
                    "<p><b>Bei Interesse an einer Wohnung</b>, finden Sie weitere Informationen zu den nächsten Schritten, sowie die Selbstauskunft in folgendem Download:</p> <a href='custom.timum.de/tk-Immo_selbstauskunft-fuer-mietinteressenten-5.pdf'>Selbstauskunft für Mitinteressenten (pdf)</a>",
                  minDuration: undefined,
                  exclusive: false,
                },
                ...TimumApiMockResponses.getProductResponse(3).products,
              ],
            };

            Default.args.currentProducts = [...json.products];

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const NoProducts = {
  args: {},

  parameters: {
    msw: {
      handlers: {
        getActiveProducts: rest.get(
          '*/rest/1/resources/:uuid/active_products*',
          (req, res, ctx) => {
            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(TimumApiMockResponses.getProductResponse(0)),
            );
          },
        ),
      },
    },
  },
};

export const Loading = {
  args: {
    ...Default.args,
  },

  parameters: {
    msw: {
      handlers: {
        getActiveProducts: rest.get(
          'https://www.timum.de/rest/1/resources/:uuid/active_products*',
          (req, res, ctx) => {
            return res(
              ctx.status(200),
              ctx.delay(100000),
              ctx.json(TimumApiMockResponses.getProductResponse(0)),
            );
          },
        ),
      },
    },
  },
};

export const SuperLongDescriptions = {
  args: {
    ...Default.args,
  },

  parameters: {
    msw: {
      handlers: {
        getActiveProducts: rest.get(
          'https://www.timum.de/rest/1/resources/:uuid/active_products*',
          (req, res, ctx) => {
            const prods = TimumApiMockResponses.getProductResponse(10);

            for (const prod of prods.products) {
              prod.description +=
                faker.commerce.productDescription() +
                faker.commerce.productDescription();
            }

            return res(ctx.status(200), ctx.delay(1000), ctx.json(prods));
          },
        ),
      },
    },
  },
};

export const SingleProduct = {
  args: {
    ...Default.args,
  },

  parameters: {
    msw: {
      handlers: {
        getActiveProducts: rest.get(
          'https://www.timum.de/rest/1/resources/:uuid/active_products*',
          (req, res, ctx) => {
            const prods = TimumApiMockResponses.getProductResponse(1);

            return res(ctx.status(200), ctx.delay(1000), ctx.json(prods));
          },
        ),
      },
    },
  },
};
