import React, { useEffect, useMemo, useCallback, createContext } from 'react';
// import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import useMediaQuery from '@mui/material/useMediaQuery';

import {
  useLazyUpcomingBookablesQuery,
  useLazyActiveProductsQuery,
  useReserveAppoinmentMutation,
  useRevokeAppointmentReservationMutation,
} from '@timum/timum_pdk';

import { isBoolean, isNil } from 'lodash';
import {
  selectSelectedProduct,
  selectPublicData,
  selectAppConfigProp,
  selectProduct,
  mutateAppConfig,
  selectSelectedResource,
  selectResToPublicDataMap,
  selectSelectedPublicData,
  selectIsIdentifyingCustomer,
  selectCustomerData,
  selectCustomerIdentificationFailed,
  selectIsWaitingForConf,
} from '../slices/appState';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';

import { useSnackbar } from 'notistack';
import useWindowDimensions from '../hooks/useWindowDimensions';

import ProgressBar from './ProgressBar';
import Footer from './Footer';

import { DateTime } from 'luxon';

import ResourceSelector from './ResourceSelector';
import { useTranslation } from 'react-i18next';

export const ProductPageContext = createContext();
export const CustomerFormContext = createContext();
export const CancelPageContext = createContext();
export const ConfirmationPageContext = createContext();
export const CalendarViewContext = createContext();
export const LoginPageContext = createContext();
export const ErrorContext = createContext();

const calendarActions = {
  setBookables: 'setBookables',
  setProducts: 'setProducts',
  resetBookables: 'resetBookables',
  resetProducts: 'resetProducts',
};

function CalendarReducer(state, { type, payload }) {
  switch (type) {
    case calendarActions.setBookables: {
      return { ...state, bookables: payload };
    }
    case calendarActions.setProducts: {
      return { ...state, products: payload };
    }
    case calendarActions.resetBookables: {
      return { ...state, products: undefined };
    }
    case calendarActions.resetProducts: {
      return { ...state, products: undefined };
    }
    default: {
      throw new Error(`Unsupported type: ${type}`);
    }
  }
}

export default function Calendar({
  children,
  reducer = CalendarReducer,
  bookables: controlledBookables,
  products: controlledProducts,
  forceResourceSelectorDialog,
}) {
  /***********************
   * STATE
   ***********************/
  const { t } = useTranslation();
  const { current: initialState } = React.useRef({
    bookables: undefined,
    products: undefined,
  });
  const [state, dispatchReducer] = React.useReducer(reducer, initialState);
  const dispatch = useDispatch();
  const refPool = useSelector((state) => selectAppConfigProp(state, 'ref'));
  const product = useSelector(selectSelectedProduct);
  const resource = useSelector(selectSelectedResource);
  const publicData = useSelector(selectSelectedPublicData);
  const publicDataMap = useSelector(selectResToPublicDataMap);
  const customerData = useSelector(selectCustomerData);
  const setIsWaitingForConf = useSelector(selectIsWaitingForConf);
  const isIdentifyingCustomer = useSelector(selectIsIdentifyingCustomer);
  const identificationFailed = useSelector(selectCustomerIdentificationFailed);
  const showAllBookables = useMemo(() => {
    return publicData?.resource?.uuid === 'all-option';
  }, [publicData?.resource?.uuid]);
  const { height: windowHeight } = useWindowDimensions();
  const isSmallScreen = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const bookablesAreControlled = controlledBookables != null;
  const productsAreControlled = controlledProducts != null;
  const bookables = bookablesAreControlled
    ? controlledBookables
    : state.bookables;
  const products = productsAreControlled ? controlledProducts : state.products;
  const { enqueueSnackbar } = useSnackbar();

  const channelKey = useSelector((state) =>
    selectAppConfigProp(state, 'channelKey'),
  );

  const additionalParams = useSelector((state) =>
    selectAppConfigProp(state, 'additionalParams'),
  );

  const cancelableAppointmentUuid = useSelector((state) =>
    selectAppConfigProp(state, 'cancelableAppointment.appointmentUuid'),
  );

  const defaultHeight = useSelector((state) =>
    selectAppConfigProp(state, 'height'),
  );

  const allowCloseOnBooking = useSelector((state) =>
    selectAppConfigProp(state, 'allowCloseOnBooking'),
  );

  const hideTimumFooter = useSelector((state) =>
    selectAppConfigProp(state, 'hideTimumFooter'),
  );

  const height =
    windowHeight && windowHeight < defaultHeight ? windowHeight : defaultHeight;

  const authToken = useSelector((state) =>
    selectAppConfigProp(state, 'cancelableAppointment.authToken'),
  );

  const fetchingProductsFailed = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.fetchingProductsFailed'),
  );

  const fetchingBookablesFailed = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.fetchingBookablesFailed'),
  );

  const fetchingProductsSucceeded = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.fetchingProductsSucceeded'),
  );

  const fetchingBookablesSucceeded = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.fetchingBookablesSucceeded'),
  );

  const fetchingPublicDataSucceeded = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.fetchingPublicDataSucceeded'),
  );

  const openedProductSelection = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.openedProductSelection'),
  );

  const openedResourceSelection = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.openedResourceSelection'),
  );

  const openedBookingPage = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.openedBookingPage'),
  );

  const openedCancelPage = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.openedCancelPage'),
  );

  const openedConfirmationPage = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.openedConfirmationPage'),
  );

  const closedProductSelection = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.closedProductSelection'),
  );

  const closedResourceSelection = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.closedResourceSelection'),
  );

  const closedBookingPage = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.closedBookingPage'),
  );

  const closedCancelPage = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.selectAppConfigProp'),
  );

  const closedConfirmationPage = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.closedConfirmationPage'),
  );

  const [isProductSelectionOpen, setProductSelection] = React.useState(false);
  const [isBookingPageOpen, setBookingPageOpen] = React.useState(false);
  const [isCancelPageOpen, setCancelPageOpen] = React.useState(false);
  const [wasCancelPageOpen, setWasCancelPageOpen] = React.useState(false); //<- so we don't reopen the page if user closed it manually
  const [isErrorDialogOpen, setErrorDialogOpen] = React.useState(false);
  const [isLoginPageOpen, setLoginPageOpen] = React.useState(false);
  const [isResourceSelectionOpen, setResourceSelectionOpen] =
    React.useState(false);
  const [isConfirmationPageOpen, setConfirmationPageOpen] =
    React.useState(false);

  const [clickedEvent, setClickedEvent] = React.useState();
  const [clickedCancelableEvent, setClickedCancelableEvent] = React.useState();
  const [loginPagePrefilledNamed, setLoginPagePrefilledName] = React.useState();

  // used by ConfirmationPage
  const [bookedEvent, setBookedEvent] = React.useState();
  const [bookingResponse, setBookingResponse] = React.useState();
  const [inputData, setInputData] = React.useState();

  /* Interfaces */

  const ResourcePage = useSelector((state) =>
    selectAppConfigProp(state, 'interfaces.ResourcePage'),
  );

  /***********************
   * QUERIES
   ***********************/

  const [
    reserveAppointment,
    { data: reservation, error: reservationFailed, reset: resetReservation },
  ] = useReserveAppoinmentMutation({
    fixedCacheKey: 'reserveAppointmentCache',
  });

  const [revokeReservation] = useRevokeAppointmentReservationMutation();

  const [
    fetchBookables,
    {
      data: bookablesData,
      error: bookablesError,
      isFetching: isFetchingBookables,
      isLoading: isLoadingBookables,
      isUninitialized: isFetchingBookablesUninitialized,
    },
  ] = useLazyUpcomingBookablesQuery();

  const [
    fetchProducts,
    { data: productsData, error: productsError, isLoading: isFetchingProducts },
  ] = useLazyActiveProductsQuery();

  /***********************
   * UTIL
   ***********************/

  /**
   * Removes the useless 'public_visible' prop.
   * Also parses start and end to luxon DateTimes without timezone information
   * Preserves original start and end values, storing them in untouchedStart and untouchedEnd respectively.
   * @param {} bookables
   */
  const preProcessBookables = useCallback((bookables, product) => {
    if (!bookables) return {};

    const parsedBookables = JSON.parse(JSON.stringify(bookables));
    delete parsedBookables.public_visible;

    for (const [date, timeslots] of Object.entries(parsedBookables)) {
      parsedBookables[date] = [];
      for (const timeslot of timeslots) {
        parsedBookables[date].push(preProcessBookbale(timeslot, product));
      }
    }

    return parsedBookables;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // todo: instead of sending back untouchedEnd we instead want to adjust
  // the end based on the chosen product's min duration (if any) or the product's max duration as follows:
  // if product has no min/max duration -> use untouchedEnd
  // if product has minDuration -> add min duration to untouched start
  // if product has maxDuration -> add max duration to untouched start (bonus: check if untouchedEnd is before start + maxDuration and use untouchedEnd if so)
  // if product has both min and max -> add min duration to untouched start
  const preProcessBookbale = useCallback(
    (bookable, product) => {
      const parsedBookable = JSON.parse(JSON.stringify(bookable));
      parsedBookable.isCancelable =
        cancelableAppointmentUuid &&
        parsedBookable.appointment_uuid === cancelableAppointmentUuid;

      parsedBookable.appointmentUuid = parsedBookable.appointment_uuid;
      parsedBookable.uuid = parsedBookable.timeslot_uuid;
      parsedBookable.start = DateTime.fromISO(parsedBookable.start);

      if (
        parsedBookable.kind?.toLowerCase().includes('bookable') && // only adjust for availabilities; appointments are fixed
        product?.minDuration
      ) {
        parsedBookable.end = parsedBookable.start.plus({
          minutes: product.minDuration,
        });
      } else if (
        parsedBookable.kind?.toLowerCase().includes('bookable') &&
        product?.maxDuration
      ) {
        parsedBookable.end = parsedBookable.start.plus({
          minutes: product.maxDuration,
        });
      } else {
        parsedBookable.end = DateTime.fromISO(parsedBookable.end);
      }

      return parsedBookable;
    },
    [cancelableAppointmentUuid],
  );

  const fetchBookablesForSelectedRes = useCallback(() => {
    const fetch = async () => {
      fetchBookables({
        channelOrResourceId: publicData?.ref,
        params: {
          channelKey: channelKey,
          ...additionalParams,
        },
        headers: authToken ? { 'X-SIGNED-APP-CUSTOMER': authToken } : undefined,
      });
    };

    if (!showAllBookables && !bookablesAreControlled && publicData) {
      fetch();
    }
  }, [
    additionalParams,
    authToken,
    bookablesAreControlled,
    channelKey,
    fetchBookables,
    publicData,
    showAllBookables,
  ]);

  const fetchBookablesForAllRes = useCallback(() => {
    const fetch = async () => {
      fetchBookables({
        channelOrResourceId: refPool[0],
        params: {
          ref: refPool.slice(1),
          channelKey: channelKey,
          ...additionalParams,
        },
        headers: authToken ? { 'X-SIGNED-APP-CUSTOMER': authToken } : undefined,
      });
    };

    if (showAllBookables && !bookablesAreControlled) {
      fetch();
    }
  }, [
    additionalParams,
    authToken,
    bookablesAreControlled,
    channelKey,
    fetchBookables,
    refPool,
    showAllBookables,
  ]);

  /***********************
   * STATE MANAGEMENT
   ***********************/

  const handleOpenResourceSelectionPage = useCallback(() => {
    setResourceSelectionOpen(true);

    if (openedResourceSelection) {
      openedResourceSelection();
    }
  }, [openedResourceSelection]);

  const handleCloseResourceSelectionPage = () => {
    setResourceSelectionOpen(false);

    if (closedResourceSelection) {
      closedResourceSelection();
    }
  };

  const handleOpenProductPage = useCallback(() => {
    setProductSelection(true);

    if (openedProductSelection) {
      openedProductSelection();
    }
  }, [openedProductSelection]);

  const handleCloseProductPage = useCallback(() => {
    setProductSelection(false);

    if (closedProductSelection) {
      closedProductSelection();
    }
  }, [closedProductSelection]);

  const handleOpenLoginPage = (userName) => {
    setLoginPagePrefilledName(userName);
    setLoginPageOpen(true);
  };

  const handleCloseLoginPage = () => {
    setLoginPagePrefilledName(undefined);
    setLoginPageOpen(false);
  };

  const handleOpenBookingPage = useCallback(
    (event) => {
      setBookingPageOpen(true);
      setClickedEvent(event);

      if (showAllBookables) {
        const data = Object.values(publicDataMap).find((data) => {
          return data.resource.uuid === event.resource_uuid;
        });

        dispatch(selectPublicData(data));
      }

      if (openedBookingPage) {
        openedBookingPage({ ...event });
      }
    },
    [dispatch, openedBookingPage, publicDataMap, showAllBookables],
  );

  const handleCloseBookingPage = useCallback(() => {
    setBookingPageOpen(false);

    if (isNil(reservationFailed) && reservation) {
      revokeReservation({
        channelOrResourceId: publicData?.ref,
        body: {
          placeholder_id: reservation?.participation?.customer_uuid,
          appointment_id: reservation?.participation?.appointment_uuid,
        },
        params: {
          ...additionalParams,
        },
      });
    }

    if (closedBookingPage) {
      closedBookingPage({ ...clickedEvent });
    }

    // let the fade animation finish before setting this
    setTimeout(() => {
      setClickedEvent(undefined);
    }, 150);
  }, [
    additionalParams,
    clickedEvent,
    closedBookingPage,
    publicData?.ref,
    reservation,
    reservationFailed,
    revokeReservation,
  ]);

  const handleOpenConfirmationPage = useCallback(() => {
    setConfirmationPageOpen(true);

    if (openedConfirmationPage) {
      openedConfirmationPage();
    }
  }, [openedConfirmationPage]);

  const handleCloseConfirmationPage = useCallback(() => {
    setConfirmationPageOpen(false);
    setBookedEvent(undefined);
    setBookingResponse(undefined);
    setInputData(undefined);

    fetchBookablesForSelectedRes();

    if (closedConfirmationPage) {
      closedConfirmationPage();
    }
  }, [closedConfirmationPage, fetchBookablesForSelectedRes]);

  const handleOpenCancelPage = useCallback(
    (event) => {
      setCancelPageOpen(true);
      setClickedCancelableEvent(event);

      if (openedCancelPage) {
        openedCancelPage({ ...event });
      }
    },
    [openedCancelPage],
  );

  const handleCloseCancelPage = useCallback(() => {
    setCancelPageOpen(false);
    // let the fade animation finish before setting this
    setTimeout(() => {
      setClickedCancelableEvent(undefined);
    }, 150);

    if (closedCancelPage) {
      closedCancelPage({ ...clickedEvent });
    }
  }, [clickedEvent, closedCancelPage]);

  const handleCancelSuccess = useCallback(() => {
    dispatch(
      mutateAppConfig({
        pathToProp: 'cancelableAppointment',
        newValue: undefined,
      }),
    );
    handleCloseCancelPage();

    if (closedCancelPage) {
      closedCancelPage();
    }
  }, [closedCancelPage, dispatch, handleCloseCancelPage]);

  const handleOpenErrorDialog = () => {
    setErrorDialogOpen(true);
  };

  const handleCloseErrorDialog = useCallback(() => {
    setErrorDialogOpen(false);
    resetReservation(); // reset reservation so user may try a new bookable without issue
  }, [setErrorDialogOpen, resetReservation]);

  /***********************
   * USE MEMO / EFFECT
   ***********************/

  useEffect(() => {
    fetchBookablesForSelectedRes();
  }, [fetchBookablesForSelectedRes]);

  /**
   * Fetch bookables in the showAllBookables scenario
   * This doesn't trigger for publicData changes.
   */
  useEffect(() => {
    fetchBookablesForAllRes();
  }, [fetchBookablesForAllRes]);

  // we dispatch in a different useEffect so that the results of automatic refetches
  // can be properly dispatched. Otherwise, after the initial request, we'd never update the interface again
  useEffect(() => {
    if (!bookablesAreControlled && !bookablesError) {
      dispatchReducer({ type: 'setBookables', payload: bookablesData });
    }
  }, [bookablesData, bookablesAreControlled, bookablesError]);

  useEffect(() => {
    const fetch = async () => {
      fetchProducts({
        channelOrResourceId: publicData?.ref,
        params: {
          channelKey: channelKey,
          ...additionalParams,
        },
      });
    };

    if (!showAllBookables && !productsAreControlled && publicData) {
      fetch();
    }
  }, [
    publicData,
    productsAreControlled,
    fetchProducts,
    channelKey,
    additionalParams,
    showAllBookables,
  ]);

  useEffect(() => {
    const fetch = async () => {
      fetchProducts({
        channelOrResourceId: refPool[0],
        params: {
          ref: refPool.slice(1),
          channelKey: channelKey,
          ...additionalParams,
        },
      });
    };

    if (showAllBookables && !productsAreControlled) {
      fetch();
    }
  }, [
    productsAreControlled,
    fetchProducts,
    channelKey,
    additionalParams,
    showAllBookables,
    refPool,
  ]);

  // we dispatch in a different useEffect so that the results of automatic refetches
  // can be properly dispatched. Otherwise, after the initial request, we'd never update the interface again
  useEffect(() => {
    if (!productsAreControlled && !productsError) {
      dispatchReducer({
        type: 'setProducts',
        payload: productsData?.products,
      });
    }
  }, [
    productsData,
    bookablesAreControlled,
    productsAreControlled,
    productsError,
  ]);

  useEffect(() => {
    if (clickedEvent) {
      reserveAppointment({
        channelOrResourceId: publicData?.ref,
        body: {
          timeslotUuid: clickedEvent?.timeslot_uuid,
          appointmentUuid: clickedEvent?.appointment_uuid,
          productUuid: product.uuid,
          from: clickedEvent.start
            .set({ milliseconds: 0 })
            .toUTC(0, {
              keepLocalTime: true,
            })
            .toISO({
              suppressMilliseconds: true,
            }),
          to: clickedEvent.end
            .set({ milliseconds: 0 })
            .toUTC(0, {
              keepLocalTime: true,
            })
            .toISO({
              suppressMilliseconds: true,
            }),
        },
        params: {
          channelKey: channelKey,
          ...additionalParams,
        },
      });
    }
  }, [
    channelKey,
    clickedEvent,
    product?.uuid,
    publicData?.ref,
    reserveAppointment,
    additionalParams,
  ]);

  useMemo(() => {
    if (reservationFailed) {
      handleOpenErrorDialog();
      handleCloseBookingPage();
    }
  }, [handleCloseBookingPage, reservationFailed]);

  useMemo(() => {
    if (
      !isCancelPageOpen &&
      !wasCancelPageOpen &&
      bookables &&
      cancelableAppointmentUuid &&
      authToken
    ) {
      let cancelableAppointment = undefined;
      for (const tsls of Object.values(bookables)) {
        if (!isBoolean(tsls) && !cancelableAppointment) {
          cancelableAppointment = tsls.find(
            (tsl) => tsl.appointment_uuid === cancelableAppointmentUuid,
          );
        }
      }

      if (cancelableAppointment) {
        setClickedCancelableEvent(preProcessBookbale(cancelableAppointment));
        setCancelPageOpen(true);
        setWasCancelPageOpen(true);
      }
    }
  }, [
    isCancelPageOpen,
    wasCancelPageOpen,
    bookables,
    cancelableAppointmentUuid,
    authToken,
    preProcessBookbale,
  ]);

  useMemo(() => {
    if (productsError && fetchingProductsFailed) {
      fetchingProductsFailed(productsError);
    }
  }, [productsError, fetchingProductsFailed]);

  useMemo(() => {
    if (bookablesError && fetchingBookablesFailed) {
      fetchingBookablesFailed(bookablesError);
    }
  }, [bookablesError, fetchingBookablesFailed]);

  useMemo(() => {
    if (products && fetchingProductsSucceeded) {
      fetchingProductsSucceeded({ ...products });
    }
  }, [products, fetchingProductsSucceeded]);

  useMemo(() => {
    if (bookables && fetchingBookablesSucceeded) {
      fetchingBookablesSucceeded({ ...bookables });
    }
  }, [bookables, fetchingBookablesSucceeded]);

  useMemo(() => {
    if (publicData && fetchingPublicDataSucceeded) {
      fetchingProductsSucceeded({ ...publicData });
    }
  }, [publicData, fetchingPublicDataSucceeded, fetchingProductsSucceeded]);

  // todo: check wether productRef is given and auto select if so
  useMemo(() => {
    if (!product && products?.length === 1) {
      dispatch(selectProduct(products[0]));
    } else if (!product && !isProductSelectionOpen && products?.length > 1) {
      handleOpenProductPage();
    }
  }, [
    product,
    products,
    isProductSelectionOpen,
    dispatch,
    handleOpenProductPage,
  ]);

  useMemo(() => {
    if (product && products) {
      const match = products.find((p) => p.uuid === product.uuid);
      if (!match) {
        dispatch(selectProduct(undefined));
        enqueueSnackbar(t('product_unavailable_for_resource_warning'), {
          autoHideDuration: 6000,
          variant: 'warning',
        });
      }
    }
  }, [product, products, dispatch, enqueueSnackbar, t]);

  const isLoading = useMemo(() => {
    return (
      isFetchingProducts ||
      isFetchingBookables ||
      isIdentifyingCustomer ||
      setIsWaitingForConf
    );
  }, [
    isFetchingBookables,
    isFetchingProducts,
    isIdentifyingCustomer,
    setIsWaitingForConf,
  ]);

  const bookingPageState = useMemo(
    () => ({
      identifiedOpen:
        isBookingPageOpen && !identificationFailed && !isIdentifyingCustomer,
      unknownOpen:
        isBookingPageOpen && identificationFailed && !isIdentifyingCustomer,
      onClose: handleCloseBookingPage,
      timeslot: (() => {
        // this is kinda hacky.
        // after successful reservation we have created an appointment with a new timeslot
        // we need to excahnge the uuid here so we book the customer to the new appointment instead the underlying availability
        // (which wouldn't work anyway bc of collision)
        if (
          clickedEvent &&
          clickedEvent.uuid !== reservation?.participation?.timeslot_uuid
        ) {
          clickedEvent.uuid = reservation?.participation?.timeslot_uuid;
        }
        return clickedEvent;
      })(),
      productName: clickedEvent?.product_name
        ? clickedEvent.product_name
        : product?.name,
      resourceName: publicData?.resource?.name,
      placeholderId: reservation?.participation?.customer_uuid,
      bookingProcess: publicData?.channel?.bookingProcess,
      customerData: customerData,
      onBookingSuccessfull: ({ timeslot, response, data }) => {
        resetReservation(); //<- reset reservation result - we otherwise delete the booking we just created
        handleCloseBookingPage();
        handleOpenConfirmationPage();
        setBookingResponse(response?.data?.createdAppointment);
        setBookedEvent(timeslot);
        setInputData(data);
      },
      onBookingFailed: ({ response, data }) => {
        if (response?.error?.data?.errors[0]?.errorCode === '300')
          handleOpenLoginPage(data.email);
      },
    }),
    [
      clickedEvent,
      customerData,
      handleCloseBookingPage,
      handleOpenConfirmationPage,
      identificationFailed,
      isBookingPageOpen,
      isIdentifyingCustomer,
      product?.name,
      publicData?.channel?.bookingProcess,
      publicData?.resource?.name,
      reservation?.participation?.customer_uuid,
      reservation?.participation?.timeslot_uuid,
      resetReservation,
    ],
  );

  const productPageState = useMemo(
    () => ({
      products,
      selectedProduct: product,
      open: isProductSelectionOpen && !isCancelPageOpen,
      onClose: handleCloseProductPage,
      selectProduct: (product) => {
        dispatch(selectProduct(product));
      },
    }),
    [
      dispatch,
      handleCloseProductPage,
      isCancelPageOpen,
      isProductSelectionOpen,
      product,
      products,
    ],
  );

  const cancelPageState = useMemo(
    () => ({
      open: isCancelPageOpen,
      onClose: handleCloseCancelPage,
      timeslot: clickedCancelableEvent,
      productName: clickedCancelableEvent?.product_name
        ? clickedCancelableEvent.product_name
        : product?.name,
      resourceName: publicData?.resource?.name,
      onCancelSuccess: handleCancelSuccess,
    }),
    [
      clickedCancelableEvent,
      handleCancelSuccess,
      handleCloseCancelPage,
      isCancelPageOpen,
      product?.name,
      publicData?.resource?.name,
    ],
  );

  const confirmationPageState = useMemo(
    () => ({
      open: isConfirmationPageOpen,
      onClose: handleCloseConfirmationPage,
      allowCloseOnBooking: allowCloseOnBooking,
      timeslot: bookedEvent,
      productName: bookedEvent?.product_name
        ? bookedEvent.product_name
        : product?.name,
      contactName: publicData?.contact?.name,
      contactEmail: publicData?.contact?.email,
      contactPhone: publicData?.contact?.mobile
        ? publicData?.contact?.mobile
        : publicData?.contact?.phone,
      productDescription: product?.description,
      resourceName: publicData?.resource?.name,
      resourceDescription: publicData?.resource?.description,
      providerName: publicData?.provider?.isThemingAllowed
        ? publicData?.provider?.name
        : undefined,
      customerMail: customerData ? customerData.email : inputData?.email,
      contactChannel: bookingResponse?.contact_channel,
      cancelLink: bookingResponse?.cancelLink,
      bookingProcess: publicData?.channel?.bookingProcess,
    }),
    [
      allowCloseOnBooking,
      bookedEvent,
      bookingResponse?.cancelLink,
      bookingResponse?.contact_channel,
      customerData,
      handleCloseConfirmationPage,
      inputData?.email,
      isConfirmationPageOpen,
      product?.description,
      product?.name,
      publicData?.channel?.bookingProcess,
      publicData?.contact?.email,
      publicData?.contact?.mobile,
      publicData?.contact?.name,
      publicData?.contact?.phone,
      publicData?.provider?.isThemingAllowed,
      publicData?.provider?.name,
      publicData?.resource?.description,
      publicData?.resource?.name,
    ],
  );

  const CalendarViewState = useMemo(
    () => ({
      bookablesQueried:
        bookablesAreControlled ||
        (!isLoadingBookables && !isFetchingBookablesUninitialized),
      bookables: preProcessBookables(bookables, product),
      isLoading,
      product: product,
      products: products,
      resource: resource,
      publicDataMap: publicDataMap,
      publicData: publicData,
      height: height - 36, // Subtract footer height
      openResourcePage: handleOpenResourceSelectionPage,
      openProductPage: handleOpenProductPage,
      openBookingPage: (event) => {
        if (event.isCancelable) {
          handleOpenCancelPage(event);
        } else {
          handleOpenBookingPage(event);
        }
      },
    }),
    [
      bookables,
      bookablesAreControlled,
      handleOpenBookingPage,
      handleOpenCancelPage,
      handleOpenProductPage,
      handleOpenResourceSelectionPage,
      height,
      isFetchingBookablesUninitialized,
      isLoading,
      isLoadingBookables,
      preProcessBookables,
      product,
      products,
      publicData,
      publicDataMap,
      resource,
    ],
  );

  const LoginPageState = useMemo(
    () => ({
      open: isLoginPageOpen,
      onClose: handleCloseLoginPage,
      userName: loginPagePrefilledNamed,
      doOnSuccess: handleCloseLoginPage,
    }),
    [isLoginPageOpen, loginPagePrefilledNamed],
  );

  const ErrorState = useMemo(
    () => ({
      open: isErrorDialogOpen,
      onClose: handleCloseErrorDialog,
    }),
    [handleCloseErrorDialog, isErrorDialogOpen],
  );

  /***********************
   * RENDERING
   ***********************/

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
      <Grid
        container
        wrap="nowrap"
        sx={{ flexGrow: 1, boxSizing: 'border-box', overflow: 'hidden' }}
      >
        <ResourcePage
          mobileOpen={isResourceSelectionOpen}
          mobileOnClose={handleCloseResourceSelectionPage}
          publicDataMap={publicDataMap}
          publicData={publicData}
          shouldDisplay={
            publicDataMap &&
            Object.keys(publicDataMap).length > 1 &&
            !forceResourceSelectorDialog
          }
          height={height - 36}
        >
          {isSmallScreen && (
            <ResourceSelector
              onResourceSelected={() => {
                handleCloseResourceSelectionPage();
              }}
              height={height - 40}
              HeadlineProps={{
                variant: 'h2',
              }}
              ListItemProps={{
                sx: {
                  mx: 'auto',
                  borderWidth: '1px',
                  borderStyle: 'solid',
                  borderColor: 'grey.300',
                  marginBottom: '16px',
                  minHeight: '58px',
                },
              }}
              ListItemButtonProps={{
                sx: {
                  position: 'absolute',
                  width: '100%',
                  height: '100%',
                  zIndex: 0,
                },
              }}
            />
          )}
          {!isSmallScreen && (
            <ResourceSelector
              onResourceSelected={() => {
                handleCloseResourceSelectionPage();
              }}
              height={height - 36}
              ListProps={{ sx: { maxWidth: '240px' } }}
              ListItemProps={{}}
              ListItemButtonProps={{}}
            />
          )}
        </ResourcePage>
        <Grid item xs>
          <ProgressBar visible={isLoading}>
            <ProductPageContext.Provider value={productPageState}>
              <CustomerFormContext.Provider value={bookingPageState}>
                <CancelPageContext.Provider value={cancelPageState}>
                  <ConfirmationPageContext.Provider
                    value={confirmationPageState}
                  >
                    <ErrorContext.Provider value={ErrorState}>
                      <LoginPageContext.Provider value={LoginPageState}>
                        <CalendarViewContext.Provider value={CalendarViewState}>
                          {children}
                        </CalendarViewContext.Provider>
                      </LoginPageContext.Provider>
                    </ErrorContext.Provider>
                  </ConfirmationPageContext.Provider>
                </CancelPageContext.Provider>
              </CustomerFormContext.Provider>
            </ProductPageContext.Provider>
          </ProgressBar>
        </Grid>
      </Grid>
      {!hideTimumFooter && <Footer />}
    </Box>
  );
}
