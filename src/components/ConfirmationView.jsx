import * as React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'react-i18next';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import Event from '@mui/icons-material/Event';
import Phonelink from '@mui/icons-material/Phonelink';
import Forum from '@mui/icons-material/Forum';
import PhoneAndroid from '@mui/icons-material/PhoneAndroid';
import TaskAlt from '@mui/icons-material/TaskAlt';
import Adjust from '@mui/icons-material/Adjust';
import Place from '@mui/icons-material/Place';
import HeadsetMic from '@mui/icons-material/HeadsetMic';
import BookmarkBorder from '@mui/icons-material/BookmarkBorder';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

import { MuiMarkdown, getOverrides } from 'mui-markdown';

import { CalendarPlus } from 'react-bootstrap-icons';

import { DateTime } from 'luxon';

import { atcb_action } from 'add-to-calendar-button';

import { ConfirmationPageContext } from './Calendar';
import usePropsBeforeContext from '../hooks/usePropsBeforeContext';

/**
 * Renders a button whith which users may add the appointment to their calendar.
 * Sets start, end, title, location, organizer mail and name, description and timezone.
 *
 * If you consider to change this code, please also consider changing IcsAppointmentRendererForPublic.appointmentAsVEvent().
 * There we also generate event data customers can add to their calendar. The data should be the same.
 * @param appointment
 * @return
 */
function AddToCalendarButton({
  start,
  end,
  contactName,
  contactEmail,
  contactPhone,
  productName,
  productDescription,
  resourceName,
  resourceDescription,
  providerName,
  cancelLink,
  location,
}) {
  const { t } = useTranslation();

  const description = React.useMemo(() => {
    let result = '';

    if (contactName) {
      result += contactName;
    }

    if (contactEmail) {
      result += ' | ';
      result += contactEmail;
    }

    if (contactPhone) {
      result += ' | ';
      result += contactPhone;
    }

    if (result) {
      result += '<br/><br/>';
    }

    if (productName) {
      result += `${productName}`;
    }

    if (productDescription) {
      result += `:<br/>${productDescription}`;
    }

    if (resourceDescription) {
      result += `<br/><br/>${resourceName}`;
    }

    if (resourceDescription) {
      result += `:<br/>${resourceDescription}`;
    }

    if (cancelLink) {
      result += `<br/><br/>[url]${cancelLink}|${t('ics.cancel_link')}[/url]`;
    }

    return result;
  }, [
    contactName,
    contactEmail,
    contactPhone,
    productName,
    productDescription,
    resourceDescription,
    cancelLink,
    resourceName,
    t,
  ]);

  return (
    <Button
      variant="outlined"
      startIcon={<CalendarPlus />}
      onClick={(event) =>
        atcb_action(
          {
            availability: 'busy',
            trigger: 'click',
            listStyle: 'overlay',
            name: `${
              providerName ? providerName : 'timum'
            }: ${productName} - ${resourceName}`,
            startDate: start.toFormat('yyyy-MM-dd'),
            endDate: end.toFormat('yyyy-MM-dd'),
            startTime: start.toFormat('HH:mm'),
            endTime: end.toFormat('HH:mm'),
            timeZone: 'Europe/Berlin',
            iCalFileName: `${
              providerName ? providerName : 'timum'
            }_${productName}_${resourceName}_${end.toFormat(
              'yyyy-MM-dd_HH-MM',
            )}`,
            location: location,
            description: description,
            background: false,
            checkmark: false,
            options: [
              'Apple',
              'Google',
              'iCal',
              'Microsoft365',
              'MicrosoftTeams',
              'Outlook.com',
              'Yahoo',
            ],
          },
          event.currentTarget,
        )
      }
    >
      {t('add_to_calendar_btn')}
    </Button>
  );
}

export default function ConfirmationView(props) {
  const { t } = useTranslation();
  const {
    timeslot,
    contactName,
    contactEmail,
    contactPhone,
    productName,
    productDescription,
    customerMail,
    resourceName,
    resourceDescription,
    providerName,
    contactChannel,
    cancelLink,
    bookingProcess,
  } = usePropsBeforeContext(props, ConfirmationPageContext);

  console.log({ ...getOverrides({}) });

  const determineContactChannelTypeIcon = () => {
    if (!contactChannel) {
      return;
    } else if (contactChannel.type === 'location') {
      return <Place />;
    }
    if (contactChannel.type === 'link') {
      return <Phonelink />;
    }
    if (contactChannel.type === 'mobile') {
      return <PhoneAndroid />;
    }
    if (contactChannel.type === 'messenger') {
      return <Forum />;
    }
    if (contactChannel.type === 'phone') {
      return <HeadsetMic />;
    } else {
      return <Adjust />;
    }
  };

  return (
    <Grid
      container
      direction="column"
      alignItems={'center'}
      alignContent={'center'}
      rowSpacing={2}
      mx={'auto'}
    >
      <Grid item textAlign={'center'}>
        <Typography variant="h2" mb={2}>
          {bookingProcess.toLowerCase() === 'immediate'
            ? t('booked_successfully_header')
            : t('requested_successfully_header')}
        </Typography>
        {customerMail && (
          <Typography variant="subtitle">
            {bookingProcess.toLowerCase() === 'immediate'
              ? t('booked_successfully_message', { mail: customerMail })
              : t('requested_successfully_message', {
                  mail: customerMail,
                })}
          </Typography>
        )}
      </Grid>
      <Grid item xs={12} borderBottom="1px solid #ddd" width={'95%'}></Grid>
      <Grid container item justifyContent={'center'}>
        <Grid item>
          <List dense>
            {productName && (
              <ListItem>
                <ListItemIcon>
                  <TaskAlt />
                </ListItemIcon>
                <ListItemText>
                  <Typography variant="body1" color={'text.secondary'}>
                    {productName}
                  </Typography>
                </ListItemText>
              </ListItem>
            )}
            {timeslot?.start && timeslot?.end && (
              <ListItem>
                <ListItemIcon>
                  <Event />
                </ListItemIcon>
                <ListItemText>
                  <Typography variant="body1" color={'text.secondary'}>
                    {`${timeslot?.start.toLocaleString(
                      DateTime.TIME_SIMPLE,
                    )} - ${timeslot?.end.toLocaleString(
                      DateTime.TIME_SIMPLE,
                    )} | ${timeslot?.start.toLocaleString(DateTime.DATE_HUGE)}`}
                  </Typography>
                </ListItemText>
              </ListItem>
            )}
            {resourceName && (
              <ListItem>
                <ListItemIcon>
                  <BookmarkBorder />
                </ListItemIcon>
                <ListItemText>
                  <Typography variant="body1" color={'text.secondary'}>
                    {resourceName}
                  </Typography>
                </ListItemText>
              </ListItem>
            )}
            {contactChannel && (
              <ListItem>
                <ListItemIcon>{determineContactChannelTypeIcon()}</ListItemIcon>
                <ListItemText>
                  <Typography variant="body1" color={'text.secondary'}>
                    {contactChannel.value}
                  </Typography>
                </ListItemText>
              </ListItem>
            )}
            {bookingProcess.toLowerCase() === 'immediate' &&
              productDescription && (
                <ListItem>
                  <ListItemIcon>{<InfoOutlinedIcon />}</ListItemIcon>
                  <ListItemText>
                    <MuiMarkdown
                      overrides={{
                        ...getOverrides({}),
                        h1: {
                          component: Typography,
                          props: {
                            variant: 'h3',
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        h2: {
                          component: Typography,
                          props: {
                            variant: 'h4',
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        h3: {
                          component: Typography,
                          props: {
                            variant: 'h5',
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        h4: {
                          component: Typography,
                          props: {
                            variant: 'h6',
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        h5: {
                          component: Typography,
                          props: {
                            variant: 'body1',
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        h6: {
                          component: Typography,
                          props: {
                            variant: 'body1',
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        p: {
                          component: Typography,
                          props: {
                            variant: 'body1',
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        span: {
                          component: Typography,
                          props: {
                            variant: 'body1',
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        blockquote: {
                          component: Typography,
                          props: {
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                        pre: {
                          component: Typography,
                          props: {
                            gutterBottom: false,
                            color: 'text.secondary',
                          },
                        },
                      }}
                    >
                      {`<span>${productDescription}</span>`}
                    </MuiMarkdown>
                  </ListItemText>
                </ListItem>
              )}
          </List>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <AddToCalendarButton
          start={timeslot?.start}
          end={timeslot?.end}
          contactEmail={contactEmail}
          contactName={contactName}
          contactPhone={contactPhone}
          resourceName={resourceName}
          resourceDescription={resourceDescription}
          productName={productName}
          productDescription={productDescription}
          providerName={providerName}
          cancelLink={cancelLink}
          location={contactChannel?.value}
        />
      </Grid>
    </Grid>
  );
}

ConfirmationView.propTypes = {
  product: PropTypes.object,
};
