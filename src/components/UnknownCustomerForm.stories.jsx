import React from 'react';
import UnknownCustomerForm from './UnknownCustomerForm';
import { rest } from 'msw';
import { useDispatch } from 'react-redux';
import { setAppConfig, selectProduct } from '../slices/appState';
import * as yup from 'yup';

import { DateTime } from 'luxon';

import Link from '@mui/material/Link';

// import TimumApiMockResponses from '../TimumApiMockResponses';

export default {
  component: UnknownCustomerForm,
  title: 'UnknownCustomerForm',
};

const Template = (args) => {
  const dispatch = useDispatch();
  dispatch(
    setAppConfig({
      fields: args.fields,
    }),
  );
  dispatch(selectProduct(args.product));
  return <UnknownCustomerForm {...args} />;
};

export const Default = {
  render: Template,

  args: {
    timeslot: {
      start: DateTime.now(),
      end: DateTime.now().plus({ hours: 1 }),
      uuid: 'someUuid',
    },
    product: { uuid: 'someOtherUuid' },
    bookingProcess: 'IMMEDIATE',
    fields: {
      firstName: {
        title: 'Vorname',
        validation: yup.string().required('Notwendig'),
      },
      lastName: {
        title: 'Nachname',
        validation: yup.string().required('Notwendig'),
      },
      email: {
        title: 'E-mail',
        format: 'email',
        prefilled: 'heinz-ag@web.de',
        validation: yup.string().email().required('Notwendig'),
      },
      mobile: {
        title: 'Mobil',
        type: 'phoneNumber',
        isRequired: true,
      },
      message: {
        title: 'Ihre Nachricht',
        prefilled: '',
        type: 'textarea',
        limit: 1024,
        validation: yup.string().max(1024),
      },
      gender: {
        title: 'Geschlecht',
        type: 'select',
        options: [
          { title: 'Weiblich', key: 'w' },
          { title: 'Männlich', key: 'm' },
          { title: 'Divers', key: 'd' },
        ],
        validation: yup.string().oneOf(['w', 'm', 'd']).required('Notwendig'),
      },

      agbs: {
        title: (
          <>
            <Link href="https://info.timum.de/datenschutz">
              Datenschutzbestimmungen
            </Link>{' '}
            gelesen und akzeptiert.
          </>
        ),
        type: 'checkbox',
        validation: yup
          .boolean()
          .required('Notwendig')
          .test(
            'agbsAccepted',
            'Sie müssen die Agbs akzeptieren bevor Sie buchen können.',
            (value) => value === true,
          ),
      },
    },
  },

  parameters: {
    msw: {
      handlers: {
        createBooking: rest.post(
          '*/rest/1/resources/:uuid/create_appointment_with_consumer',
          (req, res, ctx) => {
            const json = {
              'api-info': {
                version: '1',
              },
              createdAppointment: {
                start: req.json().start,
                end: req.json().end,
                timeslot_uuid: 'a7f943b0-9324-11ed-9e8c-e4a7a0ca32e8',
                product_uuid: req.json().product_uuid,
                contact_channel: {
                  type: 'none',
                  value:
                    'Die genaue Adresse wird am Vorabend des Termins per e-Mail zugesandt',
                },
                product_name: 'Call',
                resource_name: 'Booking Widget DEMO',
                capacity: 1,
                capacity_left: 0,
                kind: 'models.LotAppointment',
              },
            };

            return res(ctx.status(200), ctx.delay(2000), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const BookingFails = {
  render: Template,

  args: {
    ...Default.args,
  },

  parameters: {
    msw: {
      handlers: {
        createBooking: rest.post(
          'https://www.timum.de/rest/1/resources/:uuid/create_appointment_with_consumer',
          (req, res, ctx) => {
            return res(ctx.status(400), ctx.delay(500));
          },
        ),
      },
    },
  },
};

export const BookingFailsBcOfDuplicateUser = {
  render: Template,

  args: {
    ...Default.args,
  },

  parameters: {
    msw: {
      handlers: {
        createBooking: rest.post(
          'https://www.timum.de/rest/1/resources/:uuid/create_appointment_with_consumer',
          (req, res, ctx) => {
            const json = {
              'api-info': {
                version: '1',
              },
              errors: [
                {
                  errorCode: '300',
                  message:
                    'Es existiert ein Benutzer mit dieser eMail-Adresse. Sie können sich jetzt einloggen, um mit der Buchung fortzufahren.',
                },
              ],
            };

            return res(ctx.status(412), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};
