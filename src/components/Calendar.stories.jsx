import Calendar from './Calendar';
import { rest } from 'msw';
import TimumApiMockResponses from '../TimumApiMockResponses';
import { faker } from '@faker-js/faker';

// import { Default as ProductSelectorDefault } from './ProductSelector.stories';

export default {
  component: Calendar,
  title: 'Calendar',
};

export const Default = {
  args: {},

  parameters: {
    msw: {
      handlers: {
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [];
            for (const ref of [req.params.uuid]) {
              json.push({
                ref: ref,
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: ref,
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: faker.commerce.productDescription(),
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: false,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              });
            }
            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            const json = TimumApiMockResponses.getTimeslotsResponse()
              .setProviderId(faker.datatype.uuid())
              .setResourceId(faker.datatype.uuid())
              .including(5)
              .daysInPast()
              .including(5)
              .daysInFuture()
              .including()
              .today()
              .including(6)
              .timeslotsPerDay()
              .including(3)
              .timeslotsWithAppointmentsPerDay()
              .build();

            for (const [, timeslots] of Object.entries(json.timeslots)) {
              for (const tsl of timeslots) {
                tsl.capacity_left = faker.random.numeric(1) % 3;
                tsl.kind =
                  faker.random.numeric(1) % 3
                    ? 'model.Bookable'
                    : 'model.LotAppointment';

                tsl.timeslot_uuid = faker.datatype.uuid();
                tsl.appointment_uuid = faker.datatype.uuid();

                // const number = getRandomInt(
                //   0,
                //   ProductSelectorDefault.args.currentProducts.length - 1
                // );
                // tsl.product_name =
                //   ProductSelectorDefault.args.currentProducts[number].name;

                tsl.product_name = 'Besichtigung';
              }
            }

            json.timeslots.public_visible = true;

            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(json.timeslots),
            );
          },
        ),
        reserveAppointment: rest.post(
          '*/rest/1/resources/:uuid/reserve_appointment',
          (req, res, ctx) => {
            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json({
                timeslotUuid: faker.datatype.uuid(),
                resource_id: faker.datatype.uuid(),
                product_id: faker.datatype.uuid(),
                from: '2023-01-15T10:15:00Z',
                to: '2023-01-15T10:30:00Z',
              }),
            );
          },
        ),
      },
    },
  },
};

export const WithProfessionalAllowed = {
  args: {},

  parameters: {
    msw: {
      handlers: {
        ...Default.parameters.msw.handlers,
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [];
            for (const ref of [req.params.uuid]) {
              json.push({
                ref: ref,
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: ref,
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: faker.commerce.productDescription(),
                  msgHelpText: '',
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: true,
                  isLocalisationAllowed: true,
                  areCustomFieldsAllowed: true,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              });
            }
            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const ReservationFailes = {
  args: {},

  parameters: {
    msw: {
      handlers: {
        ...Default.parameters.msw.handlers,
        reserveAppointment: rest.post(
          '*/rest/1/resources/:uuid/reserve_appointment',
          (req, res, ctx) => {
            return res(ctx.status(400), ctx.delay(500));
          },
        ),
      },
    },
  },
};
