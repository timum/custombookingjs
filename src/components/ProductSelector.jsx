import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import { ProductPageContext } from './Calendar';

import { useTranslation } from 'react-i18next';
// import useElDimensions from '../hooks/useElDimensions';
import usePropsBeforeContext from '../hooks/usePropsBeforeContext';

export default function ProductSelector(props) {
  // const [headlineRef, headlineDimensions] = useElDimensions();
  const { selectedProduct, selectProduct } = useContext(ProductPageContext);
  const { t } = useTranslation();

  const { onProductSelected, products } = usePropsBeforeContext(
    props,
    ProductPageContext
  );

  if (products?.length === 1) {
    selectProduct(products[0]);
    if (onProductSelected) {
      onProductSelected(products[0]);
    }
  }

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
      spacing={2}
    >
      <Grid item xs={12}>
        <Typography variant="h2">{t('product_selection_headline')}</Typography>
      </Grid>
      <Grid
        item
        xs={12}
        sx={{
          maxHeight: `450px`,
          overflowY: 'auto',
          overflowX: 'hidden',
          width: '100%',
        }}
      >
        <List sx={{ mr: { xs: 2, sm: 2 }, ml: { xs: 1, sm: 2 } }}>
          {products?.map((product) => {
            return (
              <ListItem
                key={product.uuid}
                disablePadding
                sx={{
                  mx: 'auto',
                  borderWidth: '1px',
                  borderStyle: 'solid',
                  borderColor: 'grey.300',
                  // margin: '15px',
                  marginBottom: '16px',
                  // width: '100%',
                  // boxShadow: '0 3px 10px rgb(0 0 0 / 7%)',
                  minHeight: '58px',
                }}
              >
                <ListItemText
                  sx={{
                    py: { xs: 2, sm: 2, md: 3 },
                  }}
                  // primary={product.name}
                  // secondary={product.description}
                  // secondaryTypographyProps={{
                  //   sx: {
                  //     overflow: 'hidden',
                  //     textOverflow: 'ellipsis',
                  //     lineClamp: 2,
                  //   },
                  // }}
                />
                <ListItemButton
                  selected={product.uuid === selectedProduct?.uuid}
                  disableGutters
                  dense
                  sx={{
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    zIndex: 0,
                  }}
                  onClick={() => {
                    selectProduct(product);
                    if (onProductSelected) {
                      onProductSelected();
                    }
                  }}
                >
                  <Typography variant="body2" px={{ xs: 2, sm: 2, md: 3 }}>
                    {product.minDuration
                      ? `${product.name} (${product.minDuration} min)`
                      : `${product.name}`}
                  </Typography>
                </ListItemButton>
              </ListItem>
            );
          })}
        </List>
      </Grid>
    </Grid>
  );
}

ProductSelector.propTypes = {
  /** callback for parent to trigger action on produc selection */
  onProductSelected: PropTypes.func,
};
