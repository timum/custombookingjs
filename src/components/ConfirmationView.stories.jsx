import React from 'react';
import ConfirmationView from './ConfirmationView';
import ConfigHandler from './handlers/ConfigHandler';

import { DateTime } from 'luxon';

// import TimumApiMockResponses from '../TimumApiMockResponses';

export default {
  component: ConfirmationView,
  title: 'ConfirmationView',
};

const Template = (args) => {
  return (
    <ConfigHandler>
      {/* -> needed for i18n to work */}
      <ConfirmationView {...args} />
    </ConfigHandler>
  );
};

export const Default = {
  render: Template,

  args: {
    start: DateTime.now(),
    end: DateTime.now().plus({ hours: 1 }),
    bookingProcess: 'IMMEDIATE',
    productName: 'Besichtigung',
    resourceName: 'Tuvalu',
    customerMail: 'carsten-mohs@timum.de',
    contactChannel: {
      value:
        'Die Adresse wird mit der Terminerinnerung am Vorabend bekannt gegeben',
      type: 'none',
    },
  },
};
