import React, { useMemo } from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import { AnimatePresence, motion } from 'framer-motion';
import Link from '@mui/material/CardActionArea';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

import MuiMarkdown from 'mui-markdown';
import useElDimensions from '../../hooks/useElDimensions';
import { useTranslation } from 'react-i18next';

const variations = {
  hide: {
    opacity: 0,
    transition: { duration: 0.2, delay: 0 },
  },
  show: {
    opacity: 1,
    transition: { duration: 0.2, delay: 0 },
  },
  hideLess: {
    height: 0,
    opacity: 0,
    transition: { duration: 0.2, delay: 0 },
  },
  showLess: {
    height: 'auto',
    opacity: 1,
    transition: { duration: 0.2, delay: 0 },
  },
};

function CustomMarkdown({ children, ...props }) {
  return (
    <MuiMarkdown
      {...props}
      overrides={{
        h1: {
          component: Typography,
          props: { variant: 'h3', gutterBottom: true },
        },
        h2: {
          component: Typography,
          props: { variant: 'h4', gutterBottom: true },
        },
        h3: {
          component: Typography,
          props: { variant: 'h5', gutterBottom: true },
        },
        h4: {
          component: Typography,
          props: { variant: 'h6', gutterBottom: true },
        },
        h5: {
          component: Typography,
          props: {
            variant: 'body1',
            gutterBottom: true,
            color: 'text.secondary',
          },
        },
        h6: {
          component: Typography,
          props: {
            variant: 'body1',
            gutterBottom: true,
            color: 'text.secondary',
          },
        },
        p: {
          component: Typography,
          props: { variant: 'body2', gutterBottom: true },
        },
        span: {
          component: Typography,
          props: { variant: 'body2', gutterBottom: false },
        },
      }}
    >
      {children}
    </MuiMarkdown>
  );
}

export default function ShowMoreCard({
  text,
  expanded: expandedControlled,
  doOnExpand,
}) {
  const { t } = useTranslation();
  const isExpandedControlled = expandedControlled !== undefined;
  const [expanded, setExpanded] = React.useState(false);
  const isExpanded = isExpandedControlled ? expandedControlled : expanded;
  const [collapsedSim, collapsedSimDimensions] = useElDimensions();
  const [expandedSim, expandedSimDimensions] = useElDimensions();

  const toggleExpansion = () => {
    if (doOnExpand) {
      doOnExpand(isExpanded);
    } else {
      setExpanded(!isExpanded);
    }
  };

  const showButton = useMemo(() => {
    return text?.length > 250;
  }, [text]);

  return (
    <>
      <Card elevation={0}>
        {/* For smalll content we disable the expansion */}
        <CardContent
          sx={{
            position: 'relative',
            p: '0!important',
          }}
        >
          <Box
            ref={collapsedSim}
            sx={{ opacity: 0, position: 'absolute', left: '-10000' }}
          >
            <CustomMarkdown>{`${text.substring(0, 150)}...`}</CustomMarkdown>
          </Box>
          <Box
            ref={expandedSim}
            sx={{ opacity: 0, position: 'absolute', left: '-10000' }}
          >
            <CustomMarkdown>{text}</CustomMarkdown>
          </Box>
          <Box
            component={motion.div}
            initial={false}
            animate={{
              height: isExpanded
                ? `${expandedSimDimensions.height}px`
                : `${collapsedSimDimensions.height}px`,
            }}
            transition={{ duration: 0.2, delay: 0.2, ease: 'easeInOut' }}
            sx={{
              overflow: 'hidden',
            }}
          >
            <CustomMarkdown>{text}</CustomMarkdown>
          </Box>
          <AnimatePresence>
            {!isExpanded && showButton && (
              <motion.div
                key={'more'}
                initial={{ opacity: 0 }}
                animate={!isExpanded && showButton ? 'show' : 'hide'}
                exit={{ opacity: 0 }}
                transition={{}}
                // animate={!isExpanded && showButton ? 'show' : 'hide'}
                variants={variations}
              >
                <Link
                  disableRipple
                  sx={{
                    position: 'absolute',
                    bottom: '1px',
                    right: '15px',
                    backgroundColor: (theme) => theme.palette.background.paper,
                    boxShadow: (theme) =>
                      `0 0 20px 30px ${theme.palette.background.paper}`,
                    clipPath: 'inset(-2px -25px -5px -25px)',
                    color: 'primary.main',
                    textDecoration: 'none',
                    width: 'fit-content',
                  }}
                  onClick={toggleExpansion}
                >
                  {t('common.More')}
                </Link>
              </motion.div>
            )}
            {isExpanded && showButton && (
              <motion.div
                key={'less'}
                initial={{ opacity: 0, height: 0 }}
                animate={isExpanded && showButton ? 'showLess' : 'hideLess'}
                exit={{ opacity: 0 }}
                // animate={isExpanded && showButton ? 'show' : 'hide'}
                variants={variations}
                style={{ overflow: 'hidden' }}
              >
                <Link
                  disableRipple
                  sx={{
                    position: 'relative',
                    float: 'right',
                    backgroundColor: (theme) => theme.palette.background.paper,
                    color: 'primary.main',
                    textDecoration: 'none',
                    width: 'fit-content',
                    mb: 1,
                  }}
                  onClick={toggleExpansion}
                >
                  {t('common.Less')}
                </Link>
              </motion.div>
            )}
          </AnimatePresence>
        </CardContent>
      </Card>
    </>
  );
}
