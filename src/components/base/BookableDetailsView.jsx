import * as React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'react-i18next';
import { CircularProgress, Grid, Typography } from '@mui/material';

import BookmarkBorder from '@mui/icons-material/BookmarkBorder';
import TaskAlt from '@mui/icons-material/TaskAlt';

import Countdown, { zeroPad } from 'react-countdown';

import { DateTime } from 'luxon';

export default function BookableDetailsView({
  start,
  end,
  productName,
  resourceName,
  disableReservation = false,
  delayReservation = false,
}) {
  const { t } = useTranslation();
  const reservation = React.useMemo(() => {
    return Date.now() + 180000;
  }, []);

  return (
    <Grid container direction="column" alignContent="space-around" mt={2}>
      <Grid item>
        <Typography variant="body1" fontWeight={600} display="inline">
          {`${start?.toLocaleString(
            DateTime.TIME_SIMPLE
          )} - ${end?.toLocaleString(DateTime.TIME_SIMPLE)}`}
        </Typography>
        <Typography variant="body1" display="inline">
          {`\u00A0|\u00A0${start?.toLocaleString(DateTime.DATE_HUGE)}`}
        </Typography>
      </Grid>
      {resourceName && (
        <Grid>
          <Typography
            variant="body1"
            component="div"
            sx={{
              verticalAlign: 'middle',
              display: 'inline-flex',
              alignItems: 'center',
            }}
          >
            <BookmarkBorder fontSize="inherit" />
            <div style={{ paddingLeft: '8px' }}>{resourceName}</div>
          </Typography>
        </Grid>
      )}
      {productName && (
        <Grid item>
          <Typography
            variant="body1"
            component="div"
            sx={{
              verticalAlign: 'middle',
              display: 'inline-flex',
              alignItems: 'center',
            }}
          >
            <TaskAlt fontSize="inherit" />
            <div style={{ paddingLeft: '8px' }}>{productName}</div>
          </Typography>
        </Grid>
      )}
      {!disableReservation && !delayReservation && (
        <Grid item>
          <Countdown
            date={reservation}
            daysInHours
            renderer={({ minutes, seconds, completed }) => {
              if (completed) {
                // Render a completed state
                return (
                  <Typography variant="caption" color="text.secondary">
                    {t('reservation_expired')}
                  </Typography>
                );
              } else {
                // Render a countdown
                return (
                  <Typography variant="caption" color="text.secondary">
                    {t('until_reservation_expiration', {
                      expiration: `${zeroPad(minutes)}:${zeroPad(seconds)}`,
                    })}
                  </Typography>
                );
              }
            }}
          />
        </Grid>
      )}
      {!disableReservation && delayReservation && (
        <Grid item>
          <CircularProgress
            color="inherit"
            size={12}
            sx={{ display: 'inline-block' }}
          />{' '}
          <Typography variant="caption" color="text.secondary">
            {t('until_reservation_expiration', {
              expiration: ' ',
            })}
          </Typography>
        </Grid>
      )}
    </Grid>
  );
}

BookableDetailsView.propTypes = {
  start: PropTypes.object,
  end: PropTypes.object,
  product: PropTypes.object,
  productName: PropTypes.string,
  productMinDuration: PropTypes.number,
  resourceName: PropTypes.string,
  disableReservation: PropTypes.bool,
};
