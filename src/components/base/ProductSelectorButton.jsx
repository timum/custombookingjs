import React from 'react';
import { useTranslation } from 'react-i18next';

import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

import TaskAlt from '@mui/icons-material/TaskAlt';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

export default function ProductSelectorButton({
  openProductPage,
  products,
  product,
  buttonProps = {},
  wrapperProps = {},
}) {
  const { t } = useTranslation();
  return (
    <Box
      {...wrapperProps}
      sx={{
        borderRadius: (theme) => `${theme.shape.borderRadius}px`,
        ...(wrapperProps?.sx || {}),
      }}
    >
      {products?.length > 1 && (
        <Button
          size="small"
          variant="outlined"
          fullWidth
          onClick={openProductPage}
          startIcon={<TaskAlt fontSize="small" />}
          endIcon={<ExpandMoreIcon fontSize="10px" />}
          {...buttonProps}
          sx={{ textTransform: 'inherit', ...(buttonProps?.sx || {}) }}
        >
          {product?.name || t('product_selection_headline')}
        </Button>
      )}
      {products?.length === 1 && (
        <Grid container justifyContent={'center'} sx={{ pr: 1, pl: 1 }}>
          <Grid item sx={{ maxWidth: '20px', mr: 0.5 }}>
            <Typography
              variant="body1"
              color={'text.secondary'}
              lineHeight={'100%'}
              sx={{
                height: '100%',
              }}
            >
              <TaskAlt
                fontSize="inherit"
                sx={{
                  color: 'text.secondary',
                  lineHeight: '100%',
                  height: '100%',
                  width: '100%',
                }}
              />
            </Typography>
          </Grid>
          <Grid item xs="auto">
            <Typography variant="body1" color={'text.secondary'}>
              {product?.name || products[0]?.name}
            </Typography>
          </Grid>
        </Grid>
      )}
    </Box>
  );
}
