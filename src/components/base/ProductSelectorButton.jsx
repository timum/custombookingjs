import React from 'react';
import { useTranslation } from 'react-i18next';

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

import TaskAlt from '@mui/icons-material/TaskAlt';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

export default function ProductSelectorButton({
  openProductPage,
  products,
  product,
  ...props
}) {
  const { t } = useTranslation();
  return (
    <>
      {products?.length > 1 && (
        <Button
          size="small"
          variant="outlined"
          fullWidth
          onClick={openProductPage}
          startIcon={<TaskAlt fontSize="small" />}
          endIcon={<ExpandMoreIcon fontSize="10px" />}
          sx={{ textTransform: 'inherit' }}
          {...props}
        >
          {product?.name || t('product_selection_headline')}
        </Button>
      )}
      {products?.length === 1 && (
        <Grid container justifyContent={'center'}>
          <Grid item sx={{ minWidth: '30px' }}>
            <TaskAlt fontSize="small" sx={{ color: 'text.secondary' }} />
          </Grid>
          <Grid item xs="auto">
            <Typography variant="body1" color={'text.secondary'}>
              {product?.name || products[0]?.name}
            </Typography>
          </Grid>
        </Grid>
      )}
    </>
  );
}
