import React from 'react';
import VariableForm, {
  generateDefaultValues,
  generateValidation,
} from './VariableForm';
import * as yup from 'yup';

import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, FormProvider } from 'react-hook-form';
import Link from '@mui/material/Link';

export default {
  component: VariableForm,
  title: 'VariableForm',
};

const Template = (args) => {
  const methods = useForm({
    defaultValues: generateDefaultValues(args.fields),
    resolver: yupResolver(yup.object().shape(generateValidation(args.fields))),
    mode: 'onChange',
  });

  methods.watch((value, { name, type }) => console.log(value, name, type));

  return (
    <FormProvider {...methods}>
      <VariableForm {...args} />
    </FormProvider>
  );
};

export const Default = {
  render: Template,

  args: {
    resourceId: 'someId',
    timeslot: { start: 'lol', end: 'afterLol', uuid: 'someUuid' },
    product: { uuid: 'someOtherUuid' },
    fields: {
      firstName: {
        title: 'Vorname',
        validation: yup.string().required(),
      },
      lastName: {
        title: 'Nachname',
        validation: yup.string().required(),
      },
      email: {
        title: 'E-mail',
        format: 'email',
        prefilled: 'heinz-ag@web.de',
        validation: yup.string().email('kaputt').required(),
      },
      mobile: {
        title: 'Mobil',
        type: 'phoneNumber',
        isRequired: true,
      },
      message: {
        title: 'Ihre Nachricht',
        prefilled: '',
        type: 'textarea',
        limit: 1024,
        validation: yup.string().max(1024),
      },
      gender: {
        title: 'Geschlecht',
        type: 'select',
        prefilled: 'd',
        options: [
          { title: 'Weiblich', key: 'w' },
          { title: 'Männlich', key: 'm' },
          { title: 'Divers', key: 'd' },
        ],
        validation: yup.string().oneOf(['w', 'm', 'd']).required(),
      },
      agbs: {
        title: (
          <>
            <Link href="https://info.timum.de/datenschutz">
              Datenschutzbestimmungen
            </Link>{' '}
            gelesen und akzeptiert.
          </>
        ),
        type: 'checkbox',

        validation: yup
          .boolean()
          .required()
          .test(
            'agbsAccepted',
            'Sie müssen die Agbs akzeptieren bevor Sie buchen können.',
            (value) => value === true,
          ),
      },
    },
  },
};
