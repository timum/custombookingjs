import React from 'react';
import LoginView from './LoginView';
import ConfigHandler from './handlers/ConfigHandler';
import { rest } from 'msw';

// import TimumApiMockResponses from '../TimumApiMockResponses';

export default {
  component: LoginView,
  title: 'LoginView',
};

const Template = (args) => {
  return (
    <ConfigHandler>
      {/* -> needed for i18n to work */}
      <LoginView {...args} />
    </ConfigHandler>
  );
};

export const Default = {
  render: Template,
  args: {},

  parameters: {
    msw: {
      handlers: {
        authenticate: rest.post('*/auth/p/userpass', (req, res, ctx) => {
          return res(ctx.status(200), ctx.delay(500));
        }),
      },
    },
  },
};

export const WithUserName = {
  render: Template,

  args: {
    userName: 'prefilled@user.name',
  },

  parameters: {
    msw: {
      handlers: {
        ...Default.parameters.msw.handlers,
      },
    },
  },
};

export const AuthenticationFails = {
  render: Template,

  args: {
    userName: 'prefilled@user.name',
  },

  parameters: {
    msw: {
      handlers: {
        ...Default.parameters.msw.handlers,
        authenticate: rest.post('*/auth/p/userpass', (req, res, ctx) => {
          return res(ctx.status(412), ctx.delay(500));
        }),
      },
    },
  },
};
