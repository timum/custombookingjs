import React from 'react';
import { useSelector } from 'react-redux';

import atcbCss from '../../styles/atcb-css';
import fullCalendarCss from '../../styles/fullcalendar-css';
import { selectFcConfigProp } from '../../slices/appState';

import { Global } from '@emotion/react';

/**
 * This components sole purpose is to write css into global context.
 * It's currently only needed by 3rd party libs.
 * @param {} param0
 * @returns
 */
export default function CssHandler() {
  const useCustomTimumCss = useSelector((state) =>
    selectFcConfigProp(state, 'useCustomTimumCss'),
  );

  return (
    <>
      {useCustomTimumCss && <Global styles={fullCalendarCss} />}
      <Global styles={atcbCss} />
    </>
  );
}
