import React from 'react';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

import { locizePlugin } from 'locize'; // does things for direct edit ??
import LocizeBackend from 'i18next-locize-backend';
import i18n from 'i18next';

import { Settings } from 'luxon';
import { useSelector } from 'react-redux';
import { selectAppConfig } from '../../slices/appState';

import { merge } from 'lodash';

const i18next = i18n
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  .use(LanguageDetector)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  .use(locizePlugin)
  .use(LocizeBackend)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    detection: {
      caches: ['cookie'],
      order: ['querystring', 'cookie', 'localStorage', 'navigator'],
      lookupQuerystring: 'culture',
    },
    compatibilityJSON: 'v3',
    interpolation: { prefix: '{{', suffix: '}}' },
    fallbackLng: 'de',
    ns: ['bookingjs'],
    load: 'languageOnly',
    backend: {
      projectId: 'ea068e6d-18b6-4697-bd21-65291883e27e',
      referenceLng: 'de',
    },
  })
  .then(() => {
    Settings.defaultLocale = i18n.resolvedLanguage;
  });

export default function LanguageHandler() {
  const appConfig = useSelector(selectAppConfig);

  React.useEffect(() => {
    let active = true;
    setupLanguage();
    return () => {
      active = false;
    };

    async function setupLanguage() {
      if (appConfig) {
        await i18next;

        if (appConfig.localization && active) {
          for (const [lang, bundle] of Object.entries(appConfig.localization)) {
            if (i18n.hasResourceBundle(lang, 'bookingjs')) {
              const existingBundle = i18n.getResourceBundle(lang, 'bookingjs');
              i18n.removeResourceBundle(lang, 'bookingjs');
              const mergedBundle = merge({}, existingBundle, bundle);
              i18n.addResourceBundle(lang, 'bookingjs', mergedBundle);
            } else {
              i18n.addResourceBundle(lang, 'bookingjs', bundle);
            }
          }
        }

        if (
          appConfig.culture &&
          appConfig.culture !== i18n.resolvedLanguage &&
          active
        ) {
          await i18n.changeLanguage(appConfig.culture);
          Settings.defaultLocale = i18n.resolvedLanguage;
        }
      }
    }
  }, [appConfig]);
}
