import { useIdentifyCustomerQuery } from '@timum/timum_pdk';

import {
  selectAppConfigProp,
  selectSelectedPublicData,
  setIdentifiedCustomer,
  setCustomerIdentificationFailed,
  setIsIdentifyingCustomer,
} from '../../slices/appState';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useCallback } from 'react';

/**
 * It's only purpose is to start the identification of the customer if pData or a coocky is given.
 * Allows the parent to decide what to render based on the result of the query.
 * @param {} param0
 * @returns
 */
export default function CustomerIdentityHandler({ children }) {
  const pData = useSelector((state) => selectAppConfigProp(state, 'pData'));
  const refPool = useSelector((state) => selectAppConfigProp(state, 'ref'));
  const publicData = useSelector(selectSelectedPublicData);
  const dispatch = useDispatch();

  const cookie = useSelector((state) => selectAppConfigProp(state, 'cookie'));

  const shouldSkip = useCallback(() => {
    return (!pData || !pData.personId) && !cookie;
  }, [cookie, pData]);

  const {
    data: publicCustomerData,
    isLoading: isIdentifyingCustomer,
    isError,
  } = useIdentifyCustomerQuery(
    {
      channelOrResourceId: refPool?.[0] ?? publicData?.ref, // we must ensure that all resources belong to same provider; then it doesn't matter which one is used
      crmSlug: pData?.platform,
      personId: pData?.personId,
    },
    {
      skip: shouldSkip(),
    },
  );

  useEffect(() => {
    dispatch(setIsIdentifyingCustomer(isIdentifyingCustomer));
    dispatch(setCustomerIdentificationFailed(isError || shouldSkip()));
    dispatch(setIdentifiedCustomer(publicCustomerData));
  }, [
    dispatch,
    isIdentifyingCustomer,
    isError,
    publicCustomerData,
    shouldSkip,
  ]);

  return children;
}
