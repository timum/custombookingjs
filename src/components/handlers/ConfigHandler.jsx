import {
  selectIsThemingAllowed,
  selectAreCustomFieldsAllowed,
  selectIsLocalisationAllowed,
  setAppConfig,
  setFcConfig,
  setMuiTheme,
  setResToPublicDataMap,
  setIsWaitingForConf,
} from '../../slices/appState';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';

import { setTimumApiHost } from '@timum/timum_pdk/src';

import { isArray } from 'lodash';
import { decodeJwt } from 'jose';

import { selectPublicData } from '../../slices/appState';
import { primary } from '../../defaults';

import { merge } from 'lodash';

import React, { useCallback } from 'react';

import {
  useLazyPublicDataQuery,
  useLazySpecificBookablesQuery,
  useLazySpecificProductsQuery,
} from '@timum/timum_pdk';
// import useDeepCompareEffect from 'use-deep-compare-effect';
// import useRenderingTrace from '../../hooks/useRenderingTrace';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

const timumDomains =
  'timum\\.de|timum\\.at|timum\\.com|timum\\.fr|timum\\.io|timum\\.es|timum\\.it|timum\\.co.uktimum\\.nl|timum\\.eu|timum\\.info|timum\\.org|timum\\.me';
const allowedOrigin = `(${timumDomains}|localhost(:[0-9]+)?|127\\.0\\.0\\.1(:[0-9]+)?)`;
const allowedOriginRegex = new RegExp(
  `^https?://([a-zA-Z0-9-]+\\.)*${allowedOrigin}$`,
);

/**
 * In case the user passed a height like '500px' instead of 500.
 */
function normalizeHeight(value) {
  if (typeof value === 'string') {
    // Assuming the format is always like '400px' (number followed by unit)
    // We use a regular expression to get just the numeric part
    const match = value.match(/^(\d+)/);
    if (match && match[1]) {
      return Number(match[1]);
    }
    // Handle case where the string does not contain a number
    return 500; // default height
  } else if (typeof value === 'number') {
    // The value is already a number, just return it
    return value;
  }
  return 500;
}

function removeUndefinedFields(fields, mergedFields) {
  if (
    fields &&
    Object.hasOwn(fields, 'message') &&
    typeof fields.message === 'undefined'
  ) {
    delete mergedFields.message;
  }

  if (
    fields &&
    Object.hasOwn(fields, 'mobile') &&
    typeof fields.mobile === 'undefined'
  ) {
    delete mergedFields.mobile;
  }

  if (
    fields &&
    Object.hasOwn(fields, 'email') &&
    typeof fields.email === 'undefined'
  ) {
    delete mergedFields.email;
  }

  if (
    fields &&
    Object.hasOwn(fields, 'firstName') &&
    typeof fields.firstName === 'undefined'
  ) {
    delete mergedFields.firstName;
  }

  if (
    fields &&
    Object.hasOwn(fields, 'lastName') &&
    typeof fields.lastName === 'undefined'
  ) {
    delete mergedFields.lastName;
  }
}

function sortFields(fields, index, result) {
  if (!result) {
    result = {};
  }

  if (typeof index === 'undefined') {
    index = 0;
  }

  if (index >= Object.entries(fields).length) {
    return result;
  }

  for (const [key, value] of Object.entries(fields)) {
    if (value && value.index === index) {
      result[key] = value;
    }
  }

  index++;
  return sortFields(fields, index, result);
}

function getCookie(key) {
  var b = document.cookie.match('(^|;)\\s*' + key + '\\s*=\\s*([^;]+)');
  return b ? b.pop() : '';
}

function transformRef(ref, platform, prvUuid) {
  if (
    !ref.includes('@') &&
    !ref.match(
      /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i,
    )
  ) {
    if (!platform) {
      console.warn(
        "One or more ref parameters aren't fully qualified AND you have not provided a `platform` prop which allows timum to do it for you. " +
          "Most likely timum won't find anything. " +
          'Please either add a platform prop to your configuration or use a full reference in either the ref prop or a ref url parameter.',
      );
    } else {
      // eslint-disable-next-line prettier/prettier
      return `${ref}${prvUuid ? '@' + prvUuid : ''}@${platform}`;
    }
  }

  return ref;
}

function addPostMessageCallbacks(appConfig) {
  appConfig.callbacks = {
    createBookingStarted: ({ timeslot, data }) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'createBookingStarted',
          timeslot,
          data,
        },
        appConfig.postMessageTarget,
      );
    },
    createBookingSuccessful: ({
      timeslot,
      response /* an RTKQ response object */,
      data,
    }) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'createBookingSuccessful',
          timeslot,
          response,
          data,
        },
        appConfig.postMessageTarget,
      );
    },
    createBookingFailed: ({
      timeslot,
      response /* an RTKQ response object */,
      data,
    }) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'createBookingFailed',
          timeslot,
          response,
          data,
        },
        appConfig.postMessageTarget,
      );
    },

    cancelationgStarted: ({
      timeslot,
      response /* an RTKQ response object */,
    }) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'cancelationStarted',
          timeslot,
          response,
        },
        appConfig.postMessageTarget,
      );
    },
    cancelationSuccessful: ({
      timeslot,
      response /* an RTKQ response object */,
    }) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'cancelationSuccessful',
          timeslot,
          response,
        },
        appConfig.postMessageTarget,
      );
    },
    cancelationFailed: ({
      timeslot,
      response /* an RTKQ response object */,
    }) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'cancelationFailed',
          timeslot,
          response,
        },
        appConfig.postMessageTarget,
      );
    },
    fetchingPublicDataSucceeded: (publicData) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'fetchingPublicDataSucceeded',
          data: publicData,
        },
        appConfig.postMessageTarget,
      );
    },
    fetchingProductsSucceeded: (products) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'fetchingProductsSucceeded',
          data: products,
        },
        appConfig.postMessageTarget,
      );
    },
    fetchingBookablesSucceeded: (bookables) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'fetchingBookablesSucceeded',
          data: bookables,
        },
        appConfig.postMessageTarget,
      );
    },

    fetchingPublicDataFailed: (error /* an RTKQ error object */) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'fetchingPublicDataFailed',
          error,
        },
        appConfig.postMessageTarget,
      );
    },
    fetchingProductsFailed: (error /* an RTKQ error object */) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'fetchingProductsFailed',
          error,
        },
        appConfig.postMessageTarget,
      );
    },
    fetchingBookablesFailed: (error /* an RTKQ error object */) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'fetchingBookablesFailed',
          error,
        },
        appConfig.postMessageTarget,
      );
    },

    openedProductSelection: () => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'openedProductSelection',
        },
        appConfig.postMessageTarget,
      );
    },

    openedResourceSelection: () => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'openedResourceSelection',
        },
        appConfig.postMessageTarget,
      );
    },

    openedBookingPage: (tsl) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'openedBookingPage',
          timeslot: tsl,
        },
        appConfig.postMessageTarget,
      );
    },

    openedCancelPage: (tsl) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'openedCancelPage',
          timeslot: tsl,
        },
        appConfig.postMessageTarget,
      );
    },

    openedConfirmationPage: () => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'openedConfirmationPage',
        },
        appConfig.postMessageTarget,
      );
    },

    closedProductSelection: () => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'closedProductSelection',
        },
        appConfig.postMessageTarget,
      );
    },

    closedResourceSelection: () => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'closedResourceSelection',
        },
        appConfig.postMessageTarget,
      );
    },

    closedBookingPage: (tsl) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'closedBookingPage',
          timeslot: tsl,
        },
        appConfig.postMessageTarget,
      );
    },

    closedCancelPage: (tsl) => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'closedCancelPage',
          timeslot: tsl,
        },
        appConfig.postMessageTarget,
      );
    },

    closedConfirmationPage: () => {
      window?.parent?.postMessage(
        {
          origin: 'bookingjs',
          type: 'closedConfirmationPage',
        },
        appConfig.postMessageTarget,
      );
    },
  };
}

const addToRefPoolIfNotExists = (uuid, prdRefPool) => {
  if (uuid && !prdRefPool.includes(uuid)) {
    prdRefPool.push(uuid);
  }
};

const addPublicDataToMap = (data, map) => {
  if (!data) return;
  if (!data.forEach) {
    // no forEach means no list. Therefore we just add the data
    // I think this only happens in stories though....
    map[data.resource.uuid] = { ...data };
  } else {
    data.forEach((data) => {
      map[data.resource.uuid] = { ...data };
    });
  }
};

const addBookablesToMap = (bookables, map) => {
  if (!bookables) return;

  bookables.forEach((data) => {
    if (map[data.resource.uuid]) {
      map[data.resource.uuid].bookables = data.bookables;
      map[data.resource.uuid].products = data.products;
    } else {
      map[data.resource.uuid] = { ...data };
    }
  });
};

async function fetchReferencedData(
  t,
  appConfig,
  dispatch,
  fetchPublicData,
  fetchSpecificBookables,
  fetchSpecificProducts,
) {
  let map = {};

  const refPool = appConfig?.ref;
  const tslRefPool = appConfig?.tslRefs;
  const virtualTslRefPool = appConfig?.virtualTslRefs;
  const prdRefPool = appConfig?.prdRefs;
  const additionalData = appConfig?.additionalData;
  const allResourcesOption = appConfig?.allResourcesOption;

  const givenData = appConfig?.publicData;
  const givenTimeslots = appConfig?.tsls;

  const channelKey = appConfig?.channelKey;

  if (refPool?.length > 0 && !givenData?.length > 0) {
    const publicDataResult = await fetchPublicData({
      channelOrResourceId: refPool[0],
      params: {
        channelKey: channelKey,
        ref: refPool,
        ...additionalData,
      },
    });
    addPublicDataToMap(publicDataResult?.data, map);
  }

  if (givenData?.length > 0) {
    addPublicDataToMap(givenData, map);
  }

  if (
    allResourcesOption &&
    (!tslRefPool || tslRefPool.length === 0) &&
    (!virtualTslRefPool || virtualTslRefPool.length === 0)
  ) {
    const newMap = {
      'all-option': {
        resource: { uuid: 'all-option', name: t('common.All') },
        provider: Object.values(map)?.[0]?.provider,
      },
      ...map,
    };

    map = newMap;
  }

  if (
    (tslRefPool?.length > 0 || virtualTslRefPool?.length > 0) &&
    !givenTimeslots?.length > 0
  ) {
    // this fetches data for multiple timeslots no matter to which resource tehy belong...
    const tslResult = await fetchSpecificBookables({
      params: {
        channelKey: channelKey,
        tslIds: tslRefPool,
        ...additionalData,
      },
      body: virtualTslRefPool,
    });

    //... which is why this specifically checks for the resource id and sorts the data accoridingly
    addBookablesToMap(tslResult?.data, map);
  }

  if (givenTimeslots?.length > 0) {
    addBookablesToMap(givenTimeslots, map);
  }

  if (prdRefPool?.length > 0) {
    // we add products which are attached to a bookable
    // this way we ensure that all bookables in tslRefs are actually displayable by bookingjs.
    // In this way, selected timeslots have higher priority than selected products.
    Object.values(map).forEach((publicData) => {
      if (!publicData?.bookables) return;

      // here bookables is an object with dates as keys and a list of bookable objects as values
      // Object.values therefore gives us a list of lists of bookable objects for all loaded dates.
      // the first forEach iterates over the list of lists. The second forEach iterates over the list of bookable objects.
      Object.values(publicData?.bookables)?.forEach((dates) => {
        dates.forEach((bookable) => {
          addToRefPoolIfNotExists(bookable?.product_uuid, prdRefPool);

          bookable.products?.forEach((product) => {
            addToRefPoolIfNotExists(product.uuid, prdRefPool);
          });
        });
      });
    });

    const prdResult = await fetchSpecificProducts({
      params: {
        prdRefs: prdRefPool,
        additionalData,
      },
    });

    // now we assign the products to the fitting resources in the map.
    // we discard resources that have no matching products
    const sd = Object.entries(map).filter(([, publicData]) => {
      if (publicData?.resource?.supportedProducts?.length === 0) {
        return false; // discard
      }

      const prdMatches = prdResult.data.filter((prd) => {
        const supportedProducts = publicData?.resource?.supportedProducts;

        return (
          supportedProducts &&
          supportedProducts.some((prdUuid) => prdUuid === prd.uuid)
        );
      });

      if (prdMatches.length === 0) {
        return false; // discard
      }

      publicData.products = prdMatches;
      return true; // keep
    });

    map = Object.fromEntries(sd);
  }

  dispatch(setResToPublicDataMap(map));

  if (Object.values(map).length > 1) {
    // if it's a cancel link, in a multi resource scenario, then we preselct the resource with the cancelable appointment
    if (appConfig.cancelableAppointment) {
      let foundMatch = false;

      for (const data of Object.values(map)) {
        if (data?.resource?.uuid === appConfig.cancelableAppointment?.resId) {
          dispatch(selectPublicData(data));
          foundMatch = true;
        }
      }

      // we should still select one, even if there is no match
      if (!foundMatch) {
        dispatch(selectPublicData(Object.values(map)[0]));
      }
    } else {
      //if there's no cancelable appointment, we just select the first one
      dispatch(selectPublicData(Object.values(map)[0]));
    }
  } else if (Object.values(map).length === 1) {
    dispatch(selectPublicData(Object.values(map)[0]));
  } else {
    console.warn('Given reference(s) seems to be invalid.');
  }
}

function getJwtConfig(jwt) {
  try {
    return decodeJwt(jwt);
  } catch (error) {
    console.error('Error decrypting JWT:', error);
  }
}

function validateAndPreProcessConfig(givenAppConfig) {
  const result = { ...givenAppConfig };
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(result.queryString || queryString);

  let urlPData = urlParams.get('pData') ?? urlParams.get('pdata');
  let urlPData2 = urlParams.get('pDataPlatform')
    ? {
        platform: urlParams.get('pDataPlatform'),
        id: urlParams.get('pDataId'),
        idPropName: urlParams.get('pDataIdPropName'),
      }
    : undefined;
  const timumCookie = getCookie('PLAY_SESSION');
  let urlChannelKey =
    urlParams.get('channelKey') ?? urlParams.get('channelkey');
  const urlAuthToken = urlParams.get('X-SIGNED-APP-CUSTOMER');
  let urlResId = urlParams.get('resId'); // part of cancel link
  let urlRef = urlParams.getAll('ref');
  let urlTslRefs = urlParams.getAll('tslRefs');
  let urlVirtualTslRefs = urlParams.getAll('virtualTslRefs');
  let urlPrdRefs = urlParams.getAll('prdRefs');
  let urlCulture = urlParams.get('culture');
  let urlPostMessageTarget = urlParams.get('postMessageTarget');
  let urlAllResourcesOption = urlParams.get('allResourcesOption');
  let urlCalendarFrontend = urlParams.get('calendarFrontend');

  // if there is a compressed config, it has priority over manually entered data
  // prevents people fiddling around with their links, as then fiddling causes no changes
  let urlConfig = urlParams.get('conf');
  if (urlConfig) {
    urlConfig = getJwtConfig(urlConfig);
    urlRef = urlConfig.ref || [];
    urlChannelKey = urlConfig.channelKey;
    urlPData = urlConfig.pData;
    urlTslRefs = urlConfig.tslRefs || [];
    urlPrdRefs = urlConfig.prdRefs || [];
    urlCulture = urlConfig.urlCulture;
    urlVirtualTslRefs = urlConfig.virtualTslRefs || [];
    urlPostMessageTarget = urlConfig.postMessageTarget;
    urlAllResourcesOption = urlConfig.allResourcesOption;
    urlCalendarFrontend = urlConfig.calendarFrontend;
  }

  const hash = result.queryHash || window.location.hash;
  const [, , appointmentUuid] = hash?.split('/') ?? [];

  if (urlPData || urlPData2) {
    result.pData = urlPData ?? urlPData2;
  }

  if (urlChannelKey) {
    result.channelKey = urlChannelKey;
  }

  if (urlRef && urlRef.length > 0) {
    result.ref = urlRef;
  }

  if (urlTslRefs && urlTslRefs.length > 0) {
    result.tslRefs = urlTslRefs;
  }

  if (urlVirtualTslRefs && urlVirtualTslRefs.length > 0) {
    result.virtualTslRefs = urlVirtualTslRefs;
  }

  if (urlPrdRefs && urlPrdRefs.length > 0) {
    result.prdRefs = urlPrdRefs;
  }

  if (urlCulture) {
    result.culture = urlCulture;
  }

  if (urlPostMessageTarget) {
    result.postMessageTarget = urlPostMessageTarget;
  }

  if (urlAllResourcesOption) {
    result.allResourcesOption = urlAllResourcesOption;
  }

  if (urlCalendarFrontend) {
    result.calendarFrontend = urlCalendarFrontend;
  }

  if (!isArray(result.ref)) {
    // ensure the given ref is an array
    if (result.ref) {
      result.ref = [result.ref];
    } else {
      result.ref = [];
    }
  } else {
    result.ref = [...result.ref]; // ref is frozen sometimes; This should patch this.
  }

  // ensure the given tslRefs is an array
  if (!isArray(result.tslRefs)) {
    if (result.tslRefs) {
      result.tslRefs = [result.tslRefs];
    } else {
      result.tslRefs = [];
    }
  } else {
    result.tslRefs = [...result.tslRefs]; // tslRefs is frozen sometimes; This should patch this.
  }

  // ensure the given prdRefs is an array
  if (!isArray(result.prdRefs)) {
    if (result.prdRefs) {
      result.prdRefs = [result.prdRefs];
    } else {
      result.prdRefs = [];
    }
  } else {
    result.prdRefs = [...result.prdRefs]; // tslRefs is frozen sometimes; This should patch this.
  }

  // don't render anything if...
  if (
    (!result.ref || result.ref.length === 0) &&
    (!result.tslRefs || result.tslRefs.length === 0) &&
    (!result.virtualTslRefs || result.virtualTslRefs.length === 0)
  ) {
    console.log(
      'timum hides itself. No reference in either url or options detected.',
    );
    return;
  }

  for (let i = 0; i < result?.ref?.length; i++) {
    result.ref[i] = transformRef(
      result.ref[i],
      result.platform,
      result.prvUuid,
    );
  }

  for (let i = 0; i < result?.tslRefs?.length; i++) {
    result.tslRefs[i] = transformRef(
      result.tslRefs[i],
      result.platform,
      result.prvUuid,
    );
  }

  for (let i = 0; i < result?.prdRefs?.length; i++) {
    result.prdRefs[i] = transformRef(
      result.prdRefs[i],
      result.platform,
      result.prvUuid,
    );
  }

  if (timumCookie) {
    result.cookie = timumCookie;
  }

  if (urlAuthToken) {
    result.cancelableAppointment = {
      authToken: urlAuthToken,
      appointmentUuid: appointmentUuid,
      customerUuid: urlPData2.id,
      resourceUuid: urlResId,
    };
  }

  if (result.host) {
    setTimumApiHost(result.host);
  }

  if (result.height) {
    result.height = normalizeHeight(result.height);
  }

  return result;
}

/**
 * fills passed in configs with missing props without overriding the ones set externally
 */
const mergeAndDispatchConf = (
  givenAppConfig,
  givenMuiTheme,
  givenFcConfig,
  isThemingAllowed,
  areCustomFieldsAllowed,
  isLocalisationAllowed,
  dispatch,
) => {
  const hasCustomMuiTheme = Object.keys(givenMuiTheme).length > 0;
  const hasCustomAppConfig = Object.keys(givenAppConfig).length > 0;
  let mergedAppConfig = merge({}, primary.appConfig, givenAppConfig);
  let mergedMuiTheme = merge({}, primary.muiTheme, givenMuiTheme);
  let mergedFcConfig = merge({}, primary.fcConfig, givenFcConfig);

  // orders input fields according to their index
  mergedAppConfig.fields = sortFields(mergedAppConfig.fields);

  // remove fields if they have been set to undefined
  removeUndefinedFields(givenAppConfig.fields, mergedAppConfig.fields);

  // and now reset everyhing you just did, if they haven't paid for it
  if (hasCustomAppConfig && !isLocalisationAllowed) {
    mergedAppConfig.localization = primary.appConfig.localization;
  }
  if (hasCustomAppConfig && !areCustomFieldsAllowed) {
    mergedAppConfig.fields = primary.appConfig.fields;
  }

  if (hasCustomAppConfig && !isThemingAllowed) {
    mergedAppConfig.hideTimumFooter = primary.appConfig.hideTimumFooter;
  }

  if (hasCustomMuiTheme && !isThemingAllowed) {
    mergedMuiTheme = primary.muiTheme;
  }

  if (mergedAppConfig.postMessageTarget) {
    addPostMessageCallbacks(mergedAppConfig);
  }

  dispatch(setFcConfig(mergedFcConfig));
  dispatch(setAppConfig(mergedAppConfig));
  dispatch(setMuiTheme(mergedMuiTheme));
};

const ConfigHandler = ({
  muiTheme = {},
  fcConfig = {},
  appConfig = {},
  children,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isThemingAllowed = useSelector(selectIsThemingAllowed);
  const areCustomFieldsAllowed = useSelector(selectAreCustomFieldsAllowed);
  const isLocalisationAllowed = useSelector(selectIsLocalisationAllowed);

  const fetchingPublicDataFailed = React.useMemo(
    () => appConfig?.callbacks?.fetchingPublicDataFailed,
    [appConfig],
  );

  // const givenProducts = React.useMemo(() => appConfig?.prds, [appConfig?.prds]);
  const [fetchPublicData, { isFetching: isFetchingPublicData, error }] =
    useLazyPublicDataQuery();

  const [fetchSpecificBookables, { isFetching: isFetchingSpecificBookables }] =
    useLazySpecificBookablesQuery();

  const [fetchSpecificProducts, { isFetching: isFetchingSpecificProducts }] =
    useLazySpecificProductsQuery();

  // useRenderingTrace('ConfigHandler', {
  //   appConfig,
  //   muiTheme,
  //   fcConfig,
  //   dispatch,
  //   isThemingAllowed,
  //   areCustomFieldsAllowed,
  //   isLocalisationAllowed,
  //   fetchPublicData,
  //   fetchSpecificBookables,
  //   fetchSpecificProducts,
  // });

  const doRerender = useCallback(
    (
      newAppConfig = appConfig,
      newMuiTheme = muiTheme,
      newFcConfig = fcConfig,
    ) => {
      const processedConfig = validateAndPreProcessConfig(newAppConfig);

      if (!processedConfig) return;

      mergeAndDispatchConf(
        processedConfig,
        newMuiTheme,
        newFcConfig,
        isThemingAllowed,
        areCustomFieldsAllowed,
        isLocalisationAllowed,
        dispatch,
      );
      fetchReferencedData(
        t,
        processedConfig,
        dispatch,
        fetchPublicData,
        fetchSpecificBookables,
        fetchSpecificProducts,
      );
    },
    [
      appConfig,
      muiTheme,
      fcConfig,
      isThemingAllowed,
      areCustomFieldsAllowed,
      isLocalisationAllowed,
      dispatch,
      t,
      fetchPublicData,
      fetchSpecificBookables,
      fetchSpecificProducts,
    ],
  );

  useEffect(() => {
    const eventHandler = (event) => {
      if (
        allowedOriginRegex.test(event.origin) &&
        event.data?.type === 'rerender'
      ) {
        doRerender(
          { ...event.data?.appConfig },
          { ...event.data?.muiTheme },
          { ...event.data?.fcConfig },
        );
      } else if (event.data?.type === 'rerender') {
        console.error(
          'ConfigHandler received a message from an unexpected origin:',
          event.origin,
        );
      }
    };

    window.addEventListener('message', eventHandler);

    return () => {
      window.removeEventListener('message', eventHandler);
    };
  }, [doRerender, appConfig]);

  useEffect(() => {
    const processedConfig = validateAndPreProcessConfig(appConfig);

    if (!processedConfig) return;

    mergeAndDispatchConf(
      processedConfig,
      muiTheme,
      fcConfig,
      isThemingAllowed,
      areCustomFieldsAllowed,
      isLocalisationAllowed,
      dispatch,
    );
  }, [
    appConfig,
    muiTheme,
    fcConfig,
    dispatch,
    isThemingAllowed,
    areCustomFieldsAllowed,
    isLocalisationAllowed,
    fetchPublicData,
    fetchSpecificBookables,
    fetchSpecificProducts,
  ]);

  useEffect(() => {
    const processedConfig = validateAndPreProcessConfig(appConfig);

    if (!processedConfig) return;

    fetchReferencedData(
      t,
      processedConfig,
      dispatch,
      fetchPublicData,
      fetchSpecificBookables,
      fetchSpecificProducts,
    );
  }, [
    appConfig,
    dispatch,
    fetchPublicData,
    fetchSpecificBookables,
    fetchSpecificProducts,
    t,
  ]);

  useEffect(() => {
    dispatch(
      setIsWaitingForConf(
        isFetchingSpecificBookables ||
          isFetchingPublicData ||
          isFetchingSpecificProducts,
      ),
    );
  }, [
    dispatch,
    isFetchingPublicData,
    isFetchingSpecificBookables,
    isFetchingSpecificProducts,
  ]);

  if (error) {
    if (fetchingPublicDataFailed) {
      fetchingPublicDataFailed(error);
    }
  }

  return children;
};

ConfigHandler.displayName = 'ConfigHandler';
export default ConfigHandler;
