import React, { useEffect } from 'react';
import Calendar from '../Calendar';
import DetailsView from '../calendars/DetailsView';
import PureListView from '../calendars/PureListView';
import FullCalendarView from '../calendars/FullCalendarView';
import CondensedView from '../calendars/CondensedView';

import CssBaseline from '@mui/material/CssBaseline';

import {
  selectAppConfigProp,
  selectMuiTheme,
  selectSelectedPublicData,
} from '../../slices/appState';
import { useSelector } from 'react-redux';

import {
  ThemeProvider,
  responsiveFontSizes,
  createTheme,
} from '@mui/material/styles';

import CssHandler from './CssHandler';

// I observed flickering of this error when first loading bookingJs
// this should prevent flickering and only show it when its actually needed
function DelayedError({ children }) {
  const [showError, setShowError] = React.useState(null);

  useEffect(() => {
    setTimeout(() => {
      setShowError(true);
    }, 500);
  }, []);

  if (showError) {
    return children;
  }
}

export default function FrontendHandler(props) {
  const publicData = useSelector(selectSelectedPublicData);
  const muiTheme = useSelector(selectMuiTheme);

  const calendarFrontend = useSelector((state) =>
    selectAppConfigProp(state, 'calendarFrontend'),
  );

  const calendarFrontendOptions = useSelector((state) =>
    selectAppConfigProp(state, 'calendarFrontendOptions'),
  );

  let theme = createTheme(muiTheme ?? {});
  theme = responsiveFontSizes(theme, { factor: 2.5 });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <CssHandler />
      {calendarFrontend === 'fullCalendar' && (
        <Calendar
          {...props}
          {...calendarFrontendOptions['fullCalendar']}
          bookables={publicData?.bookables ?? undefined}
          products={publicData?.products ?? undefined}
        >
          <FullCalendarView />
        </Calendar>
      )}
      {calendarFrontend === 'detailsFullCalendar' && (
        <Calendar
          {...props}
          {...calendarFrontendOptions['detailsFullCalendar']}
          bookables={publicData?.bookables ?? undefined}
          products={publicData?.products ?? undefined}
        >
          <DetailsView CalendarView={FullCalendarView} />
        </Calendar>
      )}
      {(calendarFrontend === 'pureListView' /* deprecated */ ||
        calendarFrontend === 'listView') && (
        <Calendar
          {...props}
          {...calendarFrontendOptions['pureListView']}
          {...calendarFrontendOptions['listView']}
          bookables={publicData?.bookables ?? undefined}
          products={publicData?.products ?? undefined}
        >
          <PureListView />
        </Calendar>
      )}
      {calendarFrontend === 'detailsListView' && (
        <Calendar
          {...props}
          {...calendarFrontendOptions['detailsListView']}
          bookables={publicData?.bookables ?? undefined}
          products={publicData?.products ?? undefined}
        >
          <DetailsView CalendarView={PureListView} />
        </Calendar>
      )}
      {calendarFrontend === 'condensedView' && (
        <Calendar
          {...props}
          {...calendarFrontendOptions['condensedView']}
          bookables={publicData?.bookables ?? undefined}
          products={publicData?.products ?? undefined}
        >
          <CondensedView />
        </Calendar>
      )}
      {calendarFrontend === 'detailsCondensedView' && (
        <Calendar
          {...props}
          {...calendarFrontendOptions['detailsCondensedView']}
          bookables={publicData?.bookables ?? undefined}
          products={publicData?.products ?? undefined}
        >
          <DetailsView CalendarView={CondensedView} />
        </Calendar>
      )}
      {publicData && // only show this error if we actually have some data
        calendarFrontend !== 'fullCalendar' &&
        calendarFrontend !== 'detailsFullCalendar' &&
        calendarFrontend !== 'condensedView' &&
        calendarFrontend !== 'detailsCondensedView' &&
        calendarFrontend !== 'detailsListView' &&
        calendarFrontend !== 'listView' &&
        calendarFrontend !== 'pureListView' /* deprecated */ && (
          <DelayedError>
            <>
              You must set calendarFrontend either to <code>fullCalendar</code>,{' '}
              <code>detailsFullCalendar</code>,<code>listView</code>,{' '}
              <code>detailsListView</code>, <code>condensedView</code> or{' '}
              <code>detailsCondensedView</code> in your config.
            </>
          </DelayedError>
        )}
    </ThemeProvider>
  );
}
