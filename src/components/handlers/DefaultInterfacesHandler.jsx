import React, { useContext } from 'react';

import ProductSelector from '../ProductSelector';
import UnknownCustomerForm from '../UnknownCustomerForm';
import IdentifiedCustomerForm from '../IdentifiedCustomerForm';
import CancelAppointmentView from '../CancelAppointmentView';
import ConfirmationView from '../ConfirmationView';
import LoginView from '../LoginView';

import {
  ProductPageContext,
  CustomerFormContext,
  CancelPageContext,
  ConfirmationPageContext,
  LoginPageContext,
  ErrorContext,
} from '../Calendar';
import { useSelector } from 'react-redux';
import { selectAppConfigProp } from '../../slices/appState';

export default function DefaultInterfacesHandler({
  utilizeProductInterface = true,
  utilizeUnknownCustomerInterface = true,
  utilizeIdentifiedCustomerInterface = true,
  utilizeCancelAppointmentInterface = true,
  utilizeConfirmationInterface = true,
  utilizeLoginInterface = true,
  utilizeErrorDialogInterface = true,
}) {
  const ProductPage = useSelector((state) =>
    selectAppConfigProp(state, 'interfaces.ProductPage'),
  );
  const UnkownBookingPage = useSelector((state) =>
    selectAppConfigProp(state, 'interfaces.UnkownBookingPage'),
  );
  const IdentifiedBookingPage = useSelector((state) =>
    selectAppConfigProp(state, 'interfaces.IdentifiedBookingPage'),
  );
  const CancelPage = useSelector((state) =>
    selectAppConfigProp(state, 'interfaces.CancelPage'),
  );
  const ConfirmationPage = useSelector((state) =>
    selectAppConfigProp(state, 'interfaces.ConfirmationPage'),
  );
  const LoginPage = useSelector((state) =>
    selectAppConfigProp(state, 'interfaces.LoginPage'),
  );
  const ErrorDialog = useSelector((state) =>
    selectAppConfigProp(state, 'interfaces.ErrorDialog'),
  );

  const productContext = useContext(ProductPageContext);
  const bookingPageContext = useContext(CustomerFormContext);
  const cancelPageContext = useContext(CancelPageContext);
  const confirmationPageContext = useContext(ConfirmationPageContext);
  const loginPageContext = useContext(LoginPageContext);
  const errorContext = useContext(ErrorContext);

  return (
    <>
      {utilizeProductInterface && ProductPage && (
        <ProductPage
          open={productContext?.open}
          onClose={productContext?.onClose}
        >
          <ProductSelector onProductSelected={productContext?.onClose} />
        </ProductPage>
      )}
      {utilizeIdentifiedCustomerInterface && IdentifiedBookingPage && (
        <IdentifiedBookingPage
          open={bookingPageContext?.identifiedOpen}
          onClose={bookingPageContext?.onClose}
        >
          <IdentifiedCustomerForm {...bookingPageContext} />
        </IdentifiedBookingPage>
      )}
      {utilizeUnknownCustomerInterface && UnkownBookingPage && (
        <UnkownBookingPage
          open={bookingPageContext?.unknownOpen}
          onClose={bookingPageContext?.onClose}
        >
          <UnknownCustomerForm {...bookingPageContext} />
        </UnkownBookingPage>
      )}
      {utilizeCancelAppointmentInterface && CancelPage && (
        <CancelPage
          open={cancelPageContext?.open}
          onClose={cancelPageContext?.onClose}
        >
          <CancelAppointmentView />
        </CancelPage>
      )}
      {utilizeConfirmationInterface && ConfirmationPage && (
        <ConfirmationPage
          open={confirmationPageContext?.open}
          onClose={confirmationPageContext?.onClose}
          allowCloseOnBooking={confirmationPageContext?.allowCloseOnBooking}
        >
          <ConfirmationView />
        </ConfirmationPage>
      )}
      {utilizeLoginInterface && LoginPage && (
        <LoginPage
          open={loginPageContext?.open}
          onClose={loginPageContext?.onClose}
        >
          <LoginView doOnSuccess={loginPageContext?.doOnSuccess} />
        </LoginPage>
      )}
      {utilizeErrorDialogInterface && ErrorDialog && (
        <ErrorDialog
          open={errorContext?.open}
          onClose={errorContext?.onClose}
        />
      )}
    </>
  );
}
