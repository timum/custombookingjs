import React from 'react';
import PropTypes from 'prop-types';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

/**
 * Loading indicator.
 * Color and backdropFilter can be customised.
 *
 * @param {*} props
 */
export default function ProgressComponent({
  visible,
  wrapperSx,
  backdropSx,
  circleSx,
  size,
  children,
}) {
  return (
    <Box
      sx={{
        position: 'relative',
        ...wrapperSx,
      }}
    >
      <Backdrop
        sx={{
          height: '100%',
          width: '100%',
          position: 'absolute',
          pointerEvents: visible ? 'all' : 'none',
          backgroundColor: '#ffffffc5',
          zIndex: (theme) => theme.zIndex.drawer + 1,
          ...backdropSx,
        }}
        open={visible}
      >
        <CircularProgress
          color="secondary"
          size={size}
          sx={{
            zIndex: (theme) => theme.zIndex.drawer + 2,
            ...circleSx,
          }}
        />
      </Backdrop>
      {children}
    </Box>
  );
}

ProgressComponent.propTypes = {
  /** Whether component is shown or not */
  visible: PropTypes.bool.isRequired,
  /** Color of the backdrop element */
  color: PropTypes.string,
  area: PropTypes.string,
};
