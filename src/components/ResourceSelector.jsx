import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import List from '@mui/material/List';
import ListSubheader from '@mui/material/ListSubheader';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';

import {
  selectResToPublicDataMap,
  selectPublicData,
  selectSelectedPublicData,
} from '../slices/appState';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Typography } from '@mui/material';

export default function ResourceSelector({
  onResourceSelected,
  headerVariant = 'h4',
  height,
  HeadlineProps = {},
  ListProps = {},
  ListItemProps = {},
  ListItemButtonProps = {},
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const publicDataMap = useSelector(selectResToPublicDataMap);
  const publicData = useSelector(selectSelectedPublicData);
  const hasResourcesWithImages = useMemo(() => {
    if (!publicDataMap) return false;

    return !!Object.entries(publicDataMap).find(
      ([, data]) => !!data.resource?.imgUrl,
    );
  }, [publicDataMap]);

  if (!publicDataMap) return;

  return (
    <List
      dense
      {...ListProps}
      subheader={
        <ListSubheader sx={{ marginBottom: '24px' }}>
          <Typography
            variant={headerVariant}
            component={'h2'}
            color="text.primary"
            {...HeadlineProps}
          >
            {t('resource_selection_headline')}
          </Typography>
        </ListSubheader>
      }
      sx={[
        {
          maxHeight: height,
          overflowY: 'auto',
          paddingRight: '8px',
        },
        { ...ListProps.sx },
      ]}
    >
      {Object.entries(publicDataMap).map(([, data]) => {
        return (
          <ListItem
            key={data?.ref}
            {...ListItemProps}
            disablePadding
            sx={[
              {
                mx: 'auto',
                marginBottom: '8px',
                minHeight: '36.5px',
              },
              { ...ListItemProps.sx },
            ]}
          >
            <ListItemButton
              selected={
                publicData && publicData?.resource.uuid === data?.resource.uuid
              }
              onClick={() => {
                dispatch(selectPublicData(data));
                if (onResourceSelected) {
                  onResourceSelected(data);
                }
              }}
              {...ListItemButtonProps}
            >
              {data?.resource?.imgUrl && (
                <ListItemAvatar>
                  <Avatar src={data.resource?.imgUrl} />
                </ListItemAvatar>
              )}
              <ListItemText
                inset={!data?.resource?.imgUrl && hasResourcesWithImages}
                primary={data?.resource?.name}
                primaryTypographyProps={{ variant: 'body2' }}
              />
            </ListItemButton>
          </ListItem>
        );
      })}
    </List>
  );
}

ResourceSelector.displayName = 'ResourceSelector';
ResourceSelector.propTypes = {
  /** callback for parent to trigger action on produc selection */
  onProductSelected: PropTypes.func,
};
