import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';
// import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormHelperText from '@mui/material/FormHelperText';
import MenuItem from '@mui/material/MenuItem';
import { MuiTelInput, matchIsValidTel } from 'mui-tel-input';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';

import MuiMarkdown from 'mui-markdown';

import RHFField from './RHFField';

import { isString } from 'lodash';

export const generateValidation = (fields) => {
  const validation = {};

  for (const [key, field] of Object.entries(fields)) {
    if (field.type === 'phoneNumber') {
      validation[key] = yup
        .string()
        .test({
          name: `${key}-required-test`,
          test: (value) => (field.isRequired && !value ? false : true),
          message: 'Notwendig',
        })
        .test({
          name: `${key}-number-test`,
          test: (value) => (value ? matchIsValidTel(value, 'DE') : true),
          message: 'Nummer is nicht valide',
        });
    } else {
      validation[key] = field.validation;
    }
  }

  return validation;
};

export const generateDefaultValues = (fields) => {
  const defaultValues = {};

  for (const [key, field] of Object.entries(fields)) {
    defaultValues[key] = field.prefilled;
  }

  return defaultValues;
};

export default function VariableForm({
  fields,
  containerProps = {},
  textFieldProps = {},
  telFieldProps = {},
  selectFieldProps = {},
  textAreaProps = {},
  disableEmptyErrorText,
  rhfProps,
}) {
  const { t } = useTranslation();

  return (
    <Grid
      container
      item
      sx={{
        maxHeight: '250px',
        overflowY: 'auto',
        overflowX: 'hidden',
        width: '100%',
      }}
      {...containerProps}
    >
      {Object.entries(fields).map(([key, field]) => {
        return (
          <Grid
            item
            key={key}
            xs={12}
            my={1}
            sx={{ display: field.preventRendering ? 'none' : 'flex' }}
          >
            <RHFField
              name={key}
              rhfProps={rhfProps}
              disableEmptyErrorText={disableEmptyErrorText}
            >
              {(error, helperText, rhfFieldProps) => {
                if (!field.type || field.type === 'text') {
                  return (
                    <TextField
                      fullWidth
                      size="small"
                      label={
                        isString(field.title) ? (
                          <MuiMarkdown>{t(field.title)}</MuiMarkdown>
                        ) : (
                          field.title
                        )
                      }
                      type={field.format}
                      error={error}
                      helperText={t(helperText)}
                      {...rhfFieldProps}
                      value={
                        field.onChange
                          ? field.onChange(rhfFieldProps?.value)
                          : rhfFieldProps?.value
                      }
                      {...textFieldProps}
                    />
                  );
                }
                if (field.type === 'phoneNumber') {
                  return (
                    <MuiTelInput
                      fullWidth
                      size="small"
                      label={
                        isString(field.title) ? (
                          <MuiMarkdown>{t(field.title)}</MuiMarkdown>
                        ) : (
                          field.title
                        )
                      }
                      error={error}
                      forceCallingCode
                      defaultCountry={field.defaultCountry || 'DE'}
                      preferredCountries={
                        field.preferredCountries || ['DE', 'CH', 'AT']
                      }
                      helperText={t(helperText) || helperText}
                      {...rhfFieldProps}
                      value={
                        field.onChange
                          ? field.onChange(rhfFieldProps?.value)
                          : rhfFieldProps?.value
                      }
                      {...telFieldProps}
                    />
                  );
                }
                if (field.type === 'checkbox') {
                  return (
                    <FormControl
                      component="fieldset"
                      variant="standard"
                      error={error}
                    >
                      <FormGroup>
                        <FormControlLabel
                          control={<Checkbox {...rhfFieldProps} />}
                          label={
                            isString(field.title) ? (
                              <MuiMarkdown>{t(field.title)}</MuiMarkdown>
                            ) : (
                              field.title
                            )
                          }
                        />
                      </FormGroup>
                      <FormHelperText>
                        {t(helperText) || helperText}
                      </FormHelperText>
                    </FormControl>
                  );
                }
                if (field.type === 'textarea') {
                  const helperTextFn = () => {
                    let text = '';
                    if (field.limit) {
                      text += ` ${rhfFieldProps?.value?.length ?? 0}/${
                        field.limit
                      }`;
                    }

                    if (helperText) {
                      text = t(helperText) || helperText;
                    }

                    return text;
                  };

                  return (
                    <TextField
                      fullWidth
                      multiline
                      size="small"
                      label={
                        isString(field.title) ? (
                          <MuiMarkdown>{t(field.title)}</MuiMarkdown>
                        ) : (
                          field.title
                        )
                      }
                      type={field.format}
                      error={error}
                      helperText={helperTextFn()}
                      inputProps={{
                        maxLength: field.limit ?? 99999,
                      }}
                      {...rhfFieldProps}
                      value={
                        field.onChange
                          ? field.onChange(rhfFieldProps?.value)
                          : rhfFieldProps?.value
                      }
                      {...textAreaProps}
                    />
                  );
                }
                if (field.type === 'select' && field.options) {
                  return (
                    <TextField
                      fullWidth
                      select
                      size="small"
                      label={
                        isString(field.title) ? (
                          <MuiMarkdown>{t(field.title)}</MuiMarkdown>
                        ) : (
                          field.title
                        )
                      }
                      type={field.format}
                      error={error}
                      helperText={t(helperText) || helperText}
                      {...rhfFieldProps}
                      value={
                        field.onChange
                          ? field.onChange(rhfFieldProps?.value)
                          : rhfFieldProps?.value
                      }
                      {...selectFieldProps}
                    >
                      {field.options.map((option) => {
                        return (
                          <MenuItem key={option.key} value={option.key}>
                            {t(option.title)}
                          </MenuItem>
                        );
                      })}
                    </TextField>
                  );
                }
              }}
            </RHFField>
          </Grid>
        );
      })}
    </Grid>
  );
}

VariableForm.propTypes = {
  /** list of fields this form should display*/
  fields: PropTypes.object.isRequired,
  /** additional props for fields which are plain TextFields*/
  textFieldProps: PropTypes.object,
  /** additional props for phone number fields. Can be TextField props as well as props found here: https://viclafouch.github.io/mui-tel-input/docs/react-hook-form/*/
  telFieldProps: PropTypes.object,
  /** additional props for select TextFields. */
  selectFieldProps: PropTypes.object,
  /** additional props for multiline TextFields. */
  textAreaProps: PropTypes.object,
};
