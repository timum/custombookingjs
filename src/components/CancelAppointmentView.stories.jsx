import React from 'react';
import CancelAppointmentView from './CancelAppointmentView';
import ConfigHandler from './handlers/ConfigHandler';
import { rest } from 'msw';

import { DateTime } from 'luxon';

// import TimumApiMockResponses from '../TimumApiMockResponses';

export default {
  component: CancelAppointmentView,
  title: 'CancelAppointmentView',
};

const Template = (args) => {
  return (
    <ConfigHandler>
      {/* -> needed for i18n to work */}
      <CancelAppointmentView {...args} />
    </ConfigHandler>
  );
};

export const Default = {
  render: Template,

  args: {
    timeslot: {
      start: DateTime.now(),
      end: DateTime.now().plus({ hours: 1 }),
    },
    productName: 'Besichtigung',
    resourceName: 'Tuvalu',
  },

  parameters: {
    msw: {
      handlers: {
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data',
          (req, res, ctx) => {
            return res(ctx.status(200), ctx.delay(500));
          },
        ),
        cancelAppointment: rest.delete(
          '*/rest/1/customers/:uuid1/appointments/:uuid2',
          (req, res, ctx) => {
            return res(ctx.status(200), ctx.delay(500));
          },
        ),
      },
    },
  },
};
