import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';

import {
  useCreateAppointmentWithConsumerMutation,
  useReserveAppoinmentMutation,
} from '@timum/timum_pdk';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';

import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

import {
  // selectProduct,
  selectSelectedProduct,
  selectAppConfigProp,
  selectSelectedPublicData,
} from '../slices/appState';
import { useSelector } from 'react-redux';

import { useTranslation } from 'react-i18next';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import BookableDetailsView from './base/BookableDetailsView';

import {
  default as VariableForm,
  generateDefaultValues,
  generateValidation,
} from './VariableForm';

import usePropsBeforeContext from '../hooks/usePropsBeforeContext';
import { CustomerFormContext } from './Calendar';

import { determineType } from '../util/util';

/* There's a lot of duplicated code between this component an UnknownCustomerForm. Todo: make this cleaner */

export default function IdentifiedCustomerForm(props) {
  const { t, i18n } = useTranslation();

  const {
    timeslot,
    productName,
    resourceName,
    placeholderId,
    onBookingSuccessfull,
    onBookingFailed,
    bookingProcess,
    customerData,
  } = usePropsBeforeContext(props, CustomerFormContext);

  // in some cases, if the user was too fast with clicking on the book button, the booking failed because reservation was still in progress
  // and the data of that request wasn't accessible.
  const [, { isLoading: isCreatingReservation }] = useReserveAppoinmentMutation(
    {
      fixedCacheKey: 'reserveAppointmentCache',
    },
  );

  const publicData = useSelector(selectSelectedPublicData);
  const channelKey = useSelector((state) =>
    selectAppConfigProp(state, 'channelKey'),
  );

  const product = useSelector(selectSelectedProduct);
  const pData = useSelector((state) => selectAppConfigProp(state, 'pData'));

  const sendCustomValuesInMessage = useSelector((state) =>
    selectAppConfigProp(state, 'sendCustomValuesInMessage'),
  );

  const customerHasMissingFields = () => {
    if (
      customerData?.missingFields?.firstName ||
      customerData?.missingFields?.lastName ||
      customerData?.missingFields?.email ||
      customerData?.missingFields?.mobile ||
      customerData?.missingFields?.agbs
    ) {
      return true;
    } else {
      return false;
    }
  };

  const fields = useSelector((state) => {
    const allFields = selectAppConfigProp(state, 'fields');
    const fields = {};

    for (const prop of Object.keys(allFields)) {
      /* filter all of our standard fields (except message) */
      if (
        (prop !== 'firstName' || customerData?.missingFields?.firstName) &&
        (prop !== 'lastName' || customerData?.missingFields?.lastName) &&
        (prop !== 'email' || customerData?.missingFields?.email) &&
        (prop !== 'mobile' || customerData?.missingFields?.mobile) &&
        (prop !== 'agbs' || customerData?.missingFields?.agbs)
      ) {
        fields[prop] = allFields[prop];
      }
    }
    return fields;
  });

  const createBookingStarted = useSelector((state) =>
    selectAppConfigProp(state, 'createBookingStarted'),
  );
  const createBookingSuccessful = useSelector((state) =>
    selectAppConfigProp(state, 'createBookingSuccessful'),
  );
  const createBookingFailed = useSelector((state) =>
    selectAppConfigProp(state, 'createBookingFailed'),
  );
  const additionalParams = useSelector((state) =>
    selectAppConfigProp(state, 'additionalParams'),
  );

  const methods = useForm({
    defaultValues: generateDefaultValues(fields),
    mode: 'all',
    resolver: yupResolver(yup.object().shape(generateValidation(fields))),
  });

  const [createBooking, { isLoading: isCreatingBooking }] =
    useCreateAppointmentWithConsumerMutation();

  useEffect(() => {
    if (!fields) return;

    for (const [key, values] of Object.entries(fields)) {
      if (
        values?.preventRenderingFor?.length > 0 &&
        values.preventRenderingFor.includes('identifiedCustomers')
      ) {
        fields[key].preventRendering = true;
        fields[key].validation = undefined;
      }
    }
  }, [fields]);

  const onSubmit = async (data) => {
    if (createBookingStarted) {
      createBookingStarted({
        timeslot,
        ...pData,
      });
    }

    data.locale = i18n.language;

    for (const [key, values] of Object.entries(fields)) {
      if (values.sendToPlatform && data[key]) {
        data[key] = {
          value: data[key],
          type: determineType(data[key]),
          platform: values.sendToPlatform,
          entityType: values.entityType, // add this to an appointment or the customer?
        };
      }
    }

    let msgPayload = data.message;
    if (sendCustomValuesInMessage) {
      msgPayload = '';
      for (const prop of Object.keys(fields)) {
        /* message field should still be part of the payload even though it's default */
        /* but should there still be standard fields we don't need to concatenate those. */
        if (
          prop !== 'firstName' &&
          prop !== 'lastName' &&
          prop !== 'email' &&
          prop !== 'mobile' &&
          prop !== 'agbs' &&
          prop !== 'locale' &&
          data[prop]
        ) {
          if (typeof data[prop] !== 'object') {
            msgPayload += `${prop}: ${data[prop]},  `;
          } else if (data[prop].value) {
            msgPayload += `${prop}: ${data[prop].value},  `;
          }
        }
      }
      msgPayload = msgPayload.slice(0, -3); // remove trailing comma
    }

    const bookingData = {
      channelOrResourceId: publicData?.ref,
      shouldInvalidateCustomerIdentification: customerHasMissingFields(),
      body: {
        ...data,
        start: timeslot.start.toISO({ suppressMilliseconds: true }),
        end: timeslot.end.toISO({ suppressMilliseconds: true }),
        product_uuid: product.uuid,
        timeslot_uuid: timeslot.uuid,
        customer_id: pData ? pData.personId + '@' + pData.platform : undefined,
        placeholder_id: placeholderId,
        channelId: channelKey,
      },
      params: { ...additionalParams },
    };

    if (msgPayload) {
      bookingData.params = {
        message: msgPayload,
      };
    }

    const response = await createBooking(bookingData);

    if (!response.error) {
      // external callback - user defined
      if (createBookingSuccessful) {
        createBookingSuccessful({
          timeslot: timeslot,
          response: response,
          data: { ...data },
          pData: { ...pData },
        });
      }

      // internal - defined by parent
      if (onBookingSuccessfull) {
        onBookingSuccessfull({
          timeslot,
          response,
          data: { ...data },
          pData: { ...pData },
        });
      }
    } else {
      // external callback - user defined
      if (createBookingFailed) {
        createBookingFailed({
          timeslot,
          resonse: response,
          data: { ...data },
          pData: { ...pData },
        });
      }

      // internal - defined by parent
      if (onBookingFailed) {
        onBookingFailed({
          timeslot,
          resonse: response,
          data: { ...data },
          pData: { ...pData },
        });
      }
    }
  };

  return (
    <form onSubmit={methods.handleSubmit(onSubmit)}>
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        mt={2}
        mx="auto"
      >
        <BookableDetailsView
          start={timeslot?.start}
          end={timeslot?.end}
          productName={productName}
          resourceName={resourceName}
          delayReservation={isCreatingReservation}
        />

        <Grid
          item
          borderBottom="1px solid #ddd"
          width={'95%'}
          alignSelf="center"
          mt={2}
          mb={2}
        ></Grid>

        {(customerData?.name || customerData?.email) && (
          <Grid item xs={12} sm={8}>
            <Table size="small">
              <TableBody>
                <TableRow sx={{ border: 'none' }}>
                  <TableCell sx={{ border: 'none', pl: 0 }}>
                    {t('fields.name')}
                  </TableCell>
                  <TableCell
                    sx={{ border: 'none' }}
                  >{`*** ${customerData.name}***`}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell sx={{ border: 'none', pl: 0 }}>
                    {t('fields.email')}
                  </TableCell>
                  <TableCell sx={{ border: 'none' }}>
                    {customerData.email}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Grid>
        )}

        <Grid item mt={2}>
          <Typography paragraph variant="body1">
            {t('identified_customer_hint')}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={9}>
          <VariableForm
            fields={fields}
            containerProps={{
              spacing: 0,
              sx: {
                maxHeight: 'calc(100vh - 180px)',
                overflowY: 'auto',
                overflowX: 'hidden',
                width: '300px',
              },
            }}
            disableEmptyErrorText
            rhfProps={{ control: methods.control }}
          />
        </Grid>

        <Grid item>
          <LoadingButton
            variant="contained"
            color="primary"
            type="submit"
            loading={isCreatingBooking || isCreatingReservation}
          >
            {bookingProcess?.toLowerCase() === 'immediate'
              ? t('submit_button_book')
              : t('submit_button_request')}
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
}

IdentifiedCustomerForm.propTypes = {
  timeslot: PropTypes.object,
  productName: PropTypes.string,
  resourceName: PropTypes.string,
  placeholderId: PropTypes.string,
  onBookingSuccessfull: PropTypes.func,
  onBookingFailed: PropTypes.func,
  bookingProcess: PropTypes.string,
  confirmationComponent: PropTypes.func,
};
