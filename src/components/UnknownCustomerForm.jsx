import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';

import {
  useCreateAppointmentWithConsumerMutation,
  useReserveAppoinmentMutation,
} from '@timum/timum_pdk';
import LoadingButton from '@mui/lab/LoadingButton';

import {
  // selectProduct,
  selectSelectedProduct,
  selectAppConfigProp,
  selectSelectedPublicData,
} from '../slices/appState';
import { useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

import { useTranslation } from 'react-i18next';
import BookableDetailsView from './base/BookableDetailsView';
import {
  default as VariableForm,
  generateDefaultValues,
  generateValidation,
} from './VariableForm';

import usePropsBeforeContext from '../hooks/usePropsBeforeContext';
import { CustomerFormContext } from './Calendar';

import { determineType } from '../util/util';

export default function UnknownCustomerForm(props) {
  const { t, i18n } = useTranslation();

  const {
    timeslot,
    productName,
    resourceName,
    placeholderId,
    onBookingSuccessfull,
    onBookingFailed,
    bookingProcess,
  } = usePropsBeforeContext(props, CustomerFormContext);

  const publicData = useSelector(selectSelectedPublicData);
  const sendCustomValuesInMessage = useSelector((state) =>
    selectAppConfigProp(state, 'sendCustomValuesInMessage'),
  );
  const channelKey = useSelector((state) =>
    selectAppConfigProp(state, 'channelKey'),
  );
  const product = useSelector(selectSelectedProduct);
  const fields = useSelector((state) => selectAppConfigProp(state, 'fields'));
  const createBookingStarted = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.createBookingStarted'),
  );
  const createBookingSuccessful = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.createBookingSuccessful'),
  );

  const createBookingFailed = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.createBookingFailed'),
  );

  const auth2Token = useSelector((state) =>
    selectAppConfigProp(state, 'auth2'),
  );

  const additionalParams = useSelector((state) =>
    selectAppConfigProp(state, 'additionalParams'),
  );

  const methods = useForm({
    defaultValues: generateDefaultValues(fields),
    mode: 'all',
    resolver: yupResolver(yup.object().shape(generateValidation(fields))),
  });

  console.debug(methods.formState.errors);

  const [createBooking, { isLoading: isCreatingBooking }] =
    useCreateAppointmentWithConsumerMutation();

  // in some cases, if the user was too fast with clicking on the book button, the booking failed because reservation was still in progress
  // and the data of that request wasn't accessible.
  const [, { isLoading: isCreatingReservation }] = useReserveAppoinmentMutation(
    {
      fixedCacheKey: 'reserveAppointmentCache',
    },
  );

  const onSubmit = async (data) => {
    if (createBookingStarted) {
      createBookingStarted({
        timeslot: timeslot,
        data: { ...data },
      });
    }

    data.locale = i18n.language;

    // turns the data of the field into an object, interpretable by the backend
    for (const [prop, values] of Object.entries(fields)) {
      if (values.sendToPlatform && data[prop]) {
        data[prop] = {
          value: data[prop],
          type: determineType(data[prop]),
          platform: values.sendToPlatform,
          entityType: values.entityType, // add this to an appointment or the customer?
        };
      }
    }

    let msgPayload = data.message;
    if (sendCustomValuesInMessage) {
      msgPayload = '';
      for (const prop of Object.keys(fields)) {
        /* message field should still be part of the payload even though it's default */
        if (
          prop !== 'firstName' &&
          prop !== 'lastName' &&
          prop !== 'email' &&
          prop !== 'mobile' &&
          prop !== 'agbs' &&
          prop !== 'locale' &&
          data[prop] // don't add undefined to the string
        ) {
          if (typeof data[prop] !== 'object') {
            msgPayload += `${prop}: ${data[prop]},  `;
          } else if (data[prop].value) {
            msgPayload += `${prop}: ${data[prop].value},  `;
          }
        }
      }
      msgPayload = msgPayload.slice(0, -3); // remove trailing comma
    }

    const bookingData = {
      ...data,
      start: timeslot.start.toISO({ suppressMilliseconds: true }),
      end: timeslot.end.toISO({ suppressMilliseconds: true }),
      product_uuid: product.uuid,
      timeslot_uuid: timeslot.uuid,
      placeholder_id: placeholderId,
      email: data.email,
      firstname: data.firstName,
      lastname: data.lastName,
      locale: data.locale,
      mobile: data.mobile,
      channelId: channelKey,
    };

    if (msgPayload) {
      bookingData.message = msgPayload;
    }

    const response = await createBooking({
      channelOrResourceId: publicData?.ref,
      body: bookingData,
      params: { additionalParams },
      headers: auth2Token
        ? {
            auth2: auth2Token,
          }
        : undefined,
    });

    if (!response.error) {
      // external callback - user defined
      if (createBookingSuccessful) {
        createBookingSuccessful({
          timeslot: timeslot,
          response: response,
          data: { ...data },
        });
      }

      // internal - defined by parent
      if (onBookingSuccessfull) {
        onBookingSuccessfull({
          timeslot: timeslot,
          response: response,
          data: { ...data },
        });
      }
    } else {
      console.log('Caught error while creating booking:' + response.error);

      // external callback - user defined
      if (createBookingFailed) {
        createBookingFailed({
          timeslot: timeslot,
          response: response,
          data: { ...data },
        });
      }

      // internal - defined by parent
      if (onBookingFailed) {
        onBookingFailed({
          timeslot: timeslot,
          response: response,
          data: { ...data },
        });
      }
    }
  };

  useEffect(() => {
    if (!fields) return;

    for (const [key, values] of Object.entries(fields)) {
      if (
        values?.preventRenderingFor?.length > 0 &&
        values.preventRenderingFor.includes('unknownCustomers')
      ) {
        fields[key].preventRendering = true;
        fields[key].validation = undefined;
      }
    }
  }, [fields]);

  return (
    <form onSubmit={methods.handleSubmit(onSubmit)}>
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={{ xs: 0, md: 1 }}
        pt={1}
      >
        <Grid item>
          <BookableDetailsView
            start={timeslot?.start}
            end={timeslot?.end}
            productName={productName}
            resourceName={resourceName}
            delayReservation={isCreatingReservation}
          />
        </Grid>

        <VariableForm
          fields={fields}
          containerProps={{
            spacing: 0,
            sx: {
              // maxHeight: 'calc(100vh - 180px)',
              // overflowY: 'auto',
              // overflowX: 'hidden',
              width: '100%',
            },
          }}
          disableEmptyErrorText
          rhfProps={{ control: methods.control }}
        />
        <Grid item>
          <LoadingButton
            variant="contained"
            color="primary"
            type="submit"
            loading={isCreatingBooking || isCreatingReservation}
          >
            {bookingProcess?.toLowerCase() === 'immediate'
              ? t('submit_button_book')
              : t('submit_button_request')}
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
}

UnknownCustomerForm.propTypes = {
  /** callback for parent to trigger action on produc selection */
  onProductSelected: PropTypes.func,
};
