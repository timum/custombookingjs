import React from 'react';

import Avatar from '@mui/material/Avatar';
import LoadingButton from '@mui/lab/LoadingButton';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import { keyframes } from '@mui/system';

import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
// import { ReactComponent as TimumLogo } from '../assets/timum-logo.svg';
import VariableForm, {
  generateDefaultValues,
  generateValidation,
} from './VariableForm';
import * as yup from 'yup';
import { useTranslation, Trans } from 'react-i18next';

import { selectAppConfigProp, mutateAppConfig } from '../slices/appState';
import { useSelector, useDispatch } from 'react-redux';
import { useLazyAuthenticateQuery } from '@timum/timum_pdk/src';

import { useSnackbar } from 'notistack';
import { LoginPageContext } from './Calendar';
import usePropsBeforeContext from '../hooks/usePropsBeforeContext';

const fields = {
  password: {
    title: 'fields.password',
    validation: yup.string().required('validation.field_required'),
    format: 'password',
  },
};

const shake = keyframes`
  8%, 41% {
      transform: translateX(-10px);
  }
  25%, 58% {
      transform: translateX(10px);
  }
  75% {
      transform: translateX(-5px);
  }
  92% {
      transform: translateX(5px);
  }
  0%, 100% {
      transform: translateX(0);
  }
`;

/**
 * Loading indicator.
 * Color and backdropFilter can be customised.
 *
 * @param {*} props
 */
export default function LoginView(props) {
  const { t } = useTranslation();

  const { userName, doOnSuccess } = usePropsBeforeContext(
    props,
    LoginPageContext,
  );
  const { enqueueSnackbar } = useSnackbar();

  const methods = useForm({
    defaultValues: generateDefaultValues(fields),
    resolver: yupResolver(yup.object().shape(generateValidation(fields))),
  });

  const dispatch = useDispatch();
  const host = useSelector((state) => selectAppConfigProp(state, 'host'));

  const [loginFailed, setLoginFailed] = React.useState();
  const [authenticate, { isLoading: isAuthenticating, isError }] =
    useLazyAuthenticateQuery();

  const onSubmit = async (data) => {
    const response = await authenticate({
      body: {
        userName: userName,
        password: data.password,
      },
    });

    if (!response.error) {
      if (doOnSuccess) {
        doOnSuccess();
      }

      dispatch(
        mutateAppConfig({
          pathToProp: 'auth2',
          newValue: response.data.auth2,
        }),
      );

      enqueueSnackbar(t('login.success'), {
        variant: 'success',
      });
    } else {
      setLoginFailed(true);

      setTimeout(() => {
        setLoginFailed(false);
      }, 1000);
    }
  };

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography variant="h2">{t('login.sign_in')}</Typography>

      <Typography variant="subtitle2" textAlign="center" mt={1}>
        <Trans t={t} i18nKey="login.explanation">
          Ein Benutzer mit dieser E-Mail existiert bereits. <br></br> Sie müssen
          sich authentifizieren, bevor Sie buchen können.
        </Trans>
      </Typography>

      <Box borderBottom="1px solid #ddd" width={'80%'} mt={2} mb={2}></Box>

      <Box
        component="form"
        onSubmit={methods.handleSubmit(onSubmit)}
        noValidate
        sx={{ mt: 1 }}
      >
        <Typography variant="body1" textAlign="center">
          {userName}
        </Typography>
        <VariableForm
          fields={fields}
          disableEmptyErrorText
          rhfProps={{ control: methods.control }}
          containerProps={{
            spacing: 0,
          }}
          textFieldProps={{
            sx: {
              animation: loginFailed ? `${shake} 500ms linear 1` : '',
            },
          }}
        />

        {isError && (
          <Typography color="error.main" variant="body1" textAlign={'center'}>
            {t('login.failed')}
          </Typography>
        )}

        <LoadingButton
          type="submit"
          loading={isAuthenticating}
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          {t('login.sign_in')}
        </LoadingButton>

        <Link
          href={`${host}/auth/reset`}
          display="block"
          variant="body2"
          textAlign="center"
          target="_blank"
        >
          {t('login.forgot_password')}
        </Link>
      </Box>
    </Box>
  );
}

LoginView.propTypes = {};
