import React, { useCallback, useMemo } from 'react';

import PhoneIcon from '@mui/icons-material/Phone';
import PhoneIphoneIcon from '@mui/icons-material/PhoneIphone';
import EmailIcon from '@mui/icons-material/Email';
import MenuIcon from '@mui/icons-material/Menu';

import useMediaQuery from '@mui/material/useMediaQuery';
import ShowMoreCard from '../base/ShowMoreCard';

import { useTranslation } from 'react-i18next';
import Image from '../base/Image';

import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';

import useElDimensions from '../../hooks/useElDimensions';
import DefaultInterfacesHandler from '../handlers/DefaultInterfacesHandler';

import usePropsBeforeContext from '../../hooks/usePropsBeforeContext';
import { CalendarViewContext } from '../Calendar';
import { Link } from '@mui/material';

/**
 * Displays bookables in a vertical list.
 * @param {} param0
 * @returns
 */
export default function DetailsView(props) {
  const { t } = useTranslation();
  const {
    // products,
    // product,
    // resource,
    isLoading,
    publicDataMap,
    publicData,
    CalendarView,
    openResourcePage,
    openProductPage,
    height,
  } = usePropsBeforeContext(props, CalendarViewContext);
  const [tslHeadlineRef, tslHeadlineDimensions] = useElDimensions();

  const isSmallScreen = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const [desciptionExpanded, expandDescription] =
    React.useState(!isSmallScreen);
  const toggleDescriptionExpansion = () => {
    expandDescription(!desciptionExpanded);
  };

  const showContactData = useCallback(() => {
    return (
      publicData?.contact?.name ||
      publicData?.contact?.email ||
      publicData?.contact?.mobile ||
      publicData?.contact?.phone
    );
  }, [
    publicData?.contact?.email,
    publicData?.contact?.mobile,
    publicData?.contact?.name,
    publicData?.contact?.phone,
  ]);

  const showResourceSelect = useCallback(() => {
    return (
      openResourcePage &&
      isSmallScreen &&
      publicDataMap &&
      Object.keys(publicDataMap)?.length > 1
    );
  }, [isSmallScreen, openResourcePage, publicDataMap]);

  const showDetails = useMemo(() => {
    return publicData?.resource?.uuid !== 'all-option';
  }, [publicData?.resource?.uuid]);

  const detailsUI = () => {
    if (isLoading) return <LoadingSkeleton />;

    return (
      <>
        <Grid item container xs={12} pb={1}>
          <Grid
            item
            xs
            zeroMinWidth
            pb={1}
            alignSelf={'center'}
            sx={{ position: 'sticky', top: 0, background: '#fff' }}
          >
            <Typography variant="h4" component={'h2'} sx={{ m: 0 }}>
              {publicData?.resource?.name}
            </Typography>
          </Grid>
          {showResourceSelect() && (
            <Grid item xs="auto" pb={1}>
              <Button
                size="small"
                onClick={openResourcePage}
                color="primary"
                variant="outlined"
                sx={{
                  padding: '1px 0',
                  minWidth: '44px',
                  float: 'right',
                }}
              >
                <MenuIcon />
              </Button>
            </Grid>
          )}
        </Grid>
        {publicData?.resource?.imgUrl && (
          <Grid item container xs={12}>
            <Grid
              item
              container
              xs={12}
              sx={{
                p: { xs: 0, sm: 0 },
                mb: { xs: 1, sm: 1 },
                minHeight: '140px',
                maxHeight: `${height / 2}px`,
              }}
            >
              <Image
                src={publicData?.resource?.imgUrl}
                showLoading
                duration={1000}
                fit="scale-down"
              />
            </Grid>
          </Grid>
        )}
        {(publicData?.resource?.description || publicData?.resource?.url) && (
          <Grid item container xs={12}>
            <Grid
              item
              container
              xs={12}
              sx={{
                borderWidth: '1px',
                borderStyle: 'solid',
                borderColor: 'grey.300',
                borderRadius: (theme) => `${theme.shape.borderRadius}px`,
                p: { xs: 1, sm: 2 },
              }}
            >
              {publicData?.resource?.description && (
                <Grid item xs="12">
                  <ShowMoreCard
                    text={publicData?.resource?.description}
                    expanded={desciptionExpanded}
                    doOnExpand={toggleDescriptionExpansion}
                  />
                </Grid>
              )}
              {publicData?.resource?.url && (
                <Grid item xs="12" pt={1}>
                  <Typography variant="body2">
                    {t('common.website')}:{' '}
                    <Link underline="none" href={publicData?.resource?.url}>
                      {t('detailsListView.additional_information')}
                    </Link>
                  </Typography>
                </Grid>
              )}
            </Grid>
          </Grid>
        )}
        {showContactData() && (
          <Grid item container xs={12} sx={{ mt: 1 }}>
            <Grid
              item
              container
              xs={12}
              sx={{
                borderWidth: '1px',
                borderStyle: 'solid',
                borderColor: 'grey.300',
                borderRadius: (theme) => `${theme.shape.borderRadius}px`,
                p: { xs: 1, sm: 2 },
              }}
            >
              <Grid
                item
                xs="12"
                pb={1}
                // sx={{ mb: { xs: publicData?.resource?.description ? 0 : 1 } }}
              >
                <Card elevation={0}>
                  <CardContent
                    sx={{
                      p: '0 !important',
                    }}
                  >
                    <List dense={true} sx={{ p: 0 }}>
                      {publicData?.contact?.name && (
                        <ListItem sx={{ py: 0 }} disableGutters>
                          <ListItemText primary={publicData?.contact?.name} />
                        </ListItem>
                      )}
                      {publicData?.contact?.email && (
                        <ListItem sx={{ py: 0 }} disableGutters>
                          <ListItemIcon sx={{ minWidth: '30px' }}>
                            <EmailIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText
                            primary={
                              <Link
                                underline="none"
                                href={`mailto:${publicData?.contact?.email}`}
                              >
                                {publicData?.contact?.email}
                              </Link>
                            }
                          />
                        </ListItem>
                      )}
                      {publicData?.contact?.phone && (
                        <ListItem sx={{ py: 0 }} disableGutters>
                          <ListItemIcon sx={{ minWidth: '30px' }}>
                            <PhoneIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText
                            primary={
                              <Link
                                underline="none"
                                href={`tel:${publicData?.contact?.phone}`}
                              >
                                {publicData?.contact?.phone}
                              </Link>
                            }
                          />
                        </ListItem>
                      )}
                      {publicData?.contact?.mobile && (
                        <ListItem sx={{ py: 0 }} disableGutters>
                          <ListItemIcon sx={{ minWidth: '30px' }}>
                            <PhoneIphoneIcon fontSize="small" />
                          </ListItemIcon>
                          <ListItemText
                            primary={
                              <Link
                                underline="none"
                                href={`tel:${publicData?.contact?.mobile}`}
                              >
                                {publicData?.contact?.mobile}
                              </Link>
                            }
                          />
                        </ListItem>
                      )}
                    </List>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        )}
      </>
    );
  };

  const largeUI = () => {
    return (
      <Grid container columnSpacing={2} rowSpacing={0}>
        {showDetails && (
          <Grid
            xs={5}
            container
            item
            alignContent={'flex-start'}
            sx={{
              overflowY: 'auto',
              maxHeight: `${height}px`,
              pr: { xs: 0, sm: 2 },
            }}
          >
            {detailsUI()}
          </Grid>
        )}
        <Grid container item xs={showDetails ? 7 : 12}>
          <Grid item xs={12} mb={2}>
            <Typography
              ref={tslHeadlineRef}
              variant="h4"
              component="h2"
              sx={{ m: 0 }}
            >
              {t('detailsListView.chose_date_and_time')}
            </Typography>
          </Grid>
          <Grid item container xs={12}>
            <CalendarView
              suppressDefaultInterfaces
              openProductPage={openProductPage}
              openResourcePage={undefined}
              height={
                tslHeadlineDimensions
                  ? height - tslHeadlineDimensions.height
                  : height
              }
            />
          </Grid>
        </Grid>
        <DefaultInterfacesHandler />
      </Grid>
    );
  };

  const smallUI = () => {
    return (
      <Grid container columnSpacing={1} rowSpacing={1}>
        {showDetails && (
          <Grid container item xs={12} mb={2}>
            {detailsUI()}
          </Grid>
        )}
        <Grid container item xs={12}>
          <Grid item xs={12}>
            <Typography ref={tslHeadlineRef} variant="h4" component="h2">
              {t('detailsListView.chose_date_and_time')}
            </Typography>
          </Grid>
          <Grid item container xs={12}>
            <CalendarView
              suppressDefaultInterfaces
              openProductPage={openProductPage}
              openResourcePage={undefined}
              height={
                tslHeadlineDimensions
                  ? height - tslHeadlineDimensions.height
                  : height
              }
            ></CalendarView>
          </Grid>
        </Grid>
        <DefaultInterfacesHandler />
      </Grid>
    );
  };

  if (isSmallScreen) {
    return smallUI();
  } else {
    return largeUI();
  }
}

const LoadingSkeleton = () => {
  return (
    <Box sx={{ width: '100%' }}>
      {/* Resource Name */}
      <Typography variant="h1" sx={{ mb: 1 }}>
        <Skeleton width="120px" />
      </Typography>

      {/* Resource Image */}
      <Box sx={{ mb: 1 }}>
        <Skeleton variant="rounded" height="250px" width="100%" />
      </Box>

      {/* Resource Description */}
      <Box sx={{ mb: 1 }}>
        <Skeleton variant="rounded" height="250px" width="100%" />
      </Box>

      {/* Contact Data */}
      <Box sx={{ mb: 1 }}>
        <Skeleton variant="rounded" height="250px" width="100%" />
      </Box>
    </Box>
  );
};
