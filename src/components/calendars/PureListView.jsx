import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import BookmarkBorder from '@mui/icons-material/BookmarkBorder';

import { DateTime } from 'luxon';
import { useTranslation } from 'react-i18next';
import DefaultInterfacesHandler from '../handlers/DefaultInterfacesHandler';
// import { motion } from 'framer-motion';

import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';
import ListSubheader from '@mui/material/ListSubheader';
import Button from '@mui/material/Button';
import useElDimensions from '../../hooks/useElDimensions';
import useBookableCurator from '../../hooks/useBookableCurator';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useCallback } from 'react';

import usePropsBeforeContext from '../../hooks/usePropsBeforeContext';
import { CalendarViewContext } from '../Calendar';
import { isObject, isString } from 'lodash';
import useFirstBookbale from '../../hooks/useFirstBookbale';
import ProductSelectorButton from '../base/ProductSelectorButton';

function Blocker({ viewport }) {
  const { t } = useTranslation();

  return (
    <ListItem
      // component={motion.li}
      viewport={{ root: viewport }}
      // initial={{ opacity: 0 }}
      // whileInView={{ opacity: 1 }}
      // exit={{ opacity: 0 }}
      // transition={{ duration: 0.2, delay: 0.05 }}
      sx={{
        pointerEvents: 'none',
        marginBottom: '8px',
      }}
    >
      <ListItemText
        sx={{
          width: '100%',
          textAlign: 'center',
          py: { xs: 0, sm: 0, md: 0 },
        }}
      >
        <Typography variant="body2" color="text.disabled">
          {`${t('appoinment_at_capacity')}`}
        </Typography>
      </ListItemText>
    </ListItem>
  );
}

const Bookable = React.forwardRef(
  ({ timeslot, productName, openBookingPage, viewport }, ref) => {
    return (
      <ListItem
        ref={ref}
        disablePadding
        // component={motion.li}
        viewport={{ root: viewport }}
        // initial={{ opacity: 0 }}
        // whileInView={{ opacity: 1 }}
        // exit={{ opacity: 0 }}
        // transition={{ duration: 0.2, delay: 0.05 }}
        sx={{
          borderWidth: '1px',
          borderStyle: 'solid',
          borderColor: 'grey.300',
          // margin: '15px',
          marginBottom: '8px',
        }}
      >
        <ListItemText
          sx={{
            py: { xs: 2, sm: 2, md: 3 },
          }}
        ></ListItemText>
        <ListItemButton
          sx={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            zIndex: 0,
          }}
          onClick={() => {
            openBookingPage(timeslot);
          }}
        >
          <Typography
            variant="body2"
            color="text.secondary"
            px={{ xs: 0, sm: 0, md: 0 }}
          >
            {`${timeslot?.start?.toLocaleString(DateTime.TIME_SIMPLE)} - ${
              productName || ' '
            }`}
          </Typography>
        </ListItemButton>
      </ListItem>
    );
  },
);
Bookable.displayName = 'Bookable';

function CancelableBookable({ timeslot, openBookingPage, viewport }) {
  const { t } = useTranslation();

  return (
    <ListItem
      disablePadding
      // component={motion.li}
      viewport={{ root: viewport }}
      // initial={{ opacity: 0 }}
      // whileInView={{ opacity: 1 }}
      // exit={{ opacity: 0 }}
      // transition={{ duration: 0.2, delay: 0.05 }}
      sx={{
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'secondary.main',
        marginBottom: '8px',
      }}
    >
      <ListItemText
        sx={{
          py: { xs: 2, sm: 2, md: 3 },
        }}
      ></ListItemText>
      <ListItemButton
        sx={{
          position: 'absolute',
          width: '100%',
          height: '100%',
          zIndex: 0,
        }}
        onClick={() => {
          openBookingPage(timeslot);
        }}
      >
        <Typography
          variant="body2"
          color={'secondary.main'}
          px={{ xs: 2, sm: 2, md: 3 }}
        >
          {`${timeslot?.start?.toLocaleString(DateTime.TIME_SIMPLE)} - ${t(
            'cancellation.cancellable_appointment_highlight',
          )}`}
        </Typography>
      </ListItemButton>
    </ListItem>
  );
}

function SubHeader({ date }) {
  return (
    <ListSubheader
      sx={{
        mb: 1,
      }}
    >
      <Grid
        container
        justifyContent={'space-between'}
        alignItems={'center'}
        sx={{ height: '48px' }}
      >
        <Grid item>
          <Typography variant="body2" color={'primary'}>
            {DateTime.fromISO(date).toLocaleString(DateTime.DATE_FULL)}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body2" color={'text.secondary'}>
            {DateTime.fromISO(date).weekdayLong}
          </Typography>
        </Grid>
      </Grid>
    </ListSubheader>
  );
}

/**
 * Displays bookables in a vertical list.
 * @param {} param0
 * @returns
 */
export default function PureListView(props) {
  const { t } = useTranslation();
  const {
    isLoading,
    bookables,
    products,
    product,
    resource,
    publicDataMap,
    openResourcePage,
    openProductPage,
    openBookingPage,
    height,
    suppressDefaultInterfaces,
  } = usePropsBeforeContext(props, CalendarViewContext);

  const listRef = React.useRef(null);
  const isSmallScreen = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const [autoScrollIndex, setAutoScrollIndex] = React.useState();
  const [oldProduct, setOldProduct] = React.useState();
  const [prdBtnRef, prdBtnDimensions] = useElDimensions(undefined, 'outer');
  const [resBtnRef, resBtnDimensions] = useElDimensions(undefined, 'outer');

  const parsedBookables = useBookableCurator({
    bookables,
    products,
    selectedProduct: product,
  });

  const { index, unifiedBookables } = useFirstBookbale(parsedBookables);

  const onRefChange = useCallback(
    (node) => {
      if (node && autoScrollIndex) {
        node.scrollIntoView({
          behavior: 'smooth', // Optional: define the scrolling behavior
          block: 'center', // Optional: define vertical alignment
        });
      }
    },
    [autoScrollIndex],
  );

  useMemo(() => {
    if (index && oldProduct !== product) {
      setAutoScrollIndex(index);
      setOldProduct(product);
    }
  }, [index, oldProduct, product]);

  const shouldDisplayResourceSelect = useCallback(() => {
    return (
      openResourcePage &&
      isSmallScreen &&
      publicDataMap &&
      Object.keys(publicDataMap).length > 1
    );
  }, [isSmallScreen, openResourcePage, publicDataMap]);

  const btnHeight = prdBtnDimensions?.height + resBtnDimensions?.height;

  if (isLoading) {
    return <LoadingSkeleton />;
  }

  return (
    <Box
      sx={{
        width: '100%',
      }}
    >
      <Grid container rowSpacing={0} m={0} direction={'column'}>
        {shouldDisplayResourceSelect() && (
          <Grid item xs={12}>
            <Button
              ref={resBtnRef}
              variant="outlined"
              fullWidth
              onClick={openResourcePage}
              startIcon={<BookmarkBorder fontSize="small" />}
              endIcon={<ExpandMoreIcon fontSize="10px" />}
            >
              {resource?.name || t('resource_selection_headline')}
            </Button>
          </Grid>
        )}
        {openProductPage && unifiedBookables?.length && (
          <Grid item xs={12} alignSelf="flex-end">
            <ProductSelectorButton
              ref={prdBtnRef}
              openProductPage={openProductPage}
              product={product}
              products={products}
              disabled={products?.length === 1}
              fullWidth={isSmallScreen}
            >
              {product?.name || t('product_selection_headline')}
            </ProductSelectorButton>
          </Grid>
        )}
        <Grid item xs={12}>
          <List
            ref={listRef}
            sx={{
              '& ul': { padding: 0 },
              position: 'relative',
              height:
                height - (btnHeight + 16) /* btnHeight + spacingTop */ ||
                '500px',
              overflowY: 'auto',
              overflowX: 'hidden',
            }}
            subheader={<li />}
          >
            {!unifiedBookables?.length && (
              <Box
                sx={{
                  width: '100%',
                  height: openProductPage
                    ? `calc(100% - ${btnHeight}px)`
                    : '100%',
                  border: '1px solid #ddd',
                  borderRadius: (theme) => {
                    return theme.shape.borderRadius + 'px';
                  },
                  backgroundImage:
                    'linear-gradient(45deg, #f7f7f8 25%, transparent 25%, transparent 50%, #f7f7f8 50%, #f7f7f8 75%, transparent 75%, #fff)',
                  backgroundSize: '14px 14px',
                }}
              >
                {openProductPage && (
                  <ProductSelectorButton
                    disabled={products?.length === 1}
                    openProductPage={openProductPage}
                    product={product}
                    products={products}
                    buttonProps={{
                      ref: prdBtnRef,
                      fullWidth: isSmallScreen,
                    }}
                    wrapperProps={{
                      sx: {
                        position: 'absolute',
                        right: '8px',
                        top: '8px',
                        background: '#fff',
                      },
                    }}
                  >
                    {product?.name || t('product_selection_headline')}
                  </ProductSelectorButton>
                )}
                <Typography
                  variant="body2"
                  color="text.secondary"
                  sx={{
                    position: 'absolute',
                    left: '50%',
                    top: '50%',
                    transform: 'translate(-50%, -50%)',
                    textAlign: 'center',
                  }}
                >
                  {t('noEventsMessage')}
                </Typography>
              </Box>
            )}
            {unifiedBookables?.length > 0 &&
              unifiedBookables.map((data, i) => {
                if (isString(data)) {
                  return <SubHeader key={data} date={data} />;
                }
                if (isObject(data)) {
                  const timeslot = data;
                  if (timeslot.renderAsCancelable) {
                    return (
                      <CancelableBookable
                        key={`${timeslot.start.toISO()}-${timeslot.uuid}`}
                        viewport={listRef}
                        index={i}
                        timeslot={timeslot}
                        openBookingPage={openBookingPage}
                      />
                    );
                  } else if (timeslot.renderAsBookable) {
                    let productName = product?.name;
                    if (
                      !timeslot.hasSelectedProduct &&
                      timeslot.kind !== 'models.Bookable' // pure tsls have no product nor a custom one anyway
                    ) {
                      productName = timeslot.product_name;
                    }

                    return (
                      <Bookable
                        key={`${timeslot.start.toISO()}-${timeslot.uuid}`}
                        ref={autoScrollIndex === i ? onRefChange : undefined}
                        viewport={listRef}
                        index={i}
                        timeslot={timeslot}
                        productName={productName}
                        openBookingPage={openBookingPage}
                      />
                    );
                  } else if (timeslot.renderAsBlocker) {
                    return (
                      <Blocker
                        index={i}
                        viewport={listRef}
                        key={`${timeslot.start.toISO()}-${timeslot.uuid}`}
                      />
                    );
                  }
                }
              })}
          </List>
        </Grid>
      </Grid>
      {!suppressDefaultInterfaces && <DefaultInterfacesHandler />}
    </Box>
  );
}

const LoadingSkeleton = () => {
  return (
    <Box sx={{ width: '100%' }}>
      {/* Prod Button */}
      <Skeleton
        variant="rounded"
        height="36px"
        width="160px"
        sx={{
          position: 'absolute',
          right: '0px',
        }}
      />

      {/* Date and Day */}
      <Typography
        variant="subtitle1"
        component="div"
        sx={{ mb: 1, mt: '44px' }}
      >
        <Skeleton width="100px" />
      </Typography>

      {/* Time Slots */}
      {[...Array(3)].map((_, index) => (
        <Box key={index} sx={{ mb: 1 }}>
          <Skeleton variant="rounded" height="56px" width="100%" />
        </Box>
      ))}

      {/* Date and Day */}
      <Typography variant="subtitle1" component="div" sx={{ mb: 1 }}>
        <Skeleton width="100px" />
      </Typography>

      {/* Time Slots */}
      {[...Array(2)].map((_, index) => (
        <Box key={index} sx={{ mb: 1 }}>
          <Skeleton variant="rounded" height="56px" width="100%" />
        </Box>
      ))}
    </Box>
  );
};

PureListView.propTypes = {
  bookables: PropTypes.object.isRequired,
  product: PropTypes.object,
  products: PropTypes.array,
  openProductPage: PropTypes.func,
  openBookingPage: PropTypes.func,
  height: PropTypes.number,
};
