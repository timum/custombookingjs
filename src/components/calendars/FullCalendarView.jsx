import React, { useCallback, useEffect } from 'react';
import Box from '@mui/system/Box';

import useMediaQuery from '@mui/material/useMediaQuery';

import { useSelector } from 'react-redux';

import FullCalendar from '@fullcalendar/react'; // must go before plugins
import listPlugin from '@fullcalendar/list'; // a plugin!
import timegridPlugin from '@fullcalendar/timegrid';
import daygridPlugin from '@fullcalendar/daygrid';
import multimonthPlugin from '@fullcalendar/multimonth';
import allLocales from '@fullcalendar/core/locales-all';

import { selectFcConfig } from '../../slices/appState';

import i18n from 'i18next';

import { DateTime } from 'luxon';
import useElDimensions from '../../hooks/useElDimensions';
import useBookableCurator from '../../hooks/useBookableCurator';
import { useTranslation } from 'react-i18next';

import DefaultInterfacesHandler from '../handlers/DefaultInterfacesHandler';
import usePropsBeforeContext from '../../hooks/usePropsBeforeContext';
import { CalendarViewContext } from '../Calendar';
import useFirstBookbale from '../../hooks/useFirstBookbale';

const parseBookables = function (curatedBookables, t) {
  if (!curatedBookables) return [];

  const parsedDates = [];

  for (const [, timeslots] of curatedBookables) {
    for (const timeslot of timeslots) {
      let event;

      if (timeslot.renderAsCancelable) {
        // render as cancelable
        event = {
          start: timeslot.start.toISO({
            suppressMilliseconds: true,
          }),
          end: timeslot.end.toISO({
            suppressMilliseconds: true,
          }),
          title: t('cancellation.cancellable_appointment_highlight'),
          extendedProps: timeslot,
          borderColor: '#000',
        };
      } else if (timeslot.renderAsBookable) {
        // render as bookable
        event = {
          start: timeslot.start.toISO({
            suppressMilliseconds: true,
          }),
          end: timeslot.end.toISO({
            suppressMilliseconds: true,
          }),
          title: '',
          extendedProps: timeslot,
        };
      } else if (timeslot.renderAsBlocker) {
        // render as blocker
        event = {
          title: t('appoinment_at_capacity'),
          display: 'background',
          backgroundColor: '#ccc',
          start: timeslot.start.toISO({
            suppressMilliseconds: true,
          }),
          end: timeslot.end.toISO({
            suppressMilliseconds: true,
          }),
          extendedProps: timeslot,
        };
      }
      parsedDates.push(event);
    }
  }

  return parsedDates;
};

export default function FullCalendarView(props) {
  const { t } = useTranslation();
  const {
    bookables,
    products,
    product,
    resource,
    publicDataMap,
    openResourcePage,
    openProductPage,
    openBookingPage,
    height,
  } = usePropsBeforeContext(props, CalendarViewContext);

  const readyFcConfig = useSelector((state) => selectFcConfig(state));
  const calRef = React.useRef(null);
  const [ref, { width }] = useElDimensions();
  const isSmallScreen = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const [oldProduct, setOldProduct] = React.useState();
  const curatedBookables = useBookableCurator({
    bookables,
    products,
    selectedProduct: product,
  });
  const { bookable: firstBookable } = useFirstBookbale(curatedBookables);

  const events = parseBookables(curatedBookables, t);

  useEffect(() => {
    if (!oldProduct !== product && firstBookable) {
      const calApi = calRef.current.getApi();
      calApi.gotoDate(DateTime.fromISO(firstBookable?.start).toISODate());

      let scrollTime = DateTime.fromISO(firstBookable?.start).minus({
        hours: 5,
      });

      // Define the threshold time (8 AM) based on the DateTime's current date
      let eightAM = DateTime.fromObject({
        hour: 8,
        minute: 0,
        second: 0,
        millisecond: 0,
      });

      // Check if the new time is earlier than 8 AM
      if (scrollTime > eightAM) {
        calApi.scrollToTime(scrollTime.toFormat('HH:mm'));
      }

      setOldProduct(product);
    }
  }, [events, firstBookable, firstBookable?.start, oldProduct, product]);

  const shouldDisplayResourceSelect = useCallback(() => {
    return (
      openResourcePage &&
      isSmallScreen &&
      publicDataMap &&
      Object.keys(publicDataMap).length > 1
    );
  }, [isSmallScreen, openResourcePage, publicDataMap]);

  return (
    <Box ref={ref} sx={{ flexGrow: 1 }}>
      <FullCalendar
        ref={calRef}
        locales={allLocales}
        locale={i18n.resolvedLanguage}
        height={height}
        timeZone="utc"
        plugins={[listPlugin, timegridPlugin, daygridPlugin, multimonthPlugin]}
        customButtons={{
          productSelection: {
            text: product?.name ?? t('product_selection_headline'),
            click: () => {
              openProductPage();
            },
          },
          resourceSelection: {
            text: resource?.name ?? t('resource_selection_headline'),
            click: () => {
              openResourcePage();
            },
          },
        }}
        headerToolbar={{
          left: shouldDisplayResourceSelect()
            ? 'resourceSelection productSelection'
            : 'productSelection',
          right: 'today prev,next',
        }}
        noEventsText={t('noEventsMessage')}
        events={events}
        eventClick={(event) => {
          const timeslot = { ...event.event.extendedProps };
          timeslot.start = DateTime.fromISO(event.event.startStr);
          timeslot.end = DateTime.fromISO(event.event.endStr);
          openBookingPage(timeslot);
        }}
        windowResize={() => {
          // only enable ui responsiveness if it's properly configured
          if (readyFcConfig.smallView && readyFcConfig.largeView) {
            if (width < 601) {
              calRef?.current?.getApi()?.changeView(readyFcConfig.smallView);
            } else {
              calRef?.current?.getApi()?.changeView(readyFcConfig.largeView);
            }
          }
        }}
        {...readyFcConfig}
      />
      <DefaultInterfacesHandler />
    </Box>
  );
}
