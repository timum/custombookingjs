import React, { useMemo, useCallback } from 'react';

import useBookableCurator from '../../hooks/useBookableCurator';
import DefaultInterfacesHandler from '../handlers/DefaultInterfacesHandler';

import { selectAppConfigProp } from '../../slices/appState';
import { useSelector } from 'react-redux';

import { motion } from 'framer-motion';

import { DateTime } from 'luxon';
import {
  Grid,
  Typography,
  Button,
  IconButton,
  Box,
  List,
  ListItem,
  Container,
} from '@mui/material';
import Skeleton from '@mui/material/Skeleton';

import useElDimensions from '../../hooks/useElDimensions';
import useWindowDimensions from '../../hooks/useWindowDimensions';
import useMediaQuery from '@mui/material/useMediaQuery';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import BookmarkBorder from '@mui/icons-material/BookmarkBorder';

import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

import { useTranslation } from 'react-i18next';
import ProductSelectorButton from '../base/ProductSelectorButton';
import usePropsBeforeContext from '../../hooks/usePropsBeforeContext';
import { CalendarViewContext } from '../Calendar';

function DesktopHeader({
  displayResourceButton,
  displayProductElement,
  increaseButton,
  decreaseButton,
  productElement,
  resourceButton,
}) {
  return (
    <Grid container item xs={12} mb={2} spacing={1}>
      {displayResourceButton && (
        <Grid item xs={12} sm="auto">
          {resourceButton}
        </Grid>
      )}
      {displayProductElement && (
        <Grid item xs={12} sm="auto">
          {productElement}
        </Grid>
      )}
      <Grid
        item
        xs={12}
        sm
        container
        spacing={1}
        justifyContent={{ xs: 'space-between', sm: 'flex-end' }}
      >
        <Grid item xs="auto">
          {decreaseButton}
        </Grid>
        <Grid item xs="auto">
          {increaseButton}
        </Grid>
      </Grid>
    </Grid>
  );
}

function MobileHeader({
  displayResourceButton,
  displayProductElement,
  increaseButton,
  decreaseButton,
  productElement,
  resourceButton,
}) {
  return (
    <Grid container item xs={12} mb={2}>
      <Grid
        item
        xs={12}
        sm
        container
        alignItems={'center'}
        justifyContent={{ xs: 'space-between', sm: 'flex-end' }}
      >
        <Grid item xs="auto">
          {decreaseButton}
        </Grid>
        <Grid item container xs justifyContent={'center'} alignItems={'center'}>
          {displayResourceButton && (
            <Grid item xs>
              {resourceButton}
            </Grid>
          )}
          {displayProductElement && (
            <Grid item xs sm="auto">
              {productElement}
            </Grid>
          )}
        </Grid>
        <Grid item xs="auto">
          {increaseButton}
        </Grid>
      </Grid>
    </Grid>
  );
}

function Column({ date, timeslots, width, openBookingPage }) {
  const { t } = useTranslation();
  const title = useMemo(() => {
    return `${DateTime.fromISO(date).toLocaleString({
      weekday: 'short',
    })} ${DateTime.fromISO(date).toLocaleString({
      day: '2-digit',
      month: '2-digit',
    })}
      `;
  }, [date]);

  if (!timeslots) return <></>;

  /*
    See here for an indepth explanation of when it is okay to use index as react list keys:
    https://stackoverflow.com/questions/59517962/react-using-index-as-key-for-items-in-the-list
  */

  return (
    <Grid key={date} container item xs="auto" alignContent={'flex-start'}>
      <Grid item xs={12} height={'25px'}>
        <Typography sx={{ textAlign: 'center' }}>{title}</Typography>
      </Grid>
      <Grid
        container
        item
        xs
        direction={'row'}
        alignItems="center"
        rowSpacing={1}
        sx={{ minWidth: width - 8, mt: 1 }}
      >
        {timeslots.map((tsl, index) => {
          if (tsl.renderAsBookable) {
            // appointment
            return (
              <Grid item xs={12} key={index}>
                <Button
                  fullWidth
                  variant="outlined"
                  onClick={() => openBookingPage(tsl)}
                >
                  {tsl.start.toLocaleString({
                    hour: '2-digit',
                    minute: '2-digit',
                  })}
                </Button>
              </Grid>
            );
          } else if (tsl.renderAsCancelable) {
            return (
              <Grid item xs={12} key={index}>
                <Button
                  variant="outlined"
                  fullWidth
                  onClick={() => openBookingPage(tsl)}
                >
                  {tsl.start.toLocaleString({
                    hour: '2-digit',
                    minute: '2-digit',
                  })}
                </Button>
              </Grid>
            );
          } else if (tsl.renderAsBlocker) {
            return (
              <Grid item xs={12} key={index}>
                <Button
                  variant="text"
                  fullWidth
                  disabled
                  sx={{ textTransform: 'inherit' }}
                >
                  {t('appoinment_at_capacity')}
                </Button>
              </Grid>
            );
          }
        })}
      </Grid>
    </Grid>
  );
}

/**
 * Displays bookables in a vertical list.
 * @param {} param0
 * @returns
 */
export default function CondensedView(props) {
  const { t } = useTranslation();
  const {
    isLoading,
    bookables,
    products,
    product,
    resource,
    publicDataMap,
    openResourcePage,
    openProductPage,
    openBookingPage,
    height,
  } = usePropsBeforeContext(props, CalendarViewContext);

  const [headerRef, headerDimensions] = useElDimensions(undefined, 'outer');
  const { width: windowWidth } = useWindowDimensions();
  const [disableIncreaseButton, setDisableIncreaseButton] =
    React.useState(false);
  const [disableDecreaseButton, setDisableDecreaseButton] =
    React.useState(true);
  const listRef = React.useRef(null);
  const [colWidthBase] = React.useState(120);
  const [colWidth, setColWidth] = React.useState(colWidthBase);

  const calendarFrontendOptions = useSelector((state) =>
    selectAppConfigProp(state, 'calendarFrontendOptions'),
  );

  const isSmallScreen = useMediaQuery((theme) => theme.breakpoints.down('sm'));
  const parsedBookables = useBookableCurator({
    bookables,
    products,
    selectedProduct: product,
  });

  const maxColumns = useMemo(() => {
    return Math.min(
      Math.max(Math.floor(windowWidth / 136), 1 /* show a minimum of 1 day */),
      5, // show a maximum of 5 days
    );
  }, [windowWidth]);

  const increaseScroll = () => {
    if (listRef?.current) {
      // Calculate the index of the current item
      let currentItemIndex = Math.ceil(listRef.current.scrollLeft / 136);

      // Calculate the scrollLeft value required to fully render the next item
      let scrollTarget = (currentItemIndex + 1) * 136;

      // Smoothly scroll to the next item
      listRef.current.scrollTo({
        left: scrollTarget,
        behavior: 'smooth',
      });

      if (
        scrollTarget >=
        listRef.current.scrollWidth - listRef.current.clientWidth
      ) {
        setDisableIncreaseButton(true);
      }

      if (scrollTarget > 0) {
        setDisableDecreaseButton(false);
      }
    }
  };

  const decreaseScroll = () => {
    if (listRef) {
      // Calculate the index of the current item
      let currentItemIndex = Math.ceil(listRef.current.scrollLeft / 136);

      // Calculate the scrollLeft value required to fully render the next item
      let scrollTarget = (currentItemIndex - 1) * 136;

      // Smoothly scroll to the next item
      listRef.current.scrollTo({
        left: scrollTarget,
        behavior: 'smooth',
      });

      if (scrollTarget <= 0) {
        setDisableDecreaseButton(true);
      }

      if (
        scrollTarget <
        listRef.current.scrollWidth - listRef.current.clientWidth
      ) {
        setDisableIncreaseButton(false);
      }
    }
  };

  const shouldDisplayResourceSelect = useCallback(() => {
    return (
      openResourcePage &&
      (isSmallScreen ||
        calendarFrontendOptions.condensedView.forceResourceSelectorDialog) &&
      publicDataMap &&
      Object.keys(publicDataMap).length > 1
    );
  }, [
    calendarFrontendOptions.condensedView.forceResourceSelectorDialog,
    isSmallScreen,
    openResourcePage,
    publicDataMap,
  ]);

  useMemo(() => {
    if (parsedBookables.length < maxColumns) {
      setDisableIncreaseButton(true);
      setDisableDecreaseButton(true);
    } else {
      setDisableIncreaseButton(false);
    }
  }, [parsedBookables?.length, maxColumns]);

  useMemo(() => {
    const filledColumns = Math.min(parsedBookables.length, maxColumns);
    setColWidth(colWidthBase + 30 * (maxColumns - filledColumns));
  }, [colWidthBase, maxColumns, parsedBookables.length]);

  // find longest list
  let maxLength = 0;
  for (const entry of parsedBookables) {
    for (const timeslots of Object.values(entry)) {
      timeslots.length > maxLength ? (maxLength = timeslots.length) : null;
    }
  }

  if (isLoading) return <LoadingSkeleton />;

  return (
    <>
      <Grid
        xs
        item
        container
        justifyContent={'center'}
        sx={{ maxWidth: `${(maxColumns + 2) * colWidthBase}px`, mx: 'auto' }}
      >
        {isSmallScreen && (
          <Grid ref={headerRef} item xs={12}>
            <MobileHeader
              displayProductElement={openProductPage}
              displayResourceButton={shouldDisplayResourceSelect()}
              resourceButton={
                <Button
                  variant="outlined"
                  size="small"
                  fullWidth
                  onClick={openResourcePage}
                  startIcon={<BookmarkBorder fontSize="small" />}
                  endIcon={<ExpandMoreIcon fontSize="10px" />}
                  sx={{ textTransform: 'inherit' }}
                >
                  {resource?.name || t('resource_selection_headline')}
                </Button>
              }
              productElement={
                <ProductSelectorButton
                  openProductPage={openProductPage}
                  product={product}
                  products={products}
                />
              }
            />
          </Grid>
        )}
        {!isSmallScreen && (
          <Grid ref={headerRef} item xs={12}>
            <DesktopHeader
              displayProductElement={openProductPage}
              displayResourceButton={shouldDisplayResourceSelect()}
              decreaseButton={
                <IconButton
                  size="small"
                  color="primary"
                  onClick={decreaseScroll}
                  disabled={disableDecreaseButton}
                  edge="end"
                >
                  <ArrowBackIosIcon fontSize="small" />
                </IconButton>
              }
              increaseButton={
                <IconButton
                  size="small"
                  color="primary"
                  onClick={increaseScroll}
                  disabled={disableIncreaseButton}
                >
                  <ArrowForwardIosIcon fontSize="small" />
                </IconButton>
              }
              resourceButton={
                <Button
                  variant="outlined"
                  size="small"
                  fullWidth
                  onClick={openResourcePage}
                  startIcon={<BookmarkBorder fontSize="small" />}
                  endIcon={<ExpandMoreIcon fontSize="10px" />}
                  sx={{ textTransform: 'inherit' }}
                >
                  {resource?.name || t('resource_selection_headline')}
                </Button>
              }
              productElement={
                <ProductSelectorButton
                  openProductPage={openProductPage}
                  product={product}
                  products={products}
                />
              }
            />
          </Grid>
        )}
        <Grid
          // ref={contentRef}
          item
          container
          xs={12}
          justifyContent={
            parsedBookables.length === 0 ? 'center' : 'flex-start'
          }
          sx={{
            minHeight: '200px',
            maxHeight: height - headerDimensions.height,
            overflowY: 'auto',
          }}
        >
          {parsedBookables.length === 0 && (
            <Grid item alignSelf={'center'}>
              <Typography
                sx={{
                  textAlign: 'center',
                  color: (theme) => theme.palette.text.secondary,
                }}
              >
                {t('noEventsMessage')}
              </Typography>
            </Grid>
          )}
          {parsedBookables.length > 0 && (
            <Grid item container xs justifyContent={'center'}>
              <Box
                ref={listRef}
                sx={{
                  width: '100%',
                  maxWidth: `${colWidthBase * 6}px`,
                  overflowY: 'hidden',
                }}
              >
                <List
                  sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent:
                      parsedBookables.length >= maxColumns
                        ? 'flex-start'
                        : 'center',
                    alignItems: 'flex-start',
                  }}
                >
                  {parsedBookables.map(([date, timeslots]) => {
                    return (
                      <ListItem
                        id={date}
                        key={date}
                        sx={{
                          px: 2,
                          justifyContent: 'center',
                          flexGrow: 0,
                          flexBasis: 'auto',
                          width: `${colWidth}px`,
                          flexShrink: 0,
                        }}
                      >
                        <motion.div
                          initial={{ opacity: 0 }}
                          whileInView={{ opacity: 1, x: 0 }}
                          exit={{ opacity: 0 }}
                          viewport={{ root: listRef }}
                          transition={{ duration: 0.3 }}
                          amount="all"
                          layout="position"
                        >
                          <Column
                            date={date}
                            timeslots={timeslots}
                            openBookingPage={openBookingPage}
                            width={colWidth}
                          />
                        </motion.div>
                      </ListItem>
                    );
                  })}
                </List>
              </Box>
            </Grid>
          )}
        </Grid>
      </Grid>
      <DefaultInterfacesHandler />
    </>
  );
}

const LoadingSkeleton = () => {
  // Days and their respective dates
  const days = [
    { label: 'Wed 03/05', occupied: false },
    { label: 'Thu 03/06', occupied: true },
    { label: 'Fri 03/07', occupied: false },
    { label: 'Sat 03/08', occupied: true },
    { label: 'Sun 03/09', occupied: false },
    { label: 'Mon 03/10', occupied: true },
  ];

  // Time slots
  const timeSlots = [
    { time: '08:00 AM' },
    { time: '09:20 AM' },
    { time: '10:40 AM' },
    { time: '11:00 AM' },
    { time: '12:20 AM' },
    { time: '1:40 PM' },
    { time: '3:40 PM' },
    { time: '4:40 PM' },
  ];

  return (
    <Box sx={{ width: '100%' }}>
      <Grid container justifyContent={'space-between'}>
        {/* Prod Button */}
        <Grid item xs="auto">
          <Skeleton variant="rounded" width="120px" height="38px" />
        </Grid>
        {/* Nav Buttons */}
        <Grid item xs="auto">
          <Skeleton variant="rounded" width="120px" height="38px" />
        </Grid>
      </Grid>

      {/* Day Headers */}
      <Container maxWidth="md">
        <Grid container spacing={{ xs: 1, sm: 2 }}>
          {days.map((day, index) => (
            <Grid item xs={2} key={index} textAlign="center">
              <Skeleton
                variant="text"
                height={36}
                sx={{ mt: 1, width: '100%', maxWidth: '110px' }}
              />
            </Grid>
          ))}
        </Grid>

        {/* Time Slot Rows */}
        {timeSlots.map((slot, rowIndex) => (
          <Grid container spacing={{ xs: 1, sm: 2 }} key={rowIndex}>
            {days.map((day, colIndex) => {
              return (
                <>
                  {(colIndex !== 0 || colIndex !== 5) && (
                    <Grid item xs={2} key={colIndex} textAlign="center">
                      <Skeleton
                        variant="rounded"
                        height={36}
                        sx={{ mt: 1, width: '100%', maxWidth: '110px' }}
                      />
                    </Grid>
                  )}
                </>
              );
            })}
          </Grid>
        ))}
      </Container>
    </Box>
  );
};
