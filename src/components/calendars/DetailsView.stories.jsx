import DetailsView from './DetailsView';
import TimumApiMockResponses from '../../TimumApiMockResponses';
import { faker } from '@faker-js/faker';

export default {
  component: DetailsView,
  title: 'DetailsView',
};

const rawBookables = TimumApiMockResponses.getTimeslotsResponse()
  .setProviderId(faker.datatype.uuid())
  .setResourceId(faker.datatype.uuid())
  .including(5)
  .daysInPast()
  .including(5)
  .daysInFuture()
  .including()
  .today()
  .including(6)
  .timeslotsPerDay()
  .including(3)
  .timeslotsWithAppointmentsPerDay()
  .build().timeslots;

export const Default = {
  args: {
    bookables: rawBookables,
    product: {
      uuid: faker.datatype.uuid(),
      name: faker.commerce.productName(),
      description: faker.commerce.productDescription(),
      minDuration: faker.random.numeric(),
    },
    publicData: {
      contact: {
        name: 'Peter Lustig',
        email: 'lustig@villa_regenbogen.de',
        mobile: '01777483222',
        phone: '0339703342342',
      },
      resource: {
        name: 'Wohnung zu verkaufen in Avenida de Augusta',
        description:
          '# Wohnung zum Verkauf im Montañar I von Javea \n' +
          'nur wenige Minuten vom Fontana-Kanal und dem Meer entfernt. Anwesen mit wenig Antike besteht aus zwei Schlafzimmern mit Einbauschränken, zwei Bädern der Master en suite, mit großem Wohn- und Esszimmer, Küche definiert, große Terrasse mit ausgezeichneter Ausrichtung, Blick auf den Pool und Grünflächen, wie viel mit Klimaanlagenkanälen, doppelt verglasten Fenstern, Garage und Abstellraum, ausgezeichnete Bedingungen,   Eigentum in der Gegend von Avenida Augusta in der Nähe aller Dienstleistungen und Freizeitbereiche.  Es ist lizenziert Tourist.  ',
        msgHelpText: '',
      },
      provider: {
        name: 'Lila Lustig GmbH',
      },
    },
    openProductPage: () => {},
    openBookingPage: () => {},
    noEventsText: 'Keine Ereignisse',
  },
};

export const WithoutContactData = {
  args: {
    ...Default.args,
    publicData: {
      contact: {
        name: null,
        email: null,
        mobile: null,
        phone: null,
      },
      resource: {
        name: 'Wohnung zu verkaufen in Avenida de Augusta',
        description:
          '# Wohnung zum Verkauf im Montañar I von Javea \n ' +
          'nur wenige Minuten vom Fontana-Kanal und dem Meer entfernt. \n ' +
          '## Grundriss \n ' +
          'Anwesen mit wenig Antike besteht aus zwei Schlafzimmern mit Einbauschränken, zwei Bädern der Master en suite, mit großem Wohn- und Esszimmer, Küche definiert, große Terrasse mit ausgezeichneter Ausrichtung, Blick auf den Pool und Grünflächen, wie viel mit Klimaanlagenkanälen, doppelt verglasten Fenstern, Garage und Abstellraum, ausgezeichnete Bedingungen,   Eigentum in der Gegend von Avenida Augusta in der Nähe aller Dienstleistungen und Freizeitbereiche.  Es ist lizenziert Tourist.  ',
        msgHelpText: '',
      },
      provider: {
        name: 'Lila Lustig GmbH',
      },
    },
  },
};

export const NoEvents = {
  args: {
    ...Default.args,
    bookables: undefined,
  },
};

export const AFewEvents = {
  args: {
    ...Default.args,
    bookables: TimumApiMockResponses.getTimeslotsResponse()
      .setProviderId(faker.datatype.uuid())
      .setResourceId(faker.datatype.uuid())
      .including(1)
      .daysInFuture()
      .including()
      .today()
      .including(1)
      .timeslotsPerDay()
      .including(1)
      .timeslotsWithAppointmentsPerDay()
      .build().timeslots,
  },
};

export const ShortInfo = {
  args: {
    ...Default.args,
    publicData: {
      contact: {
        name: 'Peter Lustig',
        email: 'loewenhahrn@web.de',
        mobile: null,
        phone: null,
      },
      resource: {
        name: 'Wohnung zu verkaufen in Avenida de Augusta',
        description: null,
        msgHelpText: '',
      },
      provider: {
        name: 'Lila Lustig GmbH',
      },
    },
    bookables: TimumApiMockResponses.getTimeslotsResponse()
      .setProviderId(faker.datatype.uuid())
      .setResourceId(faker.datatype.uuid())
      .including(1)
      .daysInFuture()
      .including()
      .today()
      .including(1)
      .timeslotsPerDay()
      .including(1)
      .timeslotsWithAppointmentsPerDay()
      .build().timeslots,
  },
};
