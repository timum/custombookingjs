import React from 'react';
// import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';

/**
 * Loading indicator.
 * Color and backdropFilter can be customised.
 *
 * @param {*} props
 */
export default function Footer() {
  return (
    <Box
      sx={{
        height: '28px !important',
        width: '100% !important',
        position: 'sticky !important',
        bottom: '0 !important',
        left: '0 !important',
        display: 'flex',
        backgroundColor: '#fff',
        borderTopLeftRadius: (theme) => `${theme.shape.borderRadius}px`,
        borderTopRightRadius: (theme) => `${theme.shape.borderRadius}px`,
        zIndex: (theme) => theme.zIndex.drawer - 1,
        // borderTopWidth: '1px',
        // borderTopStyle: 'solid',
        // borderColor: 'grey.100',
      }}
    >
      <Grid
        container
        justifyContent="flex-end"
        alignContent="center"
        sx={{ height: 'inherit', pr: 2 }}
      >
        <Grid item>
          <Link
            color={'text.secondary'}
            variant="caption"
            underline="none"
            target="_blank"
            href={`https://timum.de?utm_medium=link&utm_source=${window.location.hostname.replace(
              /\./g,
              '-',
            )}&utm_campaign=bookingjs2&utm_content=powered-by`}
            sx={{
              pr: 1,

              /* make it harder for people to hide this who aren't allowed to do it */
              fontSize: '10px !important',
              lineHeight: '20px !important',
              position: 'static!important',
              display: 'block!important',
              visibility: 'visible!important',
              opacity: '1.0!important',
            }}
          >
            powered by{' '}
            <Box fontWeight={900} display="inline">
              timum
            </Box>
          </Link>
        </Grid>
      </Grid>
    </Box>
  );
}

Footer.propTypes = {};
