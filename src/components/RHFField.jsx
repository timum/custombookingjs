import React from 'react';
import { Controller } from 'react-hook-form';

export default function RHFField({
  name,
  children,
  namespace,
  rhfProps,
  disableEmptyErrorText,
}) {
  /**
   * If namespace is given this function serves to tell rect to attach the fields here to a certain object
   */
  const withNamespace = (fieldName) => {
    return namespace ? `${namespace}.${fieldName}` : fieldName;
  };

  let elements = React.Children.toArray(children);

  // there can only be one input element as child here
  if (elements.length > 1) {
    throw 'RHFTextfield may only have a single child component';
  }

  const getHelperText = (error) => {
    if (error) {
      return error.message;
    } else if (disableEmptyErrorText) {
      return undefined;
    } else {
      return ' ';
    }
  };

  const controllerArgs = {
    name: withNamespace(name),
    render: ({ field, fieldState }) => {
      const error = fieldState?.error;

      // console.log(
      //   'rendered ' +
      //     withNamespace(field.name) +
      //     ' with error [ ' +
      //     error?.message +
      //     ' ]'
      // );

      field.value = field.value ?? '';

      return children(!!error, getHelperText(error), field);
    },
  };

  // even though 'control' is optional we get an error if we set it to null or undefined explicitly
  // that's why we only add it to the config if given by parent
  if (rhfProps?.control) {
    controllerArgs.control = rhfProps.control;
  }

  return <Controller {...controllerArgs} />;
}
