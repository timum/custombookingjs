import * as React from 'react';
import PropTypes from 'prop-types';

import { selectAppConfigProp } from '../slices/appState';
import { useSelector } from 'react-redux';

import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';

import LoadingButton from '@mui/lab/LoadingButton';

import { useRemoveCustomerFromAppointmentMutation } from '@timum/timum_pdk/src';

import { useSnackbar } from 'notistack';

import BookableDetailsView from './base/BookableDetailsView';
import { CancelPageContext } from './Calendar';
import usePropsBeforeContext from '../hooks/usePropsBeforeContext';

export default function CancelAppointmentView(props) {
  const { t } = useTranslation();
  const [message, setMessage] = React.useState();
  const { enqueueSnackbar } = useSnackbar();

  const {
    timeslot,
    productName,
    resourceName,
    onCancelSuccess,
    onCancelFailed,
  } = usePropsBeforeContext(props, CancelPageContext);

  const authToken = useSelector((state) =>
    selectAppConfigProp(state, 'cancelableAppointment.authToken'),
  );

  const appointmentUuid = useSelector((state) =>
    selectAppConfigProp(state, 'cancelableAppointment.appointmentUuid'),
  );

  const customerUuid = useSelector((state) =>
    selectAppConfigProp(state, 'cancelableAppointment.customerUuid'),
  );

  const cancelationgStarted = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.cancelationgStarted'),
  );
  const cancelationSuccessful = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.cancelationSuccessful'),
  );
  const cancelationFailed = useSelector((state) =>
    selectAppConfigProp(state, 'callbacks.cancelationFailed'),
  );

  const [removeCustomer, { isLoading: isRemovingCustomer }] =
    useRemoveCustomerFromAppointmentMutation();

  const onSubmit = async () => {
    if (cancelationgStarted) {
      cancelationgStarted({
        timeslot,
      });
    }

    const response = await removeCustomer({
      customersId: customerUuid,
      appointmentId: appointmentUuid,
      headers: {
        'X-SIGNED-APP-CUSTOMER': authToken,
      },
      params: {
        message: message,
      },
    });

    if (!response.error) {
      // external callback - user defined
      if (cancelationSuccessful) {
        cancelationSuccessful({
          timeslot: timeslot,
          response: response,
        });
      }

      // internal - defined by parent
      if (onCancelSuccess) {
        onCancelSuccess({
          timeslot: timeslot,
          response: response,
        });
      }

      enqueueSnackbar(t('cancellation.cancelation_successfull_message'), {
        variant: 'success',
      });
    } else {
      // external callback - user defined
      if (cancelationFailed) {
        cancelationFailed({
          timeslot: timeslot,
          response: response,
        });
      }

      // internal - defined by parent
      if (onCancelFailed) {
        onCancelFailed({
          timeslot: timeslot,
          response: response,
        });
      }
    }
  };

  return (
    <Grid
      container
      direction="column"
      alignItems={'center'}
      alignContent={'center'}
      mt={2}
      mx="auto"
      rowSpacing={2}
    >
      <Grid item>
        <Typography variant="h2" textAlign={'center'}>
          {t('cancellation.cancel_appointment_header')}
        </Typography>
      </Grid>

      <Grid item xs>
        <BookableDetailsView
          start={timeslot?.start}
          end={timeslot?.end}
          productName={productName}
          resourceName={resourceName}
          disableReservation
        />
      </Grid>

      <Grid
        item
        borderBottom="1px solid #ddd"
        width={'95%'}
        alignSelf="center"
        mt={2}
        mb={2}
      ></Grid>

      <Grid item>
        <TextField
          fullWidth
          multiline
          size="small"
          value={message}
          onChange={(event) => {
            setMessage(event.target.value);
          }}
          variant="outlined"
          label={t('cancellation.message_label')}
          type="textarea"
          helperText={(() => {
            return `${message?.length ?? '0'}/1024`;
          })()}
          inputProps={{
            maxLength: 1024,
          }}
        />
      </Grid>

      <Grid item>
        <LoadingButton
          variant="contained"
          color="primary"
          onClick={onSubmit}
          loading={isRemovingCustomer}
        >
          {t('cancellation.submit_button_cancel')}
        </LoadingButton>
      </Grid>
    </Grid>
  );
}

CancelAppointmentView.propTypes = {
  start: PropTypes.object,
  end: PropTypes.object,
  product: PropTypes.object,
};
