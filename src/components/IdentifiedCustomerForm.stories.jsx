import React from 'react';
import IdentifiedCustomerForm from './IdentifiedCustomerForm';
import { rest } from 'msw';
import { useDispatch } from 'react-redux';
import { selectProduct } from '../slices/appState';

import { DateTime } from 'luxon';

// import TimumApiMockResponses from '../TimumApiMockResponses';

export default {
  component: IdentifiedCustomerForm,
  title: 'IdentifiedCustomerForm',
};

const Template = (args) => {
  const dispatch = useDispatch();
  dispatch(selectProduct(args.product));
  return <IdentifiedCustomerForm {...args} />;
};

export const Default = {
  render: Template,

  args: {
    timeslot: {
      start: DateTime.now(),
      end: DateTime.now().plus({ hours: 1 }),
      uuid: 'someUuid',
    },
    product: { uuid: 'someOtherUuid' },
    customerData: {
      email: 's***4@***.de',
      name: 'M',
    },
    bookingProcess: 'IMMEDIATE',
  },

  parameters: {
    msw: {
      handlers: {
        createBooking: rest.post(
          '*/rest/1/resources/:uuid/create_appointment_with_consumer',
          (req, res, ctx) => {
            const json = {
              'api-info': {
                version: '1',
              },
              createdAppointment: {
                start: req.json().start,
                end: req.json().end,
                timeslot_uuid: 'a7f943b0-9324-11ed-9e8c-e4a7a0ca32e8',
                product_uuid: req.json().product_uuid,
                contact_channel: {
                  type: 'none',
                  value:
                    'Die genaue Adresse wird am Vorabend des Termins per e-Mail zugesandt',
                },
                product_name: 'Call',
                resource_name: 'Booking Widget DEMO',
                capacity: 1,
                capacity_left: 0,
                kind: 'models.LotAppointment',
              },
            };

            return res(ctx.status(200), ctx.delay(2000), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const BookingFails = {
  render: Template,

  args: {
    ...Default.args,
  },

  parameters: {
    msw: {
      handlers: {
        createBooking: rest.post(
          '*/rest/1/resources/:uuid/create_appointment_with_consumer',
          (req, res, ctx) => {
            return res(ctx.status(400), ctx.delay(500));
          },
        ),
      },
    },
  },
};
