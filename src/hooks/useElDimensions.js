import { useState, useLayoutEffect, useRef } from 'react';

export default (
  propName /* in case el is not a toplevel prop of current */,
  mode = 'inner',
) => {
  const [height, setHeight] = useState(0);
  const [width, setWidth] = useState(0);
  const ref = useRef(null);

  const handleMode = (el) => {
    const isHidden = window.getComputedStyle(el).display === 'none';
    if (isHidden) {
      setHeight(0);
      setWidth(0);
      return;
    }

    if (mode === 'outer') {
      const rect = el.getBoundingClientRect();
      setHeight(rect.height);
      setWidth(rect.width);
    } else if (mode === 'inner') {
      setHeight(el.clientHeight);
      setWidth(el.clientWidth);
    } else {
      throw 'mode prop only accepts "inner" or "outer" as values';
    }
  };

  useLayoutEffect(() => {
    if (ref?.current) {
      if (propName) {
        handleMode(ref.current[propName]);
      } else {
        handleMode(ref.current);
      }
    }
  });

  return [ref, { height: height, width: width }];
};
