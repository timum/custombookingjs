import { DateTime } from 'luxon';
import { useMemo } from 'react';

const supportsSelectedProduct = (bookable, products, selectedProduct) => {
  let supportsProduct = false;

  // test for simple match
  if (selectedProduct && bookable.product_uuid === selectedProduct?.uuid) {
    supportsProduct = true;
  }

  if (products) {
    // is selected product contained in the bookables product list
    if (!supportsProduct) {
      const match = bookable?.products?.find(
        (prd) => prd.uuid === selectedProduct?.uuid,
      );

      if (match) {
        supportsProduct = true;
      }
    }
  }
  return supportsProduct;
};

export default function useBookableCurator({
  bookables,
  products,
  selectedProduct,
}) {
  return useMemo(() => {
    if (bookables) {
      let parsedBookables;
      parsedBookables = Object.entries(bookables);
      parsedBookables = parsedBookables.filter((date) => {
        // marks each bookable whether it has the selected product or not.
        date[1].forEach((bookable) => {
          bookable.supportsProduct = supportsSelectedProduct(
            bookable,
            products,
            selectedProduct,
          );
        });

        date[1] = date[1].filter((bookable) => {
          //-> customers should always see their own appointment
          if (bookable.isCancelable) {
            return true;
          }

          // -> bookables without products must be shown as long as no product is selected
          //    which is only applicable for some products.
          if (!bookable.products || bookable.products.length === 0) {
            return !selectedProduct?.exclusive;
          }

          // -> bookables supporting the selected product must be displayed
          if (bookable.supportsProduct) {
            return true;
          }

          // all others must be hidden
          return false;
        });

        // now remove all dates which have no bookables
        if (!date[1].length) {
          return false;
        } else {
          return true;
        }
      });

      // now sort the timeslots contained in the inner lists of each date
      // according to their start times.
      for (const [, timeslots] of parsedBookables) {
        timeslots.sort((a, b) => {
          if (a.start < b.start) {
            return -1;
          } else if (a.start === b.start) {
            return 0;
          } else {
            return 1;
          }
        });
      }

      for (const [, timeslots] of parsedBookables) {
        for (const timeslot of timeslots) {
          if (timeslot.isCancelable) {
            timeslot.renderAsCancelable = true;
          } else if (
            timeslot.capacity_left > 0 ||
            timeslot.kind === 'models.Bookable' // capacity doesn't matter for pure tsls
          ) {
            timeslot.renderAsBookable = true;
          } else {
            timeslot.renderAsBlocker = true;
          }
        }
      }

      // now sort the dates themselves
      parsedBookables = parsedBookables.sort((a, b) => {
        const dateA = DateTime.fromFormat(a[0], 'yyyy-MM-dd');
        const dateB = DateTime.fromFormat(b[0], 'yyyy-MM-dd');

        if (dateA < dateB) {
          return -1;
        } else if (dateA === dateB) {
          return 0;
        } else {
          return 1;
        }
      });

      return parsedBookables;
    }

    return [];
  }, [bookables, products, selectedProduct]);
}
