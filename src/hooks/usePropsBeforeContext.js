import { useContext } from 'react';

export default function usePropsBeforeContext(props, context) {
  const contextValue = useContext(context);
  const resolvedValues = {};

  if (contextValue === undefined) return props;
  if (props === undefined) return contextValue;

  let keys = [...Object.keys(props), ...Object.keys(contextValue)];

  keys.forEach((key) => {
    // Check if the prop is controlled by looking at isControlledProps flag,
    // if it doesn't exist, we assume it's not controlled.
    const isControlled = Object.prototype.hasOwnProperty.call(props, key)
      ? true
      : false;
    resolvedValues[key] = isControlled ? props[key] : contextValue[key];
  });

  return resolvedValues;
}
