import React from 'react';

export default function useContainerDimensions(selector) {
  const [containerDimensions, setContainerDimensions] = React.useState({
    height: 0,
    width: 0,
  });

  React.useEffect(() => {
    const container = document.querySelector(selector);

    if (!container) return;

    const { height, width } = container.getBoundingClientRect();
    setContainerDimensions({ width, height });

    const observer = new ResizeObserver((entries) => {
      for (const entry of entries) {
        // The entry object contains information about the observed element's size
        const { width, height } = entry.contentRect;

        setContainerDimensions({ width, height });
      }
    });

    observer.observe(container);
    return () => observer.disconnect();
  }, [selector]);

  return containerDimensions;
}
