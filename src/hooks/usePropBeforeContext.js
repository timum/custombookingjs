import { useContext } from 'react';
export default function usePropBeforeContext(
  controlledValue,
  context,
  contextKey,
) {
  const contextValue = useContext(context);

  if (contextKey === undefined) return controlledValue;
  if (contextValue === undefined) return controlledValue;

  return controlledValue !== undefined
    ? controlledValue
    : contextValue[contextKey];
}
