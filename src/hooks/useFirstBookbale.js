import { useMemo } from 'react';

export default (curatedBookables) => {
  return useMemo(() => {
    let unifiedBookables = [];
    if (curatedBookables?.length > 0) {
      for (const [date, timeslots] of curatedBookables) {
        // why is the date part of the unified list -> because PureListView uses the date as a header item in its list.
        // therefore, the date must have an influence on the index, which happens right here.
        unifiedBookables = [...unifiedBookables, date, ...timeslots];
      }
    }

    if (unifiedBookables?.length > 0) {
      for (let i = 0; i < unifiedBookables.length; i++) {
        const timeslot = unifiedBookables[i];
        if (timeslot.renderAsBookable) {
          return { index: i, bookable: timeslot, unifiedBookables };
        }
      }
    }

    return {};
  }, [curatedBookables]);
};
