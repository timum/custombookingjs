'use strict';
import React from 'react';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';

import { pxToRem } from './util/util';

import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';

import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

import ClosableDialogTitle from './components/ClosableDialogTitle';
import { Container, useMediaQuery } from '@mui/material';
import { useTheme } from '@mui/system';

const fontSizeBase = 14;
const fontFamily = '"Ek Mukta","Helvetica Neue", Helvetica, Arial, sans-serif';
const lineHeightBase = 1.428571429;
const lineHeightComputed = Math.floor(fontSizeBase * lineHeightBase);

const fontSizeH1 = pxToRem(Math.floor(fontSizeBase * 2.6));
const fontSizeH2 = pxToRem(Math.floor(fontSizeBase * 2.3));
const fontSizeH3 = pxToRem(Math.floor(fontSizeBase * 1.9));
const fontSizeH4 = pxToRem(Math.floor(fontSizeBase * 1.5));
const fontSizeH5 = pxToRem(Math.floor(fontSizeBase * 1.15));
const fontSizeH6 = pxToRem(Math.floor(fontSizeBase));

export const RE_EMOJI =
  /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/g;

/*
 * Default configuration
 *
 */
export const primary = {
  appConfig: {
    host: 'https://www.timum.de',

    /*
     * Valid values for resource_channel attribute:
     *  RESOURCE_PUBLIC,
     *  RESOURCE_EXCLUSIVE
     *  RESOURCE_REFERENCE
     *  CALENDAR_PUBLIC
     */
    resource_channel: 'RESOURCE_REFERENCE',

    // either fullCalendar or pureListView or detailsListView
    calendarFrontend: 'detailsListView',

    // settings which change the behaviour of a particular view
    calendarFrontendOptions: {
      condensedView: {
        // if true, the resource selection will always be rendered as a dialog
        forceResourceSelectorDialog: true,
      },
    },

    // if set to true and in a multi resource scenario, show an option which loads all bookables of all resources.
    // details are hidden when this option is selected
    allResourcesOption: false,

    // whether the confirmation view is closeable once the booking was successfull.
    allowCloseOnBooking: true,

    // whether 'powered by timum' can be hidden or not. Needs premium plan.
    hideTimumFooter: false,

    // send custom field content as comma sperarated, concatenated string via message field to backend
    sendCustomValuesAsMessage: false,

    gatherAddressData: false,

    height: 500,

    fields: {
      firstName: {
        index: 0,
        title: 'fields.firstName',
        validation: yup.string().required('validation.field_required'), // <- compare with key in localisation
      },
      lastName: {
        index: 1,
        title: 'fields.lastName',
        validation: yup.string().required('validation.field_required'),
      },
      email: {
        index: 2,
        title: 'fields.email',
        format: 'email',
        type: 'text',
        validation: yup
          .string()
          .email('validation.email_field_must_be_valid')
          .required('validation.field_required'),
      },
      mobile: {
        index: 3,
        title: 'fields.mobile',
        type: 'phoneNumber',
        isRequired: false,
        defaultCountry: 'DE',
        preferredCountries: ['DE', 'CH', 'AT'],
        // validation: is in built and ignored
      },
      message: {
        index: 4,
        title: 'fields.message',
        type: 'textarea',
        validation: yup
          .string()
          .max(1024)
          .test(
            'no-emojis',
            'validation.no_emojis',
            (value) => !RE_EMOJI.test(value),
          ),
        limit: 1024,
        onChange: (value) => value.replace(RE_EMOJI, ''),
      },
      agbs: {
        index: 5,
        title: 'fields.accept_timum_privacy',
        type: 'checkbox',
        validation: yup
          .boolean()
          .required('validation.field_required')
          .test(
            'privacyAccepted',
            'validation.privacy_field_required',
            (value) => value === true,
          ),
      },
      locale: {
        index: 6,
        preventRendering: true,
      },
    },
    interfaces: {
      UnkownBookingPage: ({ open, onClose, children }) => {
        const theme = useTheme();
        const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
        return (
          <Dialog
            open={open}
            onClose={onClose}
            hideBackdrop
            maxWidth="md"
            fullWidth
            fullScreen={fullScreen}
          >
            <ClosableDialogTitle onClose={onClose} />
            <DialogContent
              sx={{
                pl: { xs: 2, sm: 3, md: 4 },
                pr: { xs: 2, sm: 3, md: 4 },
              }}
            >
              <Container maxWidth="sm" sx={{ p: 0 }}>
                {children}
              </Container>
            </DialogContent>
          </Dialog>
        );
      },
      IdentifiedBookingPage: ({ open, onClose, children }) => {
        const theme = useTheme();
        const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
        return (
          <Dialog
            open={open}
            onClose={onClose}
            hideBackdrop
            maxWidth="md"
            fullWidth
            fullScreen={fullScreen}
          >
            <ClosableDialogTitle onClose={onClose} />
            <DialogContent
              sx={{
                pl: { xs: 2, sm: 3, md: 4 },
                pr: { xs: 2, sm: 3, md: 4 },
              }}
            >
              <Container maxWidth="sm" sx={{ p: 0 }}>
                {children}
              </Container>
            </DialogContent>
          </Dialog>
        );
      },

      // is used by unknownBookingPage and identifiedBookingPage in reaction to a successfull booking
      ConfirmationPage: (props) => {
        const { open, onClose, allowCloseOnBooking, children } = props;
        const theme = useTheme();
        const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
        return (
          <Dialog
            open={open}
            onClose={() => {
              if (allowCloseOnBooking) {
                onClose();
              }
            }}
            hideBackdrop
            maxWidth="md"
            fullWidth
            fullScreen={fullScreen}
          >
            {allowCloseOnBooking && <ClosableDialogTitle onClose={onClose} />}
            <DialogContent
              sx={{
                pl: { xs: 2, sm: 3, md: 4 },
                pr: { xs: 2, sm: 3, md: 4 },
              }}
            >
              <Container maxWidth="sm" sx={{ p: 0 }}>
                {children}
              </Container>
            </DialogContent>
          </Dialog>
        );
      },

      // is used by unknownBookingPage when typed in email belongs to an active timum user.
      // (Security feature, preventing unknwons to fill our users calenedars with arbitrary appointmens.)
      LoginPage: ({ open, onClose, children }) => {
        return (
          <Dialog open={open} onClose={onClose} hideBackdrop fullWidth>
            <DialogContent>{children}</DialogContent>
          </Dialog>
        );
      },
      CancelPage: ({
        open,
        onClose,
        // timeslot,
        // productName,
        // resourceName,
        // onCancelSuccess,
        children,
      }) => {
        const theme = useTheme();
        const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
        return (
          <Dialog
            open={open}
            onClose={onClose}
            hideBackdrop
            maxWidth="md"
            fullWidth
            fullScreen={fullScreen}
          >
            <ClosableDialogTitle onClose={onClose} />
            <DialogContent
              sx={{
                pl: { xs: 2, sm: 3, md: 4 },
                pr: { xs: 2, sm: 3, md: 4 },
              }}
            >
              <Container maxWidth="sm" sx={{ p: 0 }}>
                {children}
              </Container>
            </DialogContent>
          </Dialog>
        );
      },
      ResourcePage: ({
        mobileOpen,
        mobileOnClose,
        shouldDisplay,
        height,
        children,
      }) => {
        const theme = useTheme();
        const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
        return (
          <>
            <Dialog
              open={mobileOpen}
              onClose={mobileOnClose}
              hideBackdrop
              maxWidth="md"
              fullWidth
              fullScreen={fullScreen}
            >
              <DialogContent
                sx={{
                  pl: { xs: 2, sm: 3, md: 4 },
                  pr: { xs: 2, sm: 3, md: 4 },
                }}
              >
                <Container maxWidth="sm" sx={{ p: 0 }}>
                  {children}
                </Container>
              </DialogContent>
            </Dialog>
            {shouldDisplay && (
              <Grid
                xs="auto"
                item
                container
                display={{ xs: 'none', sm: 'flex' }}
                wrap="nowrap"
                sx={{
                  mr: 2,
                  maxHeight: height,
                  boxSizing: 'border-box',
                }}
              >
                <Grid item xs="auto">
                  {children}
                </Grid>
                <Grid item xs="1">
                  <Divider orientation="vertical" variant="middle" />
                </Grid>
              </Grid>
            )}
          </>
        );
      },

      ProductPage: ({ open, onClose, children }) => {
        const theme = useTheme();
        const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
        return (
          <Dialog
            open={open}
            onClose={onClose}
            hideBackdrop
            maxWidth="md"
            fullWidth
            fullScreen={fullScreen}
          >
            <DialogContent
              sx={{
                pl: { xs: 2, sm: 3, md: 4 },
                pr: { xs: 2, sm: 3, md: 4 },
              }}
            >
              <Container maxWidth="sm" sx={{ p: 0 }}>
                {children}
              </Container>
            </DialogContent>
          </Dialog>
        );
      },
      ErrorDialog: ({ open, onClose }) => {
        const { t } = useTranslation();
        return (
          <Dialog
            open={open}
            onClose={onClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              {t('reservation_failed.title')}
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {t('reservation_failed.message')}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={onClose} autoFocus>
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        );
      },
    },
    callbacks: {},
  },
  muiTheme: {
    palette: {
      primary: {
        main: '#337ab7',
      },
      secondary: { main: '#777' },
      success: {
        main: '#5cb85c',
      },
      warning: { main: '#f0ad4e' },
      error: { main: '#d9534f' },
      info: { main: '#5bc0de' },
    },
    shape: { borderRadius: 4 },
    typography: {
      fontFamily: fontFamily,
      fontSize: fontSizeBase,
      h1: {
        fontFamily: fontFamily,
        fontWeight: 500,
        lineHeight: 1.0,
        fontSize: `${fontSizeH1}rem`,
      },
      h2: {
        fontFamily: fontFamily,
        fontWeight: 500,
        lineHeight: 1.0,
        fontSize: `${fontSizeH2}rem`,
      },
      h3: {
        fontFamily: fontFamily,
        fontWeight: 500,
        lineHeight: 1.0,
        fontSize: `${fontSizeH3}rem`,
      },
      h4: {
        fontFamily: fontFamily,
        fontWeight: 500,
        lineHeight: 1.0,
        fontSize: `${fontSizeH4}rem`,
      },
      h5: {
        fontFamily: fontFamily,
        fontWeight: 500,
        lineHeight: 1.0,
        fontSize: `${fontSizeH5}rem`,
      },
      h6: {
        fontFamily: fontFamily,
        fontWeight: 500,
        lineHeight: 1.0,
        fontSize: `${fontSizeH6}rem`,
      },
    },
    components: {
      MuiCssBaseline: {
        styleOverrides: {
          body: {
            scrollbarColor: '#bfbfbf #ffffff',
            '&::-webkit-scrollbar, & *::-webkit-scrollbar': {
              backgroundColor: '#ffffff',
            },
            '&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb': {
              borderRadius: 8,
              backgroundColor: '#bfbfbf',
              minHeight: 8,
              width: 3,
              border: '5px solid #ffffff',
            },
            '&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus':
              {
                backgroundColor: '#8f8f8f',
              },
            '&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active':
              {
                backgroundColor: '#8f8f8f',
              },
            '&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover':
              {
                backgroundColor: '#8f8f8f',
              },
            '&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner': {
              backgroundColor: '#ffffff',
            },
          },
        },
      },
      MuiButton: {
        styleOverrides: {
          root: {
            borderRadius: 4,
          },
        },
      },
      MuiTextField: {
        defaultProps: {
          variant: 'outlined',
        },
      },
      MuiListItem: {
        styleOverrides: {
          root: {
            borderRadius: 4,
          },
        },
      },
      MuiListItemButton: {
        styleOverrides: {
          root: {
            borderRadius: 4,
          },
        },
      },
      MuiListItemText: {
        styleOverrides: {
          borderRadius: 4,
        },
      },
      MuiTypography: {
        styleOverrides: {
          h1: {
            // marginTop: lineHeightComputed,
            marginBottom: lineHeightComputed / 2,
          },
          h2: {
            // marginTop: lineHeightComputed,
            marginBottom: lineHeightComputed / 2,
          },
          h3: {
            // marginTop: lineHeightComputed,
            marginBottom: lineHeightComputed / 2,
          },
          h4: {
            // marginTop: lineHeightComputed / 2,
            marginBottom: lineHeightComputed / 2,
          },
          h5: {
            // marginTop: lineHeightComputed / 2,
            marginBottom: lineHeightComputed / 2,
          },
          h6: {
            // marginTop: lineHeightComputed / 2,
            marginBottom: lineHeightComputed / 2,
          },
        },
      },
    },
  },
  fcConfig: {
    initialView: 'timeGridWeek',
    useCustomTimumCss: true,
    displayEventEnd: false,
    scrollTimeReset: false,
    scrollTime: '09:00:00',
    views: {
      timeGridWeek: {
        slotMinTime: '08:00:00',
        slotMaxTime: '21:00:00',
        allDaySlot: false,
      },
    },
  },
};

// var customerFieldsNativeFormats = {
//   email: {
//     format: 'email',
//   },
//   comment: {
//     format: 'textarea',
//   },
//   phone: {
//     format: 'tel',
//   },
// };

// Preset: timeDateFormat = '24h-dmy-mon'
// var timeDateFormat24hdmymon = {
//   ui: {
//     booking_date_format: 'D. MMMM YYYY',
//     booking_time_format: 'HH:mm',
//   },
//   fullcalendar: {
//     timeFormat: 'HH:mm',
//     firstDay: 1,
//     views: {
//       basic: {
//         columnFormat: 'dddd D.M.',
//       },
//       agenda: {
//         columnFormat: 'ddd D.M.',
//         slotLabelFormat: 'HH:mm',
//       },
//     },
//   },
// };

// Preset: timeDateFormat = '12h-mdy-sun'
// var timeDateFormat12hmdysun = {
//   ui: {
//     booking_date_format: 'MMMM D, YYYY',
//     booking_time_format: 'h:mma',
//   },
//   fullcalendar: {
//     timeFormat: 'h:mma',
//     firstDay: 0,
//     views: {
//       basic: {
//         columnFormat: 'dddd M/D',
//       },
//       agenda: {
//         columnFormat: 'ddd M/D',
//         slotLabelFormat: 'h:mma',
//       },
//     },
//   },
// };

// Preset: availabilityView = 'agendaWeek'
// var availabilityViewAgendaWeek = {
//   fullcalendar: {
//     header: {
//       left: '',
//       center: '',
//       right: 'today, prev, next',
//     },
//     defaultView: 'agendaWeek',
//   },
// };

// Preset: availabilityView = 'listing'
// var availabilityViewListing = {
//   fullcalendar: {
//     header: {
//       left: '',
//       center: '',
//       right: '',
//     },
//     defaultView: 'listing',
//   },
// };
