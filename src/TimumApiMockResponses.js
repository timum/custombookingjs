import { DateTime, Interval, Duration } from 'luxon';
import { v4 as uuidv4 } from 'uuid';
import { faker } from '@faker-js/faker';

export class TimumApiMockResponses {
  getTimeslotsResponse() {
    let resourceId;
    let providerId;
    let offset = 0;
    let inbetween = 0;
    let inPast;
    let inFuture;
    let withToday = false;
    let tlsPerDay;
    let tlsWithApps = 0;
    let partsPerApp = 0;
    let messagesPerPart = 0;

    const setProviderId = (id) => {
      providerId = id;
      return { setResourceId };
    };

    const setResourceId = (id) => {
      resourceId = id;
      return { including };
    };

    const daysInPast = () => {
      inPast = daysInPast.number;
      return {
        including: daysInPast.inclFunction,
        build,
      };
    };

    const daysBeforeFirstTimeslot = () => {
      offset = daysBeforeFirstTimeslot.number;
      return {
        including: daysBeforeFirstTimeslot.inclFunction,
        build,
      };
    };

    const daysInbetweenTimeslots = () => {
      inbetween = daysInbetweenTimeslots.number;
      return {
        including: daysInbetweenTimeslots.inclFunction,
        build,
      };
    };

    const daysInFuture = () => {
      inFuture = daysInFuture.number;
      return {
        including: daysInFuture.inclFunction,
        build,
      };
    };

    const today = () => {
      withToday = true;
      return {
        including: today.inclFunction,
        build,
      };
    };

    const timeslotsPerDay = () => {
      tlsPerDay = timeslotsPerDay.number;
      return {
        including: timeslotsPerDay.inclFunction,
        build,
      };
    };
    const timeslotsWithAppointmentsPerDay = () => {
      tlsWithApps = timeslotsWithAppointmentsPerDay.number;
      return {
        including: timeslotsWithAppointmentsPerDay.inclFunction,
        build,
      };
    };

    const participationsPerAppointment = () => {
      partsPerApp = participationsPerAppointment.number;
      return {
        including: participationsPerAppointment.inclFunction,
        build,
      };
    };

    const messagesPerParticipation = () => {
      messagesPerPart = messagesPerParticipation.number;
      return {
        including: messagesPerParticipation.inclFunction,
        build,
      };
    };

    const build = () => {
      if (!providerId) {
        console.error(
          'No providerId set. Use setProviderId() before calling build!',
        );
      }

      if (!resourceId) {
        console.error(
          'No resourceId set. Use setResourceId() before calling build!',
        );
      }

      if (!inFuture && !inPast && !today) {
        console.error(
          'Not supposed to generate timeslots for the past, future or present. Why are you calling me again?',
        );
      }

      if (!tlsPerDay) {
        console.error(
          'Cannot generate any timeslots. tlsPerDay is 0 or undefined. Call .including(nr).timeslotsPerDay() before calling build.',
        );
      }

      const buildTimeslot = (start, end) => {
        return {
          state: 'open',
          start: start.toISO(),
          end: end.toISO(),
          raster: '1',
          rasterStart: start.toISO(),
          resource_id: resourceId,
          provider: resourceId,
          uuid: uuidv4(),
          defaultCapacity: 1,
          defaultAcceptBookings: false,
          address: null,
          appointment: {},
          products: [],
        };
      };

      const buildAppointment = (start, end, tlsUuid) => {
        const result = {
          uuid: uuidv4(),
          kind: 'models.LotAppointment',
          product_id: '5b1c8040-5178-11e8-8653-02c187e4c7f5',
          product_name: 'Besichtigung',
          price: {
            value: '0,00',
            currency: 'EUR',
          },
          resource_id: resourceId,
          from: start.toISO(),
          to: end.toISO(),
          obtainability: 'booked',
          state: 'ACTIVE',
          created_by_provider: 'false',
          created_by_id: '35ee53c0-82ac-11ec-b109-e4a7a0ca32e8',
          notes: '',
          provider_uuid: providerId,
          address: {
            id: 24077,
            uuid: uuidv4(),
            addressText: 'Some address text',
          },
          acceptBookings: false,
          capacity: 1,
          capacity_left: 0,
          contact_id: uuidv4(),
          contact_user_name: 'Some contact',
          timeslot_uuid: tlsUuid,
          customers: [],
          references: [],
        };

        for (let j = 0; j < partsPerApp; j++) {
          result.customers.push(buildParticipation());
        }

        return result;
      };

      const buildParticipationMessage = (
        initiator,
        senderName,
        senderUuid,
        senderEmail,
      ) => {
        return {
          uuid: uuidv4(),
          created: 1644221697522,
          message:
            'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. ',
          initiator: initiator,
          sender: {
            uuid: senderUuid,
            fullName: senderName,
            email: senderEmail,
            mobile: '5345364675',
          },
        };
      };

      const buildParticipation = () => {
        const result = {
          customer_user_id: uuidv4(),
          customer_uuid: uuidv4(),
          customer_note: '',
          customer_email: 'some@testMail.de',
          customer_name: 'Max Mustermann',
          customer_mobile: '5345364675',
          participationState: 'BOOKED',
          participation_id: uuidv4,
          participationMessages: [],
        };

        for (let i = 0; i < messagesPerPart; i++) {
          result.participationMessages.push(
            buildParticipationMessage(
              i % 2 === 0 ? 'CUSTOMER' : 'PROVIDER',
              i % 2 === 0 ? 'Max Mustermann' : 'Der Anbieter',
              i % 2 === 0 ? result.customer_uuid : providerId,
              i % 2 === 0 ? result.customer_email : 'providerMail@timum.de',
            ),
          );
        }

        return result;
      };

      const generateTls = (dateTime) => {
        const format = 'yyyy-MM-dd';
        const createdTls = [];
        const dayDuration = Duration.fromMillis(28800000 /* 8h */);
        const tsDuration = Duration.fromMillis(28800000 / tlsPerDay);
        const intervals = Interval.after(dateTime, dayDuration).splitBy(
          tsDuration,
        );

        for (let tsInterval of intervals) {
          const ts = buildTimeslot(tsInterval.start, tsInterval.end);

          if (createdTls.length < tlsWithApps) {
            ts.appointment = buildAppointment(
              tsInterval.start,
              tsInterval.end,
              ts.uuid,
            );
          }

          createdTls.push(ts);
        }

        const lol = {};
        lol[dateTime.toFormat(format)] = createdTls;

        return lol;
      };

      let result = {
        'api-info': {
          version: '1',
        },
        timeslots: {},
      };

      // ordered wildly to allow testing the sorting algorithm

      for (let i = 1 + offset; i <= inFuture; i = i + inbetween + 1) {
        const dateTime = DateTime.now()
          .plus({ days: i })
          .set({ hour: 8, minute: 0, second: 0 });
        const newTimeslots = generateTls(dateTime);
        result.timeslots[Object.keys(newTimeslots)[0]] =
          Object.values(newTimeslots)[0];
      }

      for (let i = 1 + offset; i <= inPast; i = i + inbetween + 1) {
        const dateTime = DateTime.now()
          .minus({ days: i })
          .set({ hour: 8, minute: 0, second: 0 });
        const newTimeslots = generateTls(dateTime);
        result.timeslots[Object.keys(newTimeslots)[0]] =
          Object.values(newTimeslots)[0];
      }

      if (withToday) {
        const dateTime = DateTime.now().set({ hour: 8, minute: 0, second: 0 });
        const newTimeslots = generateTls(dateTime);
        result.timeslots[Object.keys(newTimeslots)[0]] =
          Object.values(newTimeslots)[0];
      }

      return result;
    };

    const including = (number) => {
      daysInFuture.number = number;
      daysInFuture.inclFunction = including;

      daysInPast.number = number;
      daysInPast.inclFunction = including;

      today.inclFunction = including;

      daysInbetweenTimeslots.number = number;
      daysInbetweenTimeslots.inclFunction = including;

      daysBeforeFirstTimeslot.number = number;
      daysBeforeFirstTimeslot.inclFunction = including;

      timeslotsWithAppointmentsPerDay.number = number;
      timeslotsWithAppointmentsPerDay.inclFunction = including;

      timeslotsPerDay.number = number;
      timeslotsPerDay.inclFunction = including;

      participationsPerAppointment.number = number;
      participationsPerAppointment.inclFunction = including;

      messagesPerParticipation.number = number;
      messagesPerParticipation.inclFunction = including;

      return {
        daysInFuture,
        daysInPast,
        today,
        timeslotsWithAppointmentsPerDay,
        daysBeforeFirstTimeslot,
        daysInbetweenTimeslots,
        timeslotsPerDay,
        participationsPerAppointment,
        messagesPerParticipation,
      };
    };

    return {
      setProviderId,
    };
  }

  getProductResponse(number) {
    const products = [];
    for (let i = 0; i < number; i++) {
      const hasDescription = Math.random() < 0.5;
      const hasMinDuration = Math.random() < 0.5;

      window.faker = faker;

      products.push({
        uuid: faker.datatype.uuid(),
        name: faker.commerce.productName(),
        description: hasDescription
          ? faker.commerce.productDescription()
          : undefined,
        minDuration: hasMinDuration
          ? faker.datatype.number({ min: 0, max: 120 })
          : undefined,
        exclusive: Math.random() < 0.5,
      });
    }
    return { products: products };
  }
}

export default new TimumApiMockResponses();
