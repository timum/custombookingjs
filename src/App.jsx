import React, { Suspense } from 'react';
import CustomerIdentityHandler from './components/handlers/CustomerIdentityHandler';
import FrontendHandler from './components/handlers/FrontendHandler';

import ConfigHandler from './components/handlers/ConfigHandler';
import { Provider } from 'react-redux';
import store from './store/store';

import { StyledEngineProvider } from '@mui/material/styles';
import { SnackbarProvider } from 'notistack';

import ProgressComponent from './components/ProgressBar';
import LanguageHandler from './components/handlers/LanguageHandler';

export default function App({ appConfig = {}, muiTheme = {}, fcConfig = {} }) {
  return (
    <>
      <Suspense
        fallback={
          <ProgressComponent
            visible
            circleSx={{ color: '#777' }}
            wrapperSx={{ height: appConfig?.height ? appConfig?.height : 500 }}
          />
        }
      >
        <StyledEngineProvider injectFirst>
          <SnackbarProvider
            maxSnack={3}
            autoHideDuration={3000}
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
          >
            <Provider store={store}>
              <ConfigHandler
                muiTheme={muiTheme}
                fcConfig={fcConfig}
                appConfig={appConfig}
              >
                <LanguageHandler />
                <CustomerIdentityHandler>
                  <FrontendHandler />
                </CustomerIdentityHandler>
              </ConfigHandler>
            </Provider>
          </SnackbarProvider>
        </StyledEngineProvider>
      </Suspense>
    </>
  );
}
