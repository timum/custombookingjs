import { createSlice, createSelector /* , current */ } from '@reduxjs/toolkit';
import { get, set, isString, isObject } from 'lodash';

const initialState = {};
// fields must be writable (several reasons: React/react-hook-form need to add properties/functions)
// also: yup validation functions aren't serializable so we transparently move all that into a separate object outside of the store
let fields = {};
// the same applies to custom interfaces
let interfaces = {};

const tryParseJSON = (value) => {
  try {
    return { value: JSON.parse(value), valid: true };
  } catch (e) {
    return { value: value, valid: false };
  }
};

const isPDataValid = (pData) => {
  return isObject(pData) && pData?.personId && pData?.platform;
};

export const appStateSlice = createSlice({
  name: 'appState',
  initialState,
  reducers: {
    reset: {
      reducer(state) {
        // eslint-disable-next-line no-unused-vars
        state = initialState;
      },
    },
    setAppConfig: {
      reducer(state, action) {
        // normalize pData first
        let pData = action.payload.pData;
        if (pData) {
          if (isString(pData)) {
            pData = tryParseJSON(pData);

            if (pData?.valid) {
              pData = pData.value;
            }
          }

          pData.personId =
            pData.personId || pData.address || pData.partnerId || pData.id;

          if (isPDataValid(pData)) {
            action.payload.pData = pData;
          } else {
            delete action.payload.pData;
            console.warn(
              'malformed pData parameter detected. Tried to parse as JSON but failed. Ignoring param for now.',
            );
          }
        }
        // fields must be writable (several reasons: React/react-hook-form need to add properties/functions)
        // also: yup validation functions aren't serializable so we transparently move all that into a separate object outside of the store
        fields = action.payload.fields;
        delete action.payload.fields;

        // the same is true for interfaces
        interfaces = action.payload.interfaces;
        delete action.payload.interfaces;

        state.appConfig = action.payload;
        // console.log(JSON.stringify(current(state), undefined, 2));
      },
    },
    mutateAppConfig: {
      reducer(state, action) {
        set(
          state.appConfig,
          action.payload.pathToProp,
          action.payload.newValue,
        );
      },
    },
    setFcConfig: {
      reducer(state, action) {
        state.fcConfig = action.payload;
        // console.log(JSON.stringify(current(state), undefined, 2));
      },
    },
    setMuiTheme: {
      reducer(state, action) {
        state.muiTheme = action.payload;
        // console.log(JSON.stringify(current(state), undefined, 2));
      },
    },
    setIdentifiedCustomer: {
      reducer(state, action) {
        state.customerData = action.payload;
      },
    },
    setIsIdentifyingCustomer: {
      reducer(state, action) {
        state.isIdentifyingCustomer = action.payload;
      },
    },
    setCustomerIdentificationFailed: {
      reducer(state, action) {
        state.customerIdentificationFailed = action.payload;
      },
    },
    setIsWaitingForConf: {
      reducer(state, action) {
        state.isWaitingForConf = action.payload;
      },
    },
    selectPublicData: {
      reducer(state, action) {
        state.selectedPublicData = action.payload;
        state.selectedResource = action.payload?.resource;
        // console.log(JSON.stringify(current(state), undefined, 2));
      },
    },
    selectProduct: {
      reducer(state, action) {
        state.selectedProduct = action.payload;
        // console.log(JSON.stringify(current(state), undefined, 2));
      },
    },
    selectResource: {
      reducer(state, action) {
        state.selectedResource = action.payload;
        const associatedPublicData =
          state.resToPublicDataMap[action.payload?.resource?.uuid];
        state.selectedPublicData = associatedPublicData;
        // console.log(JSON.stringify(current(state), undefined, 2));
      },
    },
    setResToPublicDataMap: {
      reducer(state, action) {
        state.resToPublicDataMap = action.payload;
        // console.log(JSON.stringify(current(state), undefined, 2));
      },
    },
  },
});

export const {
  setAppConfig,
  mutateAppConfig,
  setFcConfig,
  setMuiTheme,
  selectProduct,
  selectResource,
  selectPublicData,
  setResToPublicDataMap,
  setIdentifiedCustomer,
  setIsIdentifyingCustomer,
  setCustomerIdentificationFailed,
  setIsWaitingForConf,
  reset,
} = appStateSlice.actions;

export const selectMuiTheme = (state) => {
  return state.appState.muiTheme;
};

export const selectMuiThemeProp = (state, pathToProp) => {
  return get(state.appState.muiTheme, pathToProp);
};

export const selectFcConfig = (state) => {
  return state.appState.fcConfig;
};

export const selectFcConfigProp = (state, pathToProp) => {
  return get(state.appState.fcConfig, pathToProp);
};

const getAppConfig = (state) => state.appState.appConfig;

/**
 * Selects the whole app config.
 *
 * fields must be writable (several reasons: React/react-hook-form need to add properties/functions)
 * also: yup validation functions aren't serializable so we transparently move all that into a separate object outside of the store
 *
 * The same reasons apply to the interfaces. Since they are JSX react needs to have full access.
 * @param {} state
 * @returns
 */
export const selectAppConfig = createSelector([getAppConfig], (appConfig) => {
  return {
    ...appConfig,
    fields: { ...fields },
    interfaces: { ...interfaces },
  };
});

export const selectAppConfigProp = (state, pathToProp) => {
  return get(selectAppConfig(state), pathToProp);
};

export const selectSelectedProduct = (state) => {
  return state.appState.selectedProduct;
};

export const selectSelectedResource = (state) => {
  return state.appState.selectedResource;
};

export const selectCustomerData = (state) => {
  return state.appState.customerData;
};

export const selectIsIdentifyingCustomer = (state) => {
  return state.appState.isIdentifyingCustomer;
};

export const selectCustomerIdentificationFailed = (state) => {
  return state.appState.customerIdentificationFailed;
};

export const selectIsWaitingForConf = (state) => {
  return state.appState.isWaitingForConf;
};

export const selectSelectedPublicData = (state) => {
  return state.appState.selectedPublicData;
};

export const selectIsThemingAllowed = (state) => {
  return state.appState.selectedPublicData?.provider?.isThemingAllowed;
};

export const selectAreCustomFieldsAllowed = (state) => {
  return state.appState.selectedPublicData?.provider?.areCustomFieldsAllowed;
};

export const selectIsLocalisationAllowed = (state) => {
  return state.appState.selectedPublicData?.provider?.isLocalisationAllowed;
};

export const selectPublicDataOfRes = (state, uuid) => {
  return state.appState.resToPublicDataMap[uuid];
};

export const selectResToPublicDataMap = (state) => {
  return state.appState.resToPublicDataMap;
};

export const selectResInPublicData = () =>
  createSelector([selectResToPublicDataMap], (publicDataMap) => {
    return Object.values(publicDataMap).map(
      (publicData) => publicData?.resource,
    );
  });

export default appStateSlice.reducer;
