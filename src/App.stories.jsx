import React from 'react';
import App from './App';
import { rest } from 'msw';
import TimumApiMockResponses from './TimumApiMockResponses';

import * as yup from 'yup';

import Link from '@mui/material/Link';
import { Trans } from 'react-i18next';
import i18next from 'i18next';

// import { rest } from 'msw';
import {
  Default as DefaultProductStory,
  SingleProduct as SingleProductStory,
} from './components/ProductSelector.stories';
import {
  Default as DefaultUnknownCustomerFormStory,
  BookingFailsBcOfDuplicateUser as UnknownCustomerFormBookingFailsStory,
} from './components/UnknownCustomerForm.stories';
import { Default as IdentifiedCustomerFormStories } from './components/IdentifiedCustomerForm.stories';
import {
  Default as CalendarStories,
  WithProfessionalAllowed as CalendarProfessionalStory,
} from './components/Calendar.stories';
import { Default as CancelAppointmentStories } from './components/CancelAppointmentView.stories';
import {
  Default as DefaultLoginViewStory,
  AuthenticationFails as LoginViewAuthFailsStory,
} from './components/LoginView.stories';

// import TimumApiMockResponses from '../TimumApiMockResponses';
import { faker } from '@faker-js/faker';

const customTheme = {
  // This is a mui theme object. Documentation: https://mui.com/material-ui/customization/default-theme/
  // If you are in a professional plan you can customise the look and feel of timum booking to your hearts content.
  // otherwise timum's standard theme is used.

  // Here is an example which alters some aspects of the standard timum theme.
  // (Based on https://github.com/app-generator/react-berry-dashboard)
  palette: {
    common: {
      black: '#000000',
    },
    primary: {
      light: '#b39ddb',
      main: '#5e35b1',
    },
    secondary: {
      light: '#ede7f6',
      main: '#673ab7',
      dark: '#5e35b1',
    },
    error: {
      main: '#f44336',
    },
    warning: {
      main: '#ffe57f',
    },
    success: {
      main: '#00e676',
    },
    grey: {
      50: '#fafafa',
      100: '#f5f5f5',
      500: '#9e9e9e',
      600: '#757575',
      700: '#616161',
      900: '#212121',
    },
    text: {
      primary: '#616161',
      secondary: '#9e9e9e',
      dark: '#212121',
      hint: '#f5f5f5',
    },
  },
  typography: {
    fontFamily: "'Inter', sans-serif",
    fontSize: 16,
    h6: {
      fontWeight: 500,
      color: '#757575',
      fontSize: '0.75rem',
    },
    h5: {
      fontSize: '0.875rem',
      color: '#757575',
      fontWeight: 500,
    },
    h4: {
      fontSize: '1rem',
      color: '#757575',
      fontWeight: 600,
    },
    h3: {
      fontSize: '1.25rem',
      color: '#757575',
      fontWeight: 600,
    },
    h2: {
      fontSize: '1.5rem',
      color: '#757575',
      fontWeight: 700,
    },
    h1: {
      fontSize: '2.125rem',
      color: '#757575',
      fontWeight: 700,
    },
    subtitle1: {
      fontSize: '0.875rem',
      fontWeight: 500,
      color: '#212121',
    },
    subtitle2: {
      fontSize: '0.75rem',
      fontWeight: 400,
      color: '#9e9e9e',
    },
    caption: {
      fontSize: '0.75rem',
      color: '#9e9e9e',
      fontWeight: 400,
    },
    body1: {
      fontSize: '0.875rem',
      fontWeight: 400,
      lineHeight: '1.334',
    },
    body2: {
      fontSize: '0.800rem',
      letterSpacing: '0em',
      fontWeight: 400,
      lineHeight: '1.5',
      color: '#616161',
    },
  },
  shape: {
    borderRadius: 16,
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          fontWeight: 500,
          textTransform: 'capitalize',
          borderRadius: '16px',
        },
      },
    },
    MuiPaper: {
      defaultProps: {
        elevation: 0,
      },
      styleOverrides: {
        root: {
          backgroundImage: 'none',
        },
        rounded: {
          borderRadius: '16px',
        },
      },
    },

    MuiCardContent: {
      styleOverrides: {
        root: {
          padding: '12px',
        },
      },
    },
    MuiCardActions: {
      styleOverrides: {
        root: {
          padding: '12px',
        },
      },
    },
    MuiListItem: {
      styleOverrides: {
        root: {
          borderRadius: '16px',
        },
      },
    },
    MuiListItemButton: {
      styleOverrides: {
        root: {
          color: '#616161',
          paddingTop: '10px',
          paddingBottom: '10px',
          borderRadius: '16px',
          '&:hover': {
            backgroundColor: '#ede7f6',
            color: '#ede7f6',
            '& .MuiListItemIcon-root': {
              color: '#5e35b1',
            },
          },
        },
      },
    },
    MuiListItemIcon: {
      styleOverrides: {
        root: {
          color: '#616161',
          minWidth: '36px',
        },
      },
    },
    MuiListItemText: {
      styleOverrides: {
        primary: {
          color: '#212121',
        },
        borderRadius: '16px',
      },
    },
    MuiInputBase: {
      styleOverrides: {
        input: {
          color: '#212121',
          '&::placeholder': {
            color: '#212121',
            fontSize: '0.875rem',
          },
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          background: '#fafafa',
          borderRadius: '16px',
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: '#bdbdbd',
          },
          '&:hover $notchedOutline': {
            borderColor: '#e3f2fd',
          },
          '&.MuiInputBase-multiline': {
            padding: 1,
          },
        },
        input: {
          fontWeight: 500,
          background: '#fafafa',
          padding: '15.5px 14px',
          borderRadius: '16px',
          '&.MuiInputBase-inputSizeSmall': {
            padding: '10px 14px',
            '&.MuiInputBase-inputAdornedStart': {
              paddingLeft: 0,
            },
          },
        },
        inputAdornedStart: {
          paddingLeft: 4,
        },
        notchedOutline: {
          borderRadius: '16px',
        },
      },
    },
  },
};

export default {
  component: App,
  title: 'App',
};

const Template = (args) => {
  return <App {...args} />;
};

export const Default = {
  render: Template,

  args: {
    appConfig: {
      ref: faker.datatype.uuid(),
      culture: 'en',
      calendarFrontend: 'detailsListView',
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...DefaultUnknownCustomerFormStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
      },
    },
  },
};

export const DefaultWithoutBookables = {
  render: Template,

  args: {
    appConfig: { ref: faker.datatype.uuid(), culture: 'de' },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...DefaultUnknownCustomerFormStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            return res(ctx.status(204), ctx.delay(500), ctx.json(undefined));
          },
        ),
      },
    },
  },
};

export const WithAutoSelectedSingleProduct = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...SingleProductStory.parameters.msw.handlers,
        ...DefaultUnknownCustomerFormStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
      },
    },
  },
};

export const WithoutBookables = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...DefaultUnknownCustomerFormStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            return res(ctx.status(204), ctx.delay(500), ctx.json(undefined));
          },
        ),
      },
    },
  },
};

export const WithPData = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
      pData: {
        platform: 'timum',
        personId: '12345',
      },
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        identify: rest.get(
          '*/rest/1/crms/timum/resources/:uuid/customers/12345/identify',
          (req, res, ctx) => {
            const json = {
              email: 's***4@***.de',
              name: 'M',
            };

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const CustomFields = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
      sendCustomValuesInMessage: true,
      fields: {
        firstName: {
          index: 0,
          title: 'fields.firstName',
          validation: yup.string().required('validation.field_required'), // <- compare with key in localisation
        },
        lastName: {
          index: 1,
          title: 'fields.lastName',
          validation: yup.string().required('validation.field_required'),
        },
        email: {
          index: 2,
          title: 'fields.email',
          format: 'email',
          type: 'text',
          validation: yup
            .string()
            .email('validation.email_field_must_be_valid')
            .required('validation.field_required'),
        },
        gender: {
          // defines the position of this custom field
          index: 3,
          // new language key
          title: 'fields.gender.title',
          // best for a limited number of predefined choices
          type: 'select',
          // could use 'validation.field_required' but
          // for this example let's create a new  key
          validation: yup.string().required('validation.gender_field_required'),
          options: [
            { key: 'm', title: 'fields.gender.male' },
            { key: 'w', title: 'fields.gender.female' },
            { key: 'd', title: 'fields.gender.other' },
          ],
        },
        message: {
          index: 4,
          title: 'fields.message',
          type: 'textarea',
          validation: yup.string().max(1024),
          limit: 1024,
        },
        agbs: {
          index: 5,
          title: (
            <div>
              <Trans t={i18next.t} i18nKey="fields.accept_timum_privacy">
                <Link href="https://info.timum.de/datenschutz" target="_blank">
                  Datenschutzbestimmungen
                </Link>{' '}
                gelesen und akzeptiert.
              </Trans>
            </div>
          ),
          type: 'checkbox',
          validation: yup
            .boolean()
            .required('validation.field_required')
            .test(
              'privacyAccepted',
              'validation.privacy_field_required',
              (value) => value === true,
            ),
        },

        // set these fields to undefined explicitly in order to remove them
        mobile: undefined,
      },
      localization: {
        de: {
          validation: {
            gender_field_required: 'Bitte wählen.',
          },
          fields: {
            gender: {
              title: 'Geschlecht',
              male: 'Männlich',
              female: 'Weiblich',
              other: 'Divers',
            },
          },
        },
      },
      pData: {
        platform: 'timum',
        personId: '12345',
      },
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [
              {
                ref: faker.datatype.uuid(),
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: faker.datatype.uuid(),
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: '',
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: true,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              },
            ];

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithPDataAndCustomFields = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
      sendCustomValuesInMessage: true,
      fields: {
        firstName: {
          index: 0,
          title: 'fields.firstName',
          validation: yup.string().required('validation.field_required'), // <- compare with key in localisation
        },
        lastName: {
          index: 1,
          title: 'fields.lastName',
          validation: yup.string().required('validation.field_required'),
        },
        email: {
          index: 2,
          title: 'fields.email',
          format: 'email',
          type: 'text',
          validation: yup
            .string()
            .email('validation.email_field_must_be_valid')
            .required('validation.field_required'),
        },
        gender: {
          // defines the position of this custom field
          index: 3,
          // new language key
          title: 'fields.gender.title',
          // best for a limited number of predefined choices
          type: 'select',
          // could use 'validation.field_required' but
          // for this example let's create a new  key
          validation: yup.string().required('validation.gender_field_required'),
          options: [
            { key: 'm', title: 'fields.gender.male' },
            { key: 'w', title: 'fields.gender.female' },
            { key: 'd', title: 'fields.gender.other' },
          ],
        },
        message: {
          index: 4,
          title: 'fields.message',
          type: 'textarea',
          validation: yup.string().max(1024),
          limit: 1024,
        },
        agbs: {
          index: 5,
          title: (
            <div>
              <Trans t={i18next.t} i18nKey="fields.accept_timum_privacy">
                <Link href="https://info.timum.de/datenschutz" target="_blank">
                  Datenschutzbestimmungen
                </Link>{' '}
                gelesen und akzeptiert.
              </Trans>
            </div>
          ),
          type: 'checkbox',
          validation: yup
            .boolean()
            .required('validation.field_required')
            .test(
              'privacyAccepted',
              'validation.privacy_field_required',
              (value) => value === true,
            ),
        },

        // set these fields to undefined explicitly in order to remove them
        mobile: undefined,
      },
      localization: {
        de: {
          validation: {
            gender_field_required: 'Bitte wählen.',
          },
          fields: {
            gender: {
              title: 'Geschlecht',
              male: 'Männlich',
              female: 'Weiblich',
              other: 'Divers',
            },
          },
        },
      },
      pData: {
        platform: 'timum',
        personId: '12345',
      },
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        identify: rest.get(
          '*/rest/1/crms/timum/resources/:uuid/customers/12345/identify',
          (req, res, ctx) => {
            const json = {
              email: 's***4@***.de',
              name: 'M',
            };

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [
              {
                ref: faker.datatype.uuid(),
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: faker.datatype.uuid(),
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: '',
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: true,
                  isLocalisationAllowed: true,
                  areCustomFieldsAllowed: true,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              },
            ];

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithPDataAndCustomFieldsSomeBlockedForIdentifiedCutomers = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
      sendCustomValuesInMessage: true,
      fields: {
        firstName: {
          index: 0,
          title: 'fields.firstName',
          validation: yup.string().required('validation.field_required'), // <- compare with key in localisation
        },
        lastName: {
          index: 1,
          title: 'fields.lastName',
          validation: yup.string().required('validation.field_required'),
        },
        email: {
          index: 2,
          title: 'fields.email',
          format: 'email',
          type: 'text',
          validation: yup
            .string()
            .email('validation.email_field_must_be_valid')
            .required('validation.field_required'),
        },
        gender: {
          // defines the position of this custom field
          index: 3,
          // new language key
          title: 'fields.gender.title',
          // best for a limited number of predefined choices
          type: 'select',
          // could use 'validation.field_required' but
          // for this example let's create a new  key
          validation: yup.string().required('validation.gender_field_required'),
          options: [
            { key: 'm', title: 'fields.gender.male' },
            { key: 'w', title: 'fields.gender.female' },
            { key: 'd', title: 'fields.gender.other' },
          ],
          preventRenderingFor: ['identifiedCustomers'],
        },
        message: {
          index: 4,
          title: 'fields.message',
          type: 'textarea',
          validation: yup.string().max(1024),
          limit: 1024,
          preventRenderingFor: ['identifiedCustomers'],
        },
        agbs: {
          index: 5,
          title: (
            <div>
              <Trans t={i18next.t} i18nKey="fields.accept_timum_privacy">
                <Link href="https://info.timum.de/datenschutz" target="_blank">
                  Datenschutzbestimmungen
                </Link>{' '}
                gelesen und akzeptiert.
              </Trans>
            </div>
          ),
          type: 'checkbox',
          validation: yup
            .boolean()
            .required('validation.field_required')
            .test(
              'privacyAccepted',
              'validation.privacy_field_required',
              (value) => value === true,
            ),
        },

        // set these fields to undefined explicitly in order to remove them
        mobile: undefined,
      },
      localization: {
        de: {
          validation: {
            gender_field_required: 'Bitte wählen.',
          },
          fields: {
            gender: {
              title: 'Geschlecht',
              male: 'Männlich',
              female: 'Weiblich',
              other: 'Divers',
            },
          },
        },
      },
      pData: {
        platform: 'timum',
        personId: '12345',
      },
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        identify: rest.get(
          '*/rest/1/crms/timum/resources/:uuid/customers/12345/identify',
          (req, res, ctx) => {
            const json = {
              email: 's***4@***.de',
              name: 'M',
            };

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [
              {
                ref: faker.datatype.uuid(),
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: faker.datatype.uuid(),
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: '',
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: true,
                  isLocalisationAllowed: true,
                  areCustomFieldsAllowed: true,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              },
            ];

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithPDataAndCustomFieldsSomeBlockedForUnknownCutomers = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
      sendCustomValuesInMessage: true,
      fields: {
        firstName: {
          index: 0,
          title: 'fields.firstName',
          validation: yup.string().required('validation.field_required'), // <- compare with key in localisation
        },
        lastName: {
          index: 1,
          title: 'fields.lastName',
          validation: yup.string().required('validation.field_required'),
        },
        email: {
          index: 2,
          title: 'fields.email',
          format: 'email',
          type: 'text',
          validation: yup
            .string()
            .email('validation.email_field_must_be_valid')
            .required('validation.field_required'),
        },
        gender: {
          // defines the position of this custom field
          index: 3,
          // new language key
          title: 'fields.gender.title',
          // best for a limited number of predefined choices
          type: 'select',
          // could use 'validation.field_required' but
          // for this example let's create a new  key
          validation: yup.string().required('validation.gender_field_required'),
          options: [
            { key: 'm', title: 'fields.gender.male' },
            { key: 'w', title: 'fields.gender.female' },
            { key: 'd', title: 'fields.gender.other' },
          ],
          preventRenderingFor: ['unknownCustomers'],
        },
        message: {
          index: 4,
          title: 'fields.message',
          type: 'textarea',
          validation: yup.string().max(1024),
          limit: 1024,
        },
        agbs: {
          index: 5,
          title: (
            <div>
              <Trans t={i18next.t} i18nKey="fields.accept_timum_privacy">
                <Link href="https://info.timum.de/datenschutz" target="_blank">
                  Datenschutzbestimmungen
                </Link>{' '}
                gelesen und akzeptiert.
              </Trans>
            </div>
          ),
          type: 'checkbox',
          validation: yup
            .boolean()
            .required('validation.field_required')
            .test(
              'privacyAccepted',
              'validation.privacy_field_required',
              (value) => value === true,
            ),
        },

        // set these fields to undefined explicitly in order to remove them
        mobile: undefined,
      },
      localization: {
        de: {
          validation: {
            gender_field_required: 'Bitte wählen.',
          },
          fields: {
            gender: {
              title: 'Geschlecht',
              male: 'Männlich',
              female: 'Weiblich',
              other: 'Divers',
            },
          },
        },
      },
      pData: {
        platform: 'timum',
        personId: '12345',
      },
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [
              {
                ref: faker.datatype.uuid(),
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: faker.datatype.uuid(),
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: '',
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: true,
                  isLocalisationAllowed: true,
                  areCustomFieldsAllowed: true,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              },
            ];

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const DocuExampleTest = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
      fields: {
        firstName: {
          index: 0,
          title: 'fields.firstName',
          validation: yup.string().required('validation.field_required'), // <- compare with key in localisation
        },
        lastName: {
          index: 1,
          title: 'fields.lastName',
          validation: yup.string().required('validation.field_required'),
        },
        email: {
          index: 2,
          title: 'fields.email',
          format: 'email',
          type: 'text',
          validation: yup
            .string()
            .email('validation.email_field_must_be_valid')
            .required('validation.field_required'),
        },
        gender: {
          // defines the position of this custom field
          index: 3,
          // new language key
          title: 'fields.gender.title',
          // best for a limited number of predefined choices
          type: 'select',
          // could use 'validation.field_required' but
          // for this example let's create a new  key
          validation: yup.string().required('validation.gender_field_required'),
          options: [
            { key: 'm', title: 'fields.gender.male' },
            { key: 'w', title: 'fields.gender.female' },
            { key: 'd', title: 'fields.gender.other' },
          ],
        },
        agbs: {
          index: 4,
          title: (
            <div>
              <Trans t={i18next.t} i18nKey="fields.accept_timum_privacy">
                <Link href="https://info.timum.de/datenschutz" target="_blank">
                  Datenschutzbestimmungen
                </Link>{' '}
                gelesen und akzeptiert.
              </Trans>
            </div>
          ),
          type: 'checkbox',
          validation: yup
            .boolean()
            .required('validation.field_required')
            .test(
              'privacyAccepted',
              'validation.privacy_field_required',
              (value) => value === true,
            ),
        },

        // set these fields to undefined explicitly in order to remove them
        message: undefined,
        mobile: undefined,
      },

      // now we need to add the new translation keys and their translations.

      localization: {
        de: {
          validation: {
            gender_field_required: 'Bitte wählen.',
          },
          fields: {
            gender: {
              title: 'Geschlecht',
              male: 'Männlich',
              female: 'Weiblich',
              other: 'Divers',
            },
          },
        },
        en: {
          validation: {
            gender_field_required: 'Please select an option.',
          },
          fields: {
            gender: {
              title: 'Gender',
              male: 'Male',
              female: 'Female',
              other: 'Others',
            },
          },
        },
      },
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
      },
    },
  },
};

export const Loading = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
    },
  },

  parameters: {
    msw: {
      handlers: {
        identify: rest.get(
          '*/rest/1/crms/:uuid/resources/:uuid2/customers/:uuid3/identify*',
          (req, res, ctx) => {
            return res(ctx.status(200), ctx.delay(500));
          },
        ),
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            return res(ctx.status(200), ctx.delay(500));
          },
        ),
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [
              {
                ref: faker.datatype.uuid(),
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: faker.datatype.uuid(),
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: '',
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: false,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              },
            ];

            return res(ctx.status(200), ctx.delay(99999), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const AutoScroll = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
    },
  },

  parameters: {
    msw: {
      handlers: {
        getActiveProducts: rest.get(
          '*/rest/1/resources/:uuid/active_products*',
          (req, res, ctx) => {
            const json = {
              products: [
                {
                  uuid: faker.datatype.uuid(),
                  name: 'Besichtigung',
                  description: undefined,
                  minDuration: undefined,
                  exclusive: false,
                },
              ],
            };

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            const json = TimumApiMockResponses.getTimeslotsResponse()
              .setProviderId(faker.datatype.uuid())
              .setResourceId(faker.datatype.uuid())
              .including(8)
              .daysInFuture()
              .including(3)
              .timeslotsPerDay()
              .including(3)
              .timeslotsWithAppointmentsPerDay()
              .build();

            for (let i = 0; i < Object.entries(json.timeslots).length; i++) {
              const [, timeslots] = Object.entries(json.timeslots)[i];
              for (const tsl of timeslots) {
                if (i < 7) {
                  tsl.capacity_left = 0;
                } else {
                  tsl.capacity_left = 1;
                }
                tsl.kind =
                  faker.random.numeric(1) % 3
                    ? 'model.Bookable'
                    : 'model.LotAppointment';

                tsl.timeslot_uuid = faker.datatype.uuid();
                tsl.product_name = 'Besichtigung';
              }
            }

            json.timeslots.public_visible = true;

            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(json.timeslots),
            );
          },
        ),
      },
    },
  },
};

export const AutoScrollMultiProduct = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
    },
  },

  parameters: {
    msw: {
      handlers: {
        getActiveProducts: rest.get(
          '*/rest/1/resources/:uuid/active_products*',
          (req, res, ctx) => {
            const json = {
              products: [
                {
                  uuid: 'd370f3f2-e262-40f6-aae3-ba8ba9c3cd54',
                  name: 'Besichtigung',
                  description: undefined,
                  minDuration: undefined,
                  exclusive: true,
                },
                {
                  uuid: '1a2f5dd3-e9ea-498d-864e-c5acb81621de',
                  name: 'Beratung',
                  description: undefined,
                  minDuration: undefined,
                  exclusive: true,
                },
              ],
            };

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            const json = TimumApiMockResponses.getTimeslotsResponse()
              .setProviderId(faker.datatype.uuid())
              .setResourceId(faker.datatype.uuid())
              .including(20)
              .daysInFuture()
              .including(3)
              .timeslotsPerDay()
              .including(3)
              .timeslotsWithAppointmentsPerDay()
              .build();

            for (let i = 0; i < Object.entries(json.timeslots).length; i++) {
              const [, timeslots] = Object.entries(json.timeslots)[i];
              for (const tsl of timeslots) {
                if (i < 7 || (i > 10 && i < 17)) {
                  tsl.capacity_left = 0;
                } else {
                  tsl.capacity_left = 1;
                }
                tsl.kind = 'model.Bookable';

                tsl.timeslot_uuid = faker.datatype.uuid();

                if (i < 10) {
                  tsl.product_uuid = 'd370f3f2-e262-40f6-aae3-ba8ba9c3cd54';
                  tsl.products = ['d370f3f2-e262-40f6-aae3-ba8ba9c3cd54'];
                }
                if (i > 10) {
                  tsl.product_uuid = '1a2f5dd3-e9ea-498d-864e-c5acb81621de';
                  tsl.products = ['1a2f5dd3-e9ea-498d-864e-c5acb81621de'];
                }
              }
            }

            json.timeslots.public_visible = true;

            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(json.timeslots),
            );
          },
        ),
      },
    },
  },
};

export const WithCancelableAppointment = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
      cancelableAppointment: {
        authToken: faker.internet.mac(),
        appointmentUuid: faker.datatype.uuid(),
        customerUuid: faker.datatype.uuid(),
      },
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            const json = TimumApiMockResponses.getTimeslotsResponse()
              .setProviderId(faker.datatype.uuid())
              .setResourceId(faker.datatype.uuid())
              .including(5)
              .daysInPast()
              .including(5)
              .daysInFuture()
              .including()
              .today()
              .including(6)
              .timeslotsPerDay()
              .including(3)
              .timeslotsWithAppointmentsPerDay()
              .build();

            let cancelableIdAssigned = false;
            for (const [, timeslots] of Object.entries(json.timeslots)) {
              for (const tsl of timeslots) {
                tsl.capacity_left = faker.random.numeric(1) % 3;
                tsl.kind =
                  faker.random.numeric(1) % 3
                    ? 'model.Bookable'
                    : 'model.LotAppointment';

                if (!cancelableIdAssigned) {
                  tsl.appointment_uuid =
                    WithCancelableAppointment.args.appConfig.cancelableAppointment.appointmentUuid;
                  cancelableIdAssigned = true;
                }
                tsl.timeslot_uuid = faker.datatype.uuid();

                // const number = getRandomInt(
                //   0,
                //   ProductSelectorDefault.args.currentProducts.length - 1
                // );
                // tsl.product_name =
                //   ProductSelectorDefault.args.currentProducts[number].name;

                tsl.product_name = 'Besichtigung';
              }
            }

            json.timeslots.public_visible = true;

            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(json.timeslots),
            );
          },
        ),
      },
    },
  },
};

export const WithCustomThemeButForbidden = {
  render: Template,

  args: {
    appConfig: {
      ...Default.args.appConfig,
    },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
      },
    },
  },
};

export const WithAllowedCustomTheme = {
  render: Template,

  args: {
    appConfig: { ref: faker.datatype.uuid() },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarProfessionalStory.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
      },
    },
  },
};

export const WithCustomThemeAndLoginPrompt = {
  render: Template,

  args: {
    appConfig: { ref: faker.datatype.uuid() },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarProfessionalStory.parameters.msw.handlers,
        ...UnknownCustomerFormBookingFailsStory.parameters.msw.handlers,
        ...DefaultLoginViewStory.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            const json = TimumApiMockResponses.getTimeslotsResponse()
              .setProviderId(faker.datatype.uuid())
              .setResourceId(faker.datatype.uuid())
              .including(5)
              .daysInPast()
              .including(5)
              .daysInFuture()
              .including()
              .today()
              .including(6)
              .timeslotsPerDay()
              .including(3)
              .timeslotsWithAppointmentsPerDay()
              .build();

            let cancelableIdAssigned = false;
            for (const [, timeslots] of Object.entries(json.timeslots)) {
              for (const tsl of timeslots) {
                tsl.capacity_left = faker.random.numeric(1) % 3;
                tsl.kind =
                  faker.random.numeric(1) % 3
                    ? 'model.Bookable'
                    : 'model.LotAppointment';

                if (!cancelableIdAssigned) {
                  tsl.appointment_uuid =
                    WithCancelableAppointment.args.appConfig.cancelableAppointment.appointmentUuid;
                  cancelableIdAssigned = true;
                }
                tsl.timeslot_uuid = faker.datatype.uuid();
                tsl.product_name = 'Besichtigung';
              }
            }

            json.timeslots.public_visible = true;

            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(json.timeslots),
            );
          },
        ),
      },
    },
  },
};

export const WithCustomThemeAndFailingLoginPrompt = {
  render: Template,

  args: {
    appConfig: { ref: faker.datatype.uuid() },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarProfessionalStory.parameters.msw.handlers,
        ...UnknownCustomerFormBookingFailsStory.parameters.msw.handlers,
        ...LoginViewAuthFailsStory.parameters.msw.handlers,
      },
    },
  },
};

const personId = faker.datatype.uuid();

export const WithCustomThemePdataAndCancellableAppointment = {
  render: Template,

  args: {
    appConfig: {
      ref: faker.datatype.uuid(),
      pData: {
        platform: 'timum',
        personId: personId,
      },
      cancelableAppointment: {
        authToken: faker.internet.mac(),
        appointmentUuid: faker.datatype.uuid(),
        customerUuid: personId,
      },
    },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        ...CalendarProfessionalStory.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        identify: rest.get(
          '*/rest/1/crms/timum/resources/:uuid1/customers/:uuid2/identify',
          (req, res, ctx) => {
            const json = {
              email: 's***4@***.de',
              name: 'M',
            };

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            const json = TimumApiMockResponses.getTimeslotsResponse()
              .setProviderId(faker.datatype.uuid())
              .setResourceId(faker.datatype.uuid())
              .including(5)
              .daysInPast()
              .including(5)
              .daysInFuture()
              .including()
              .today()
              .including(6)
              .timeslotsPerDay()
              .including(3)
              .timeslotsWithAppointmentsPerDay()
              .build();

            let cancelableIdAssigned = false;
            for (const [, timeslots] of Object.entries(json.timeslots)) {
              for (const tsl of timeslots) {
                tsl.capacity_left = faker.random.numeric(1) % 3;
                tsl.kind =
                  faker.random.numeric(1) % 3
                    ? 'model.Bookable'
                    : 'model.LotAppointment';

                if (!cancelableIdAssigned) {
                  tsl.appointment_uuid =
                    WithCustomThemePdataAndCancellableAppointment.args.appConfig.cancelableAppointment.appointmentUuid;
                  cancelableIdAssigned = true;
                }
                tsl.timeslot_uuid = faker.datatype.uuid();

                // const number = getRandomInt(
                //   0,
                //   ProductSelectorDefault.args.currentProducts.length - 1
                // );
                // tsl.product_name =
                //   ProductSelectorDefault.args.currentProducts[number].name;

                tsl.product_name = 'Besichtigung';
              }
            }

            json.timeslots.public_visible = true;

            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(json.timeslots),
            );
          },
        ),
      },
    },
  },
};

export const WithOnlyAFewBookables = {
  render: Template,

  args: {
    appConfig: {
      ref: faker.datatype.uuid(),
      calendarFrontend: 'condensedView',
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...DefaultUnknownCustomerFormStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            const json = TimumApiMockResponses.getTimeslotsResponse()
              .setProviderId(faker.datatype.uuid())
              .setResourceId(faker.datatype.uuid())
              .including(0)
              .daysInPast()
              .including(1)
              .daysInFuture()
              .including()
              .today()
              .including(2)
              .timeslotsPerDay()
              .including(0)
              .timeslotsWithAppointmentsPerDay()
              .build();

            let cancelableIdAssigned = false;
            for (const [, timeslots] of Object.entries(json.timeslots)) {
              for (const tsl of timeslots) {
                tsl.capacity_left = faker.random.numeric(1) % 3;
                tsl.kind =
                  faker.random.numeric(1) % 3
                    ? 'model.Bookable'
                    : 'model.LotAppointment';

                if (!cancelableIdAssigned) {
                  tsl.appointment_uuid =
                    WithCustomThemePdataAndCancellableAppointment.args.appConfig.cancelableAppointment.appointmentUuid;
                  cancelableIdAssigned = true;
                }
                tsl.timeslot_uuid = faker.datatype.uuid();

                // const number = getRandomInt(
                //   0,
                //   ProductSelectorDefault.args.currentProducts.length - 1
                // );
                // tsl.product_name =
                //   ProductSelectorDefault.args.currentProducts[number].name;

                tsl.product_name = 'Besichtigung';
              }
            }

            json.timeslots.public_visible = true;

            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(json.timeslots),
            );
          },
        ),
      },
    },
  },
};

export const WithCustomThemeAndPDataButForbidden = {
  render: Template,

  args: {
    appConfig: {
      ref: faker.datatype.uuid(),
      pData: {
        platform: 'timum',
        personId: '12345',
      },
    },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        identify: rest.get(
          '*/rest/1/crms/timum/resources/:uuid/customers/12345/identify',
          (req, res, ctx) => {
            const json = {
              email: 's***4@***.de',
              name: 'M',
            };

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithoutResourceDescription = {
  render: Template,

  args: {
    appConfig: { ref: faker.datatype.uuid() },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarProfessionalStory.parameters.msw.handlers,
        ...UnknownCustomerFormBookingFailsStory.parameters.msw.handlers,
        ...DefaultLoginViewStory.parameters.msw.handlers,
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [
              {
                ref: faker.datatype.uuid(),
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: faker.datatype.uuid(),
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: '',
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: false,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              },
            ];

            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithNeitherResourceDescriptionNorContactInfo = {
  render: Template,

  args: {
    appConfig: { ref: faker.datatype.uuid() },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarProfessionalStory.parameters.msw.handlers,
        ...UnknownCustomerFormBookingFailsStory.parameters.msw.handlers,
        ...DefaultLoginViewStory.parameters.msw.handlers,
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [
              {
                ref: faker.datatype.uuid(),
                contact: {
                  name: '',
                  email: '',
                  mobile: '',
                  phone: '',
                },
                resource: {
                  uuid: faker.datatype.uuid(),
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: '',
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: false,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              },
            ];

            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithLongResourceDescription = {
  render: Template,

  args: {
    appConfig: { ref: faker.datatype.uuid() },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarProfessionalStory.parameters.msw.handlers,
        ...UnknownCustomerFormBookingFailsStory.parameters.msw.handlers,
        ...DefaultLoginViewStory.parameters.msw.handlers,
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = {
              ref: faker.datatype.uuid(),
              contact: {
                name: 'Peter Lustig',
                email: 'lustig@villa_regenbogen.de',
                mobile: '01777483222',
                phone: '',
              },
              resource: {
                uuid: faker.datatype.uuid(),
                name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                description: `# Majestic Manor at Willow Creek

Nestled within the serene folds of **Willow Creek**, this stately manor stands as a timeless beacon of elegance and comfort. As you approach the estate, a grandiose **archway of fragrant wisteria** greets you, leading to the expansive stone-paved driveway. The exterior of the mansion boasts a harmonious blend of classical architecture and modern touches, characterized by brick masonry accented with colonial-style windows and a sweeping, **slate-tiled roof**.

## Interior Grandeur

Upon entry, the grand foyer unveils a sprawling open-plan area illuminated by natural light cascading from a **magnificent crystal chandelier**. The polished oak floors guide you through the living quarters, where every detail—from the intricate crown moldings to the plush furnishings—exudes an aura of refined luxury. The **two-story library** holds an extensive collection of works, offering a perfect sanctuary for avid readers.

## Kitchen & Dining

The gourmet kitchen is a chef's dream, featuring state-of-the-art appliances, a sizeable marble island, and custom cabinetry. Adjacent to it is the formal dining room, where panoramic windows reveal breathtaking views of the manicured gardens, further enhancing the dining experience.

## Outdoor Oasis

The sprawling grounds encompass beautifully landscaped gardens, a tranquil koi pond, and an **orchard of ancient olive trees**. The centerpiece of the outdoor refuge is the azure infinity pool, accompanied by a shaded patio—ideal for alfresco dining.

In sum, the Willow Creek Manor offers a symphony of elegance and tranquility, perfect for those seeking a luxurious retreat in nature's lap.`,
                msgHelpText: '',
                url: faker.internet.url(),
              },
              provider: {
                name: 'Lila Lustig GmbH',
                description: 'Some description of this provider',
                isThemingAllowed: false,
                isLocalisationAllowed: false,
                areCustomFieldsAllowed: false,
              },
              channel: {
                bookingProcess: 'IMMEDIATE',
              },
            };

            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};

const resourceRefs = [
  faker.datatype.uuid(),
  faker.datatype.uuid(),
  faker.datatype.uuid(),
  faker.datatype.uuid(),
  faker.datatype.uuid(),
  faker.datatype.uuid(),
  faker.datatype.uuid(),
];

export const WithCancelableAppointmentInMultiResource = {
  render: Template,

  args: {
    appConfig: {
      ref: resourceRefs,
      cancelableAppointment: {
        authToken: faker.internet.mac(),
        appointmentUuid: faker.datatype.uuid(),
        customerUuid: faker.datatype.uuid(),
        resId: resourceRefs[3],
      },
      calendarFrontend: 'pureListView',
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        getUpcomingBookables: rest.post(
          '*/rest/1/resources/:uuid/upcoming_bookables*',
          (req, res, ctx) => {
            const json = TimumApiMockResponses.getTimeslotsResponse()
              .setProviderId(faker.datatype.uuid())
              .setResourceId(req.params.uuid)
              .including(5)
              .daysInPast()
              .including(5)
              .daysInFuture()
              .including()
              .today()
              .including(6)
              .timeslotsPerDay()
              .including(3)
              .timeslotsWithAppointmentsPerDay()
              .build();

            let cancelableIdAssigned = false;
            for (const [, timeslots] of Object.entries(json.timeslots)) {
              for (const tsl of timeslots) {
                tsl.capacity_left = faker.random.numeric(1) % 3;
                tsl.kind =
                  faker.random.numeric(1) % 3
                    ? 'model.Bookable'
                    : 'model.LotAppointment';

                if (!cancelableIdAssigned) {
                  tsl.appointment_uuid =
                    WithCancelableAppointmentInMultiResource.args.appConfig.cancelableAppointment.appointmentUuid;
                  cancelableIdAssigned = true;
                }
                tsl.timeslot_uuid = faker.datatype.uuid();

                // const number = getRandomInt(
                //   0,
                //   ProductSelectorDefault.args.currentProducts.length - 1
                // );
                // tsl.product_name =
                //   ProductSelectorDefault.args.currentProducts[number].name;

                tsl.product_name = 'Besichtigung';
              }
            }

            json.timeslots.public_visible = true;

            return res(
              ctx.status(200),
              ctx.delay(500),
              ctx.json(json.timeslots),
            );
          },
        ),
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [];
            for (const ref of resourceRefs) {
              json.push({
                ref: ref,
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: ref,
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: faker.commerce.productDescription(),
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: false,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              });
            }
            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const MultiResourceWithCustomThemeAndPDataButForbidden = {
  render: Template,

  args: {
    appConfig: {
      ref: [
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
      ],
      pData: {
        platform: 'timum',
        personId: '12345',
      },
      calendarFrontend: 'pureListView',
    },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        identify: rest.get(
          '*/rest/1/crms/timum/resources/:uuid/customers/12345/identify',
          (req, res, ctx) => {
            const json = {
              email: 's***4@***.de',
              name: 'M',
            };

            return res(ctx.status(200), ctx.delay(500), ctx.json(json));
          },
        ),
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [];
            for (const ref of MultiResourceWithCustomThemeAndPDataButForbidden
              .args.appConfig.ref) {
              json.push({
                ref: ref,
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: ref,
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: faker.commerce.productDescription(),
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: false,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              });
            }
            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithMultipleResources = {
  render: Template,

  args: {
    appConfig: {
      ref: [
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
      ],
      calendarFrontend: 'condensedView',
      allResourcesOption: true,
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...DefaultUnknownCustomerFormStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [];
            for (const ref of WithMultipleResources.args.appConfig.ref) {
              json.push({
                ref: ref,
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: ref,
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: faker.commerce.productDescription(),
                  msgHelpText: '',
                  url: faker.internet.url(),
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: false,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              });
            }
            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithMultipleResourcesWithImages = {
  render: Template,

  args: {
    appConfig: {
      ref: [
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
      ],
      calendarFrontend: 'detailsListView',
      allResourcesOption: true,
    },
  },

  parameters: {
    msw: {
      handlers: {
        ...DefaultProductStory.parameters.msw.handlers,
        ...DefaultUnknownCustomerFormStory.parameters.msw.handlers,
        ...CalendarStories.parameters.msw.handlers,
        getPublicData: rest.get(
          '*/rest/1/resources/:uuid/public_data*',
          (req, res, ctx) => {
            const json = [];
            for (const ref of WithMultipleResources.args.appConfig.ref) {
              json.push({
                ref: ref,
                contact: {
                  name: 'Peter Lustig',
                  email: 'lustig@villa_regenbogen.de',
                  mobile: '01777483222',
                  phone: '',
                },
                resource: {
                  uuid: ref,
                  name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
                  description: faker.commerce.productDescription(),
                  msgHelpText: '',
                  url: faker.internet.url(),
                  imgUrl:
                    'https://lebenstraum-immobilien.com/wp-content/uploads/2020/09/immobilie-richtig-verkkaufen.jpg',
                },
                provider: {
                  name: 'Lila Lustig GmbH',
                  description: 'Some description of this provider',
                  isThemingAllowed: false,
                  isLocalisationAllowed: false,
                  areCustomFieldsAllowed: false,
                },
                channel: {
                  bookingProcess: 'IMMEDIATE',
                },
              });
            }
            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};

const generateBookables = (resourceId) => {
  return TimumApiMockResponses.getTimeslotsResponse()
    .setProviderId(faker.datatype.uuid())
    .setResourceId(resourceId ? resourceId : faker.datatype.uuid())
    .including(6)
    .daysBeforeFirstTimeslot()
    .including(7)
    .daysInFuture()
    .including(6)
    .timeslotsPerDay()
    .including(3)
    .timeslotsWithAppointmentsPerDay()
    .build().timeslots;
};

const generateProducts = () => {
  return [
    {
      uuid: faker.datatype.uuid(),
      name: 'Besichtigung',
      description: undefined,
      minDuration: undefined,
      exclusive: false,
    },
    ...TimumApiMockResponses.getProductResponse(3).products,
  ];
};

const generatePublicData = (refs, withBookables, withProducts) => {
  const json = [];
  for (const ref of refs) {
    json.push({
      ref: ref,
      contact: {
        name: 'Peter Lustig',
        email: 'lustig@villa_regenbogen.de',
        mobile: '01777483222',
        phone: '',
      },
      resource: {
        uuid: ref,
        name: faker.commerce.productName(), //<- 80 chars, max length of names in timum
        description: faker.commerce.productDescription(),
        msgHelpText: '',
        url: faker.internet.url(),
      },
      provider: {
        name: 'Lila Lustig GmbH',
        description: 'Some description of this provider',
        isThemingAllowed: false,
        isLocalisationAllowed: false,
        areCustomFieldsAllowed: false,
      },
      channel: {
        bookingProcess: 'IMMEDIATE',
      },
      bookables: withBookables ? generateBookables() : undefined,
      products: withProducts ? generateProducts() : undefined,
    });
  }

  return json;
};

export const WithMultipleTimeslots = {
  render: Template,

  args: {
    appConfig: {
      tslRefs: [
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
      ],
      calendarFrontend: 'pureListView',
    },
  },

  parameters: {
    msw: {
      handlers: {
        getSpecificBookables: rest.post(
          '*/rest/1/bookables/specific_bookables*',
          (req, res, ctx) => {
            const json = generatePublicData(
              WithMultipleTimeslots.args.appConfig.tslRefs,
              true,
              true,
            );
            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};

export const WithAllowedCustomThemeAndMultipleTimeslots = {
  render: Template,

  args: {
    appConfig: {
      tslRefs: [
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
        faker.datatype.uuid(),
      ],
      calendarFrontend: 'pureListView',
    },
    muiTheme: customTheme,
  },

  parameters: {
    msw: {
      handlers: {
        ...IdentifiedCustomerFormStories.parameters.msw.handlers,
        ...CancelAppointmentStories.parameters.msw.handlers,
        getSpecificBookables: rest.post(
          '*/rest/1/bookables/specific_bookables*',
          (req, res, ctx) => {
            const json = generatePublicData(
              WithAllowedCustomThemeAndMultipleTimeslots.args.appConfig.tslRefs,
              true,
              true,
            );
            return res(ctx.status(200), ctx.delay(0), ctx.json(json));
          },
        ),
      },
    },
  },
};
