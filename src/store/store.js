import { configureStore } from '@reduxjs/toolkit';
import { timumApiSlice } from '@timum/timum_pdk';
import appStateReducer from '../slices/appState';

export default configureStore({
  reducer: {
    [timumApiSlice.reducerPath]: timumApiSlice.reducer,
    appState: appStateReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(timumApiSlice.middleware),
  devTools: { trace: true },
});
