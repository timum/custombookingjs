import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import { isEqual } from 'lodash';
// import useDeepCompareEffect from 'use-deep-compare-effect';

export function init(appConfig, muiTheme, fcConfig, iFrameDocument) {
  let rootEl = iFrameDocument?.getElementById(
    appConfig?.rootElId ?? 'bookingjs',
  );

  if (!rootEl) {
    rootEl = document.getElementById(appConfig?.rootElId ?? 'bookingjs');
  }

  const root = createRoot(rootEl);
  root.render(
    <React.StrictMode>
      <App appConfig={appConfig} muiTheme={muiTheme} fcConfig={fcConfig} />
    </React.StrictMode>,
  );
}

/**
 * Initialises timum booking. For usage in script tags.
 * For usage in React use the 'TimumBooking' component instead.
 *
 * Wraps then app in an iFrame. Useful if more than one bookingJs must be displayed on the same page
 * Or where we encounter css interference.
 * @param {*} appConfig - the config object. See examples/fullExample.htm for all available conf options.
 * @param {*} muiTheme  - a mui theme object. See examples/fullExample.htm for a non exhaustive usage example. Works only for professional plans.
 * @param {*} fcConfig  - configurations for a fullCalendar frontend (set appConfig.calendarFrontend = 'fullCalendar') for this to have any effect.
 *                       Not muiTheme compatible. See https://fullcalendar.io/docs#toc fo all config options
 */
export function initialise(appConfig, muiTheme, fcConfig) {
  const rootEl = document.getElementById(appConfig?.rootElId ?? 'bookingjs');

  const iFrame = document.createElement('iframe');
  iFrame.style = 'border-width:0';
  iFrame.height = appConfig.height;
  iFrame.width = '100%';
  rootEl.appendChild(iFrame);

  appConfig.queryString = window.location.search;
  appConfig.queryHash = window.location.hash;

  document.appConfig = appConfig;
  document.muiTheme = muiTheme;
  document.fcConfig = fcConfig;

  const cdnUrl =
    appConfig.cdnUrl ?? 'https://cdn.timum.de/bookingjs/1/booking.js';

  iFrame.srcdoc = `
  <div id='${
    appConfig?.rootElId ?? 'bookingjs'
  }' style='margin: 0px 16px 0px 8px'>
    <script type='module'>
      import { init } from '${cdnUrl}';
      const context = window.parent.document;
      init(context.appConfig, context.muiTheme, context.fcConfig, window.document);
    </script>
  </div>
  `;
}

/**
 * React component.
 * For usage in script tags use the 'init' function instead.
 * @param {*} appConfig - the config object. See examples/fullExample.htm for all available conf options.
 * @param {*} muiTheme  - a mui theme object. See examples/fullExample.htm for a non exhaustive usage example. Works only for professional plans.
 * @param {*} fcConfig  - configurations for a fullCalendar frontend (set appConfig.calendarFrontend = 'fullCalendar') for this to have any effect.
 *                       Not muiTheme compatible. See https://fullcalendar.io/docs#toc fo all config options
 */
export function TimumBooking({ appConfig, muiTheme, fcConfig, iFrameStyle }) {
  // const [contentRef, setContentRef] = React.useState(null);
  // const mountNode = contentRef?.contentWindow?.document?.body;
  // const cache = createCache({
  //   key: 'css',
  //   container: contentRef?.contentWindow?.document?.head,
  //   prepend: true,
  // });

  // This implementation is just soo ugly. We always need to redownload bookingjs from our cdn :/
  // unfortunately mui adds its styles to the header of the main page and not of the iFrame
  // tried using emotion's createCache and CacheProvider combo but it had no effect.

  // also tried the solution described here but without success: https://github.com/mui/material-ui/issues/36558#issuecomment-1474984932
  const cdnUrl =
    appConfig.cdnUrl ?? 'https://cdn.timum.de/bookingjs/1/booking.js';

  appConfig.queryString = window.location.search;
  appConfig.queryHash = window.location.hash;
  appConfig.isComponent = true;

  // always get a undefined error when using hooks in this component.
  if (
    !isEqual(document.appConfig, appConfig) ||
    !isEqual(document.muiTheme, muiTheme) ||
    !isEqual(document.fcConfig, fcConfig)
  ) {
    const frame = document.getElementById('timum_booking_iframe');
    if (frame) {
      frame.contentWindow.postMessage({
        type: 'rerender',
        appConfig,
        muiTheme,
        fcConfig,
      });
    }
  }

  document.appConfig = appConfig;
  document.muiTheme = muiTheme;
  document.fcConfig = fcConfig;

  // useDeepCompareEffect(() => {
  //   const rerender = window?.parent?.document?.rerenderTimumBooking;
  //   if (rerender) {
  //     rerender({ ...appConfig }, { ...muiTheme }, { ...fcConfig });
  //   }
  // }, [appConfig, muiTheme, fcConfig]);

  // this never rerenders on a prop change, because none of the iframes props change.
  return (
    <iframe
      /* ref={setContentRef} */
      id="timum_booking_iframe"
      height={appConfig.height ?? '100%'}
      width={'100%'}
      style={{ borderWidth: 0, ...iFrameStyle }}
      srcDoc={`
        <div id='${
          appConfig?.rootElId ?? 'bookingjs'
        }' style='margin: 0px 16px 0px 8px'>
          <script type='module'>
            import { init } from '${cdnUrl}';
            const context = window.parent.document;
            init(context.appConfig, context.muiTheme, context.fcConfig, window.document);
          </script>
        </div>
  `}
    ></iframe>
  );
}

/** re-export yup so that users may write validation rules for custom fields */
export * as yup from 'yup';

/** re-export i18next instance so users can opt into localisation */
export * as i18next from 'i18next';

export { RE_EMOJI } from './defaults';

export { default as React } from 'react';
