@echo off

rem Define the list of tags to be deleted
setlocal enabledelayedexpansion
set "tags=v1.0.0 v1.1.0 v1.10.0 v1.10.1 v1.2.0 v1.2.1 v1.2.2 v1.3.0 v1.4.0 v1.4.1 v1.4.2 v1.5.0 v1.5.1 v1.5.2 v1.6.0 v1.7.0 v1.7.1 v1.7.2 v1.8.0 v1.8.1 v1.8.2 v1.9.0 v1.9.1 v1.9.2 v1.9.3 v2.0.0-beta v1.11.0 v1.12.0 v1.13.0 v1.14.0 v1.15.0 v1.15.1 v1.15.2 v1.16.0 v1.17.0 v1.17.1 v1.18.0 v1.18.1 v1.18.2 v1.19.0 v1.19.1 v1.20.0 v1.21.0 v1.22.0 v1.22.1 v1.22.2 v1.22.3 v1.22.4 v1.23.0 v1.24.0 v1.24.1 v1.24.2 v1.24.3 v1.25.0 v2.0.0 v2.0.1 v2.0.2 v2.1.0 v2.2.0 v2.3.0 v2.3.1 v2.3.2 v2.4.0 v2.5.0 v2.5.1 v2.5.2 v2.5.3 v2.6.0 v2.7.0 v2.7.1 v2.8.0 v2.8.1 v2.8.2 v2.8.3 v2.8.4 v2.8.5 v2.9.0 v2.9.1 v2.9.2 v2.9.3 v2.9.4 v2.9.5 v2.9.6 v2.9.7 v2.9.8 v2.10.0 v2.10.1"

rem Split the tags by spaces and loop through to delete
for %%t in (%tags%) do (
  echo Deleting tag %%t
  git push origin :refs/tags/%%t
  git tag --delete %%t
)

endlocal
pause

